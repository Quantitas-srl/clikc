<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', [App\Http\Controllers\DashboardController::class, 'landing']);

Route::get('welcome', function (Request $request) {
    return view('pages.welcome');
})->middleware(['auth', 'verified']);
Route::get('startup', [App\Http\Controllers\DashboardController::class, 'showStartup'])->middleware(['auth', 'verified']);
Route::get('profile', [App\Http\Controllers\DashboardController::class, 'showStartup'])->middleware(['auth', 'verified']);
Route::get('ia/next','App\Http\Controllers\IAController@nextEmpty')->middleware(['auth', 'verified', 'user.data']);



Route::get('learning-unit/{id}/show','App\Http\Controllers\LearningUnitController@showLearningUnit')->middleware(['auth', 'verified', 'user.data']);
Route::get('learning-unit/{id}/test','App\Http\Controllers\LearningUnitController@showLearningUnitTest')->middleware(['auth', 'verified', 'user.data']);
Route::get('learning-unit/{id}/test-completed','App\Http\Controllers\LearningUnitController@confirmLearningUnitTest')->middleware(['auth', 'verified', 'user.data']);
Route::get('learning-unit/{id}/pending','App\Http\Controllers\LearningUnitController@pendingLUAdvice')->middleware(['auth', 'verified', 'user.data']);

//Route::get('registry', [App\Http\Controllers\RegistryController::class, 'showRegistry'])->middleware(['auth', 'verified', 'user.data']);

Route::get('report', [App\Http\Controllers\ReportController::class, 'showReport'])->middleware(['auth', 'verified', 'user.data']);

Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'set'])->middleware(['auth', 'verified', 'user.data']);


Route::get('recap-test-time', [App\Http\Controllers\TestController::class, 'recapMessage'])->middleware(['auth', 'verified', 'user.data']);
Route::get('recap-test', [App\Http\Controllers\TestController::class, 'recap'])->middleware(['auth', 'verified', 'user.data']);
Route::get('recap-test-completed', [App\Http\Controllers\TestController::class, 'setRecapCompleted'])->middleware(['auth', 'verified', 'user.data']);

Route::post('ajax/startup-depending-data','App\Http\Controllers\AjaxController@startupDependingData');
Route::post('ajax/check-test','App\Http\Controllers\AjaxController@checkTest')->middleware(['auth', 'verified', 'user.data']);
Route::post('ajax/like-lu','App\Http\Controllers\AjaxController@likeLU')->middleware(['auth', 'verified', 'user.data']);
Route::post('ajax/unlike-lu','App\Http\Controllers\AjaxController@unlikeLU')->middleware(['auth', 'verified', 'user.data']);
Route::post('ajax/set-cluster-skill','App\Http\Controllers\AjaxController@setClusterSkill')->middleware(['auth', 'verified']);
Route::post('ajax/set-cluster-preference','App\Http\Controllers\AjaxController@setClusterPreference')->middleware(['auth', 'verified']);
Route::post('ajax/update-profile-data','App\Http\Controllers\AjaxController@updateProfileData')->middleware(['auth', 'verified']);
Route::post('ajax/ia-next','App\Http\Controllers\AjaxController@iaNext')->middleware(['auth', 'verified']);
Route::post('ajax/save-image','App\Http\Controllers\AjaxController@saveImage')->middleware(['auth', 'verified']);
Route::post('ajax/save-report','App\Http\Controllers\AjaxController@saveReport')->middleware(['auth', 'verified']);

Route::get('learning-unit-labour-market/{id}/show','App\Http\Controllers\LearningUnitLabourMarketController@showLearningUnitLabourMarket')->middleware(['auth', 'verified', 'user.data']);
Route::get('learning-unit-labour-market/{id}/completed','App\Http\Controllers\LearningUnitLabourMarketController@confirmLearningUnitLabourMarketCompleted')->middleware(['auth', 'verified', 'user.data']);


Route::get('switch-to-basic-user','App\Http\Controllers\DashboardController@switchToBasicUser')->middleware(['auth', 'verified']);
Route::get('switch-to-super-user','App\Http\Controllers\DashboardController@switchToSuperUser')->middleware(['auth', 'verified']);
Route::get('superuser/users-list','App\Http\Controllers\LearningUnitController@usersRecap')->middleware(['auth', 'verified', 'user.super']);
Route::get('superuser/profile', [App\Http\Controllers\DashboardController::class, 'showStartup'])->middleware(['auth', 'verified', 'user.super']);
Route::get('superuser/report', [App\Http\Controllers\ReportController::class, 'showReport'])->middleware(['auth', 'verified', 'user.super']);
Route::get('superuser/dashboard', [App\Http\Controllers\DashboardController::class, 'set'])->middleware(['auth', 'verified', 'user.super']);



Route::get('terms-and-conditions', function (Request $request) {
    return view('pages.terms-and-conditions');
});



Route::get('test-checker','App\Http\Controllers\TestController@checker');
Route::get('emails-creator','App\Http\Controllers\UserDataController@emailsCreator')->middleware(['auth', 'verified', 'user.super']);


require __DIR__.'/auth.php';
