function readMoreOrLess() {
    var readBtn = document.getElementsByClassName("readBtn")[0];
    var objectivesP = document.getElementsByClassName("objectivesP")[0];
    var infoP = document.getElementsByClassName("infoP")[0];

    if (objectivesP.hidden) {
        readBtn.style.marginBottom = "0px";
        readBtn.querySelector("span").textContent = "Read Less";
        readBtn.querySelector("img").src = "static/img/Vector-up.png";
        objectivesP.hidden = false;
        infoP.hidden = false;
        readBtn.scrollIntoView();
    }
    else {
        readBtn.style.marginBottom = "60px";
        readBtn.querySelector("span").textContent = "Read More";
        readBtn.querySelector("img").src = "static/img/Vector-down.png";
        objectivesP.hidden = true;
        infoP.hidden = true;
    }
}