<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * A class tracking a user to the learning units.
 */
class LearningUnitTracker extends Model {

    use HasFactory;
    protected $table = "user_learning_unit_tracker";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $uid;
    protected $luid;
    protected $isLabourMarket;


    /**
     * The constructor
     * @param {number} uid the id of the user, optional
     * @param {number} luid the id of a learning unit, optional
     * @param {boolean} isLabourMarket true if the learning unit is of type labour market
     */
    function __construct($uid = null, $luid = null, $isLabourMarket = false) {
        $this->uid = $uid;
        $this->luid = $luid;
        $this->isLabourMarket = $isLabourMarket;
    }


    /**
     * Returns the last learning units completed by this user.
     * @param {number} limit the number of learning units to return
     * @param {boolean} includeLabourMarket true to include labour market learning units
     * @return {array} an array of learning units
     */
    public function getLastLearningUnitStreak($limit = 10, $includeLabourMarket = false) {
        return DB::select(
            ' SELECT * ' . 
            ' FROM ' . $this->table . 
            ' WHERE ' . 
            ($includeLabourMarket ? '' : ' learning_unit_id IS NOT NULL AND ') . 
            ' user_id = ' . $this->uid . ' ORDER BY ' . $this->primaryKey . ' DESC LIMIT ' . $limit
        );
    }


    /**
     * Returns all the not-started learning units for this user
     * @return {array} the not started learning units
     */
    public function listUnstartedLearningUnitsId() {
        $sql = 'SELECT id FROM learning_unit WHERE ' . $this->primaryKey . ' NOT IN ' . 
        ' (SELECT learning_unit_id FROM ' . $this->table . ' ' .
        ' WHERE user_id = ' . $this->uid . ' AND learning_unit_id IS NOT NULL)';
        return DB::select($sql);
    }


    /**
     * Returns all the not-started labour market learning units for this user
     * @return {array} the not started labour market learning units
     */
    public function listUnstartedLabourMarketLearningUnitsId() {
        $sql = 'SELECT id FROM learning_unit_labour_market WHERE ' . $this->primaryKey . ' NOT IN ' . 
        ' (SELECT learning_unit_labour_market_id FROM ' . $this->table . ' ' . 
        ' WHERE user_id = ' . $this->uid . ' AND learning_unit_labour_market_id IS NOT NULL)';
        return DB::select($sql);
    }


    /**
     * Gets the status of a user in doing a learning unit, in terms of:
     * - started_on time or null
     * - completed_on time or null
     * - test_completed_on time or null
     * - liked 1 = liked, -1 = disliked, 0 = not yet expressed or the learning unit is labour market
     * @return {object} the user's status in this learning unit, or null
     */
    public function getUserStatus() {
        $field = $this->isLabourMarket ? 'learning_unit_labour_market_id' : 'learning_unit_id';
        $status = DB::select(
            ' SELECT started_on, completed_on, test_completed_on, liked ' . 
            ' FROM ' . $this->table . ' ' . 
            ' WHERE ' . $field . ' = ' . $this->luid . ' and user_id = ' . $this->uid);
        if (count($status)) {
            return $status[0];
        }
        return null;
    }


    /**
     * Returns the pending learning unit, if any
     * @return {object} the pending learning unit, or null
     */
    public function getPending() {
        $sql = 'SELECT learning_unit_id FROM ' . $this->table . ' ' . 
        ' WHERE test_completed_on IS NULL AND user_id = ' . $this->uid . ' AND learning_unit_id IS NOT NULL ORDER BY id DESC';
        $res = DB::select($sql);
        if (count($res)) {
            return $res[0]->learning_unit_id;
        }
        return null;
    }


    /**
     * Sets this learning unit as started
     */
    public function setStarted() {
        $sql = 'INSERT INTO ' . $this->table . ' (user_id, ' . $this->getIdName() . ', started_on, completed_on, test_completed_on) ' . 
        ' VALUES (' . $this->uid . ', ' . $this->luid . ', CURRENT_TIMESTAMP, NULL, NULL) ';
        DB::insert($sql);
    }


    /**
     * Sets this learning unit as completed (= test is running)
     */
    public function setCompleted() {
        $sql = 'UPDATE ' . $this->table . ' SET completed_on = CURRENT_TIMESTAMP WHERE user_id = ' . 
        $this->uid . ' and ' . $this->getIdName() . ' = ' . $this->luid;
        DB::update($sql);
    }


    /**
     * Sets this learning unit as completed with test completed
     */
    public function setTestCompleted() {
        $sql = 'UPDATE ' . $this->table . ' SET test_completed_on = CURRENT_TIMESTAMP WHERE user_id = ' . 
        $this->uid . ' and ' . $this->getIdName() . ' = ' . $this->luid;
        DB::update($sql);
    }


    /**
     * Returns the foreign key to consider when updating the table:
     * - learning_unit_id for normal learning units
     * - learning_unit_labour_market_id for labour market learning units
     */
    private function getIdName() {
        return $this->isLabourMarket ? 'learning_unit_labour_market_id' : 'learning_unit_id';
    }


    /**
     * Likes this learning unit
     */
    public function like() {
        return $this->setLike(+1);
    }


    /**
     * Dislikes this learning unit
     */
    public function unlike() {
        return $this->setLike(-1);
    }


    /**
     * Sets the like status for this learning unit
     * @param {number} value >0 to like, <0 to dislike
     */
    private function setLike($value) {
        DB::update(
            ' UPDATE ' . $this->table . ' SET liked = ' . $value . 
            ' WHERE user_id = ' . $this->uid . ' AND learning_unit_id = ' . $this->luid
        );
    }


    /**
     * Returns the learning units visited by this user
     * @param {number} limit a limit on the number of results, optional
     * @return {array} a list of learning units
     */
    public function list($limit = null) {
        $sql = 'SELECT t.title, lu.cluster_number, ulut.learning_unit_id, ' . 
        ' ulut.started_on, ulut.completed_on, ulut.test_completed_on, ulut.liked ' . 
        ' FROM user_learning_unit_tracker ulut LEFT JOIN learning_unit lu ON ulut.learning_unit_id = lu.id ' . 
        ' left join translation t on lu.id = t.learning_unit_id ' .
        ' WHERE ulut.user_id = ' . $this->uid . ' AND ulut.learning_unit_id IS NOT NULL ' .
        ' ORDER BY started_on DESC';
        $setLU = DB::select($sql);

        $sql = 'SELECT lu.title, ulut.learning_unit_labour_market_id, ' . 
        ' ulut.started_on, ulut.completed_on, ulut.test_completed_on, ulut.liked ' . 
        ' FROM user_learning_unit_tracker ulut LEFT JOIN learning_unit_labour_market lu ON ulut.learning_unit_labour_market_id = lu.id ' . 
        '  ' .
        ' WHERE ulut.user_id = ' . $this->uid . ' AND ulut.learning_unit_labour_market_id IS NOT NULL ' .
        ' ORDER BY started_on DESC';
        $setLULM = DB::select($sql);

        $merge = array_merge($setLU, $setLULM);

        usort($merge, function ($a, $b) {
            return strcmp($b->started_on, $a->started_on);
        });

        if (isset($limit)) {
            $merge = array_slice($merge, 0, $limit);
        }
        return $merge;
    }

    


    /**
     * Checks if this learning unit is started
     * @return {boolean} true if this learning unit is started, false otherwise
     */
    public function isStarted() {
        return isset(($this->getUserStatus())->started_on);
    }


    /**
     * Checks if this learning unit is completed
     * @return {boolean} true if this learning unit is completed, false otherwise
     */
    public function isCompleted() {
        return isset(($this->getUserStatus())->completed_on);
    }


    /**
     * Checks if this learning unit is completed with test completed
     * @return {boolean} true if this learning unit is completed with test completed, false otherwise
     */
    public function isTestCompleted() {
        return isset(($this->getUserStatus())->test_completed_on);
    }


    /**
     * Checks if this user still has some labour market learning units to do
     * @return {boolean} true if this user still has some labour market learning units to do
     */
    public function hasLabourMarketLearningUnitsToDo() {
        $total = +DB::select('SELECT count(' . $this->primaryKey . ') as n FROM learning_unit_labour_market')[0]->n;
        $done = +DB::select('SELECT count(' . $this->primaryKey . ') as n FROM ' . $this->table . ' WHERE user_id= ' . $this->uid . ' and learning_unit_labour_market_id is not null')[0]->n;
        return $done < $total;
    }

    /**
     * Checks if this learning unit is in user's history
     * or in the last recommendation
     * @return {boolean} true if in history
     */
    public function isInHistory() {
        $res = DB::select('SELECT ' . $this->primaryKey . ' FROM ' . $this->table . ' WHERE (learning_unit_id = ' . $this->luid . ' OR learning_unit_labour_market_id = ' . $this->luid . ' ) AND user_id = ' . $this->uid);
        if (count($res) > 0) { // if in history, immediately return true
            return true;
        }
        // else check in last recommendation
        $res = DB::select('SELECT recommendation FROM user_recommendation WHERE user_id = ' . $this->uid . ' ORDER BY recommended_on DESC');
        if (count($res) > 0) {
            $data = json_decode($res[0]->recommendation);
            $ids = $data->ids;
            return array_search($this->luid, $ids) !== false;
        } else {
            return false;
        }
    }


    /**
     * Saves a recommendation
     * @param {array} data the recommendation
     */
    public function saveRecommendation($data) {
        $sql = 'INSERT INTO user_recommendation (user_id, recommendation) VALUES (' . $this->uid . ', :data)';
        DB::insert(DB::raw($sql), array(
            'data' => json_encode($data)
        ));
    }


    /**
     * Gets user's tests result for this learning unit
     * @return {number} the average accurancy of the tests
     */
    public function getLearningUnitTestResults() {
        $res = DB::select('SELECT id FROM translation WHERE learning_unit_id = ' . $this->luid);
        $tid = $res[0]->id;
        $tids = implode(',', array_map(function($item) {
            return $item->id;
        }, DB::select('SELECT id FROM tests WHERE translation_id = ' . $tid)));
        $accs = DB::select('SELECT accuracy FROM user_test_tracker WHERE test_id in (' . $tids . ') AND used_for_recap_test = 0 AND user_id = ' . $this->uid);
        $avg = 0;
        foreach ($accs as $acc) {
            $avg += $acc->accuracy;
        }
        return count($accs) ? round(($avg / count($accs))*100) : 0;
    }


    /**
     * Counts started learning units for this user
     * @return {number} the amount
     */
    public function countStarted() {
        $sql = 'SELECT count(*) as n FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid;
        return DB::select($sql)[0]->n;
    }


    /**
     * Counts completed learning units for this user, grouped by cluster
     * @return {number} the amount
     */
    public function countCompletedByCluster() {
        $sql = 'SELECT cluster_number, count(*) as n FROM user_learning_unit_tracker ulut left join learning_unit lu on ulut.learning_unit_id=lu.id WHERE completed_on IS NOT NULL AND learning_unit_id IS NOT NULL AND ulut.user_id = ' . $this->uid . ' group by cluster_number';
        return DB::select($sql);
    }


    /**
     * Counts started learning units per day for this user
     * @return {array} the amount per day
     */
    public function countPerDay() {
        $sql = 'SELECT DATE(started_on) as date, count(*) as n FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid . ' group by DATE(started_on)';
        return DB::select($sql);
    }


    /**
     * Gets info about best month
     * @return {array} the info
     */
    public function bestMonth() {
        $sql = 'SELECT YEAR(started_on) as year, MONTH(started_on) as month, count(*) as n FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid . ' group by YEAR(started_on), MONTH(started_on) order by n desc limit 1';
        $res = DB::select($sql);
        if (isset($res) && $res && count($res)) {
            return $res[0];
        }
        return -1;
    }


    /**
     * Gets first date of LUs completed
     * @return {*} the date or null
     */
    public function getFirstDate() {
        $sql = 'SELECT completed_on FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid . ' and completed_on is not null order by completed_on asc limit 1';
        $res = DB::select($sql);
        if (count($res)) {
            return $res[0]->completed_on;
        }
        return null;
    }


    /**
     * Gets last date of LUs completed
     * @return {*} the date or null
     */
    public function getLastDate() {
        $sql = 'SELECT completed_on FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid . ' and completed_on is not null order by completed_on desc limit 1';
        $res = DB::select($sql);
        if (count($res)) {
            return $res[0]->completed_on;
        }
        return null;
    }


    /**
     * Returns users recap for superuser
     */
    public function getUsersRecap() {
        $tabDataByUser = array();

        // retrieve data from user_data table
        $usersDataSql = 'select user_id, name, surname, other from user_data';
        $usersData = DB::select($usersDataSql);
        foreach ($usersData as $userData) {
            $tabDataByUser[$userData->user_id] = array(
                'name' => $userData->name,
                'surname' => $userData->surname,
                'email' => json_decode($userData->other ? $userData->other : '{"email":""}')->email
            );
        }

        // get user ids from previous query
        $uids = array_map(function($item) {
            return $item->user_id;
        }, $usersData);

        // get data from auth system
        $missingCreatedAt = array();
        $accountsDataSql = 'select id, created_at from users where id in (' . implode(',', $uids) . ')';
        $accountsData = DB::select($accountsDataSql);
        foreach ($accountsData as $accountData) {
            $tabDataByUser[$accountData->id]['created_at'] = $accountData->created_at;
            if (!isset($accountData->created_at)) {
                $missingCreatedAt[] = $accountData->id;
            }
        }

        // in case of missing created_at date, retrieve date from first user_learning_unit_tracker record
        foreach ($missingCreatedAt as $missingCreatedAtId) {
            $sql = 'select started_on from user_learning_unit_tracker where user_id=' . $missingCreatedAtId . ' order by started_on asc limit 1';
            $data = DB::select($sql);
            if (isset($data) && count($data)) {
                $tabDataByUser[$missingCreatedAtId]['created_at'] = $data[0]->started_on;
            }
        }

        // get data from LU tracker
        $LUsTrackerDataSql = 'select user_id, count(*) as n from user_learning_unit_tracker where user_id in (' . implode(',', $uids) . ') group by user_id';
        $LUsTrackerData = DB::select($LUsTrackerDataSql);
        foreach ($LUsTrackerData as $LUTrackerData) {
            $tabDataByUser[$LUTrackerData->user_id]['nlu'] = $LUTrackerData->n;
        }

        // get data from tests
        $testsDataSql = 'select user_id, avg(accuracy) as accuracy from user_test_tracker where user_id in (' . implode(',', $uids) . ') group by user_id';
        $testsData = DB::select($testsDataSql);
        foreach ($testsData as $testData) {
            $tabDataByUser[$testData->user_id]['accuracy'] = $testData->accuracy*100;
        }
        
        return $tabDataByUser;
    }


    /**
     * Counts liked and disliked LUs by this user
     */
    public function countLikedDislikedByUser() {
        $sql = 'SELECT liked, count(liked) as n FROM `user_learning_unit_tracker` WHERE user_id = ' . $this->uid . ' group by liked;';
        $data = DB::select($sql);
        $liked = 0;
        $disliked = 0;
        if (isset($data) && $data && count($data)) {
            foreach ($data as $datum) {
                if (+$datum->liked === 1) {
                    $liked = $datum->n;
                } else if (+$datum->liked === -1) {
                    $disliked = $datum->n;
                }
            }
        }
        return array(
            'liked' => $liked,
            'disliked' => $disliked
        );
    }

    

}
