<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * A class tracking users to tests.
 */
class TestTracker extends Model {

    use HasFactory;
    protected $table = "user_test_tracker";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $uid;

    
    /**
     * The constructor
     * @param {number} uid the id of the user, optional
     */
    function __construct($uid = null) {
        $this->uid = $uid;
    }


    /**
     * Returns the last recap test, if any
     * @return {array} the last recap test data
     */
    public function getLastRecap() {
        return DB::select(
            ' SELECT * FROM user_recap_test_tracker ' . 
            ' WHERE user_id = ' . $this->uid . 
            ' ORDER BY started_on DESC LIMIT 1'
        );
    }


    /**
     * Returns the submitted data for a set of tests for a user
     * @param {number} uid the id of the user
     * @param {array} tids the id of the tests
     * @param {boolean} usedForRecapTest true if the test is used for a recap, false otherwise
     * @return {array} the data
     */
    public function get($uid, $tids, $usedForRecapTest = false) {
        $tids = implode(',', $tids);
        $sql =  ' SELECT submitted_data, accuracy FROM user_test_tracker ' . 
                ' WHERE user_id = ' . $uid .' AND test_id in (' . $tids . ') ';
        if ($usedForRecapTest) {
            $sql .= ' AND used_for_recap_test =  ' . ($usedForRecapTest ? '1' : '0');
        }
        return DB::select($sql);
    }


    /**
     * Saves the data of a submitted test
     * @param {number} uid the id of the user
     * @param {number} tid the id of the test
     * @param {object} data the submitted data
     * @param {number} accuracy the score of the test, between 0 and 1
     * @param {boolean} isRecapTest true if the test is used for a recap, false otherwise
     */
    public function track($uid, $tid, $data, $accuracy, $isRecapTest) {
        $sql = 'INSERT INTO user_test_tracker ' .
            '(user_id, test_id, submitted_on, submitted_data, accuracy, used_for_recap_test) VALUES ' . 
            '(' . $uid . ', ' . $tid . ', CURRENT_TIMESTAMP, :data, ' . $accuracy . ', ' . ($isRecapTest ? 1 : 0) . ')';
        $results = DB::insert(DB::raw($sql), array(
            'data' => json_encode($data)
        ));
    }


    /**
     * Returns the id of a pending recap, if any
     * @return {number} the id of the pending recap
     */
    public function getPendingRecapId() {
        $sql = 'SELECT id FROM user_recap_test_tracker WHERE user_id = ' . $this->uid .
            ' AND completed_on IS NULL ' .
            ' ORDER BY started_on DESC LIMIT 1 ';
        $res = DB::select($sql);
        if (count($res)) {
            return $res[0]->id;
        }
        return null;
    }


    /**
     * Stores a new recap
     * @param {number} lastLearningUnitId the id of the last learning unit visited by the user
     * @param {array} randomTestsData the data
     */
    public function insertRecap($lastLearningUnitId, $randomTestsData) {
        
        $sql = 'INSERT INTO user_recap_test_tracker ' . 
            ' (user_id, after_learning_unit_id ) VALUES (' .
                $this->uid . ', ' . $lastLearningUnitId . 
            ')';
        DB::insert($sql);

        $sql = 'SELECT id FROM user_recap_test_tracker WHERE ' .
            ' user_id = ' . $this->uid . ' ORDER BY started_on DESC LIMIT 1 ';
        $lastId = DB::select($sql)[0]->id;
        
        foreach ($randomTestsData as $randomTestData) {
            $sql = 'INSERT INTO user_recap_test_tracker_test ' . 
            ' (user_recap_test_tracker_id, test_id) VALUES (' .
                $lastId . ', ' . $randomTestData['id'] . 
            ')';
            DB::insert($sql);
        }

    }


    /**
     * Returns a recap
     * @param {number} id the id of the recap
     * @return {array} the recap data
     */
    public function getRecap($id) {
        $sql = 'SELECT t.* FROM user_recap_test_tracker_test urttt LEFT JOIN tests t ON ' . 
            'urttt.test_id = t.id WHERE urttt.user_recap_test_tracker_id = ' . $id;
        $tests =  DB::select($sql);
        $testsData = array();
        foreach ($tests as $test) {
            $testId = $test->id;
            $testType = $test->type;
            $testCustomData = DB::select('SELECT * FROM ' . str_replace('-', '_', $testType) . '_answer where test_id = ' . $testId . ' order by id asc');
            $testsData[$testId] = array(
                'id' => $testId,
                'type' => $testType,
                'description' => $test->description,
                'question' => $test->question,
                'cloze' => $test->cloze,
                'custom' => $testCustomData
            );
        }
        return $testsData;
    }


    /**
     * Sets a recap test as completed
     * @param {number} id the id of the recap
     */
    public function setRecapAsCompleted($id) {
        DB::update('UPDATE user_recap_test_tracker SET completed_on = CURRENT_TIMESTAMP WHERE id = ' . $id);
    }


    /**
     * Counts the number of recaps started for a user
     * @return {number} the number of recaps started for this user
     */
    public function countRecaps() {
        return DB::select('SELECT count(id) as n FROM user_recap_test_tracker WHERE user_id = ' . $this->uid)[0]->n;
    }


    /**
     * Gets a recap of user recap tests
     * @return {array} the recap
     */
    public function getUserRecapsSummary() {
        $sql = 'SELECT urtt.id, avg(utt.accuracy)*100 as score FROM user_recap_test_tracker urtt left join user_recap_test_tracker_test urttt on urtt.id=urttt.user_recap_test_tracker_id left join user_test_tracker utt on urttt.test_id=utt.test_id where utt.used_for_recap_test=1 and urtt.user_id = ' . $this->uid . ' group by urtt.id order by urtt.id asc';
        return DB::select($sql);
    }


    /**
     * Gets a recap of user tests
     * @param {number} 1 if the test was used for a recap, 0 otherwise
     * @return {array} the recap
     */
    public function getUserTestsSummary($usedForRecap = 0) {
        $sql = 'SELECT accuracy*100 as score FROM user_test_tracker where used_for_recap_test = ' . $usedForRecap . ' and user_id = ' . $this->uid;
        return DB::select($sql);
    }


    /**
     * Gets all user's tests with cluster number and score
     * @return {array} the tests info
     */
    public function getUserTestsByCluster() {
        $sql = 'SELECT lu.cluster_number, accuracy*100 as score FROM user_test_tracker utt left join tests t on utt.test_id = t.id left join translation tr on t.translation_id = tr.id left join learning_unit lu on tr.learning_unit_id = lu.id where utt.user_id = ' . $this->uid;
        return DB::select($sql);
    }

}
