<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * A class representing a test.
 */
class Test extends Model {

    use HasFactory;
    protected $table = "learning_unit";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $id;
    

    /**
     * The constructor
     * @param {number} id the id of the test
     */
    function __construct($id = -1) {
        $this->id = $id;
    }


    /**
     * Returns this test
     * @return {object} the content of this test from tests table
     */
    public function get() {
        return DB::select('SELECT * FROM tests WHERE id=' . $this->id);
    }


    /**
     * Returns this test's custom data
     * @return {object} the content of this test from [...]_answer table (e.g. gap_fill_answer)
     */
    public function getCustomData($type) {
        return DB::select('SELECT * FROM ' . str_replace('-', '_', $type) . '_answer WHERE test_id=' . $this->id);
    }


    /**
     * Returns data related to a translation's id
     * @param {number} translationId the id of the translation
     * @param {array} the data
     */
    public function getByTranslationId($translationId) {
        if (!isset($translationId)) {
            return array();
        }
        $tests = DB::select('SELECT * FROM tests where translation_id = ' . $translationId);
        $testsData = array();
        foreach ($tests as $test) {
            $testId = $test->id;
            $testType = $test->type;
            $testCustomData = DB::select('SELECT * FROM ' . str_replace('-', '_', $testType) . '_answer where test_id = ' . $testId . ' order by id asc');
            $testsData[$testId] = array(
                'id' => $testId,
                'type' => $testType,
                'description' => $test->description,
                'question' => $test->question,
                'cloze' => $test->cloze,
                'custom' => $testCustomData
            );
        }
        return $testsData;
    }


    /**
    * Returns the data of the learning units related to a set of tests
    * @param {array} ids the ids of the tests
    * @param {array} the data of the related learning units
    */
    public function getLearningUnitData($ids) {
        $clause = '(' . implode(',', $ids) . ')';
        $sql = ' SELECT lu.* ' . 
            ' FROM tests t ' . 
            ' LEFT JOIN translation tr ON t.translation_id = tr.id ' . 
            ' LEFT JOIN learning_unit lu ON tr.learning_unit_id = lu.id ' . 
            ' WHERE t.id in ' . $clause;
        return DB::select($sql);
    }


    /**
     * Randomly picks a test from a learning unit
     * @param {number} learning_unitId the id of the learning unit
     * @param {array} the data of the picked test
     */
    public function randomlyGetFromLearningUnit($learning_unitId) {

        $res = DB::select('SELECT id FROM translation where language_name="en" and learning_unit_id = ' . $learning_unitId);

        $translationId = $res[0]->id;

        $data = array_values($this->getByTranslationId($translationId));
        $n = count($data);
        if ($n) {
            $rand = rand(0, $n-1);
            return $data[$rand];
        }
        return null;
    }













    // FROM HERE ONE: TEST CHECKER

    /**
     * Gets data of tests of a certain type
     * @param {string} type the type
     */
    function getTestData($type) {
        return DB::select("SELECT * FROM tests WHERE type = '" . $type . "'");
    }


    /**
     * Gets custom data of tests of a certain type
     * @param {string} table the table with data
     * @param {string} where the where clause
     */
    function getTestCustomData($table, $where) {
        return DB::select("SELECT * FROM " . $table . ($where ? (" WHERE " . $where) : ''));
    }


    /**
     * Searches for errors on tests
     */
    function checker() {

        $warnings = array(
            'gap-fill' => array(),
            'matching-terms' => array(),
            'multiple-choice' => array(),
            'jumble-text' => array()
        );
        $errors = array(
            'gap-fill' => array(),
            'matching-terms' => array(),
            'multiple-choice' => array(),
            'jumble-text' => array()
        );



        
        $gap_fill_answer = $this->getTestCustomData('gap_fill_answer', "cloze_answer <> ''");
        $gap_fill_test = $this->getTestData('gap-fill');

        $matching_terms_answer = $this->getTestCustomData('matching_terms_answer', '');
        $matching_terms_test = $this->getTestData('matching-terms');

        $multiple_choice_answer = $this->getTestCustomData('multiple_choice_answer', '');
        $multiple_choice_test = $this->getTestData('multiple-choice');

        $jumble_text_answer = $this->getTestCustomData('jumble_text_answer', '');
        $jumble_text_test = $this->getTestData('jumble-text');

        
        



        $errors['gap-fill'][] = array(
            'type' => 'Missing cloze placeholders. Correct format is: "-1-". An example of wrong format is: "-1".',
            'data' => $this->getGapFillWithoutPlaceholders($gap_fill_test)
        );

        $warnings['gap-fill'][] = array(
            'type' => 'Only 1 answer in clozes. Provide more than one answer to each cloze, separated by ";".',
            'data' => $this->getGapFillWithOneAnswer($gap_fill_answer)
        );

        $warnings['gap-fill'][] = array(
            'type' => 'Unexpected characters, for example something like "_____". Only text and cloze placeholders (e.g. "-1-") are allowed.',
            'data' => $this->getGapFillWithWrongPlaceholders($gap_fill_test)
        );

        $warnings['gap-fill'][] = array(
            'type' => 'Short description. Provide descriptive content for the user.',
            'data' => $this->getWithShortDescription($gap_fill_test, 13)
        );

        $warnings['matching-terms'][] = array(
            'type' => 'Short description. Provide descriptive content for the user.',
            'data' => $this->getWithShortDescription($matching_terms_test, 15)
        );

        $warnings['matching-terms'][] = array(
            'type' => 'Unexpected characters, for example something like "_____". Only text is allowed in pairs.',
            'data' => $this->getMatchingTermsWithWrongPlaceholders($matching_terms_answer)
        );

        $warnings['matching-terms'][] = array(
            'type' => 'Empty answer found.',
            'data' => $this->getMatchingTermsWithWithEmptyAnswer($matching_terms_answer)
        );
        
        $warnings['multiple-choice'][] = array(
            'type' => 'Short description. Provide descriptive content for the user.',
            'data' => $this->getWithShortDescription($multiple_choice_test, 13)
        );

        $warnings['jumble-text'][] = array(
            'type' => 'Short description. Provide descriptive content for the user.',
            'data' => $this->getWithShortDescription($jumble_text_test, 22)
        );

        $warnings['jumble-text'][] = array(
            'type' => 'Empty answer found.',
            'data' => $this->getJumbleTextWithWithEmptyAnswer($jumble_text_answer)
        );

        $warnings['multiple-choice'][] = array(
            'type' => 'No correct option provided.',
            'data' => $this->getMultipleChoiceWithoutCorrect($multiple_choice_answer)
        );

        $warnings['multiple-choice'][] = array(
            'type' => 'Empty answer found.',
            'data' => $this->getMultipleChoiceWithEmptyAnswer($multiple_choice_answer)
        );


        $tests = $this->groupByTest($errors, $warnings);
        
        return $tests;
    }



    /**
     * Groups error/warnings data by test
     * @param {array} items the error/warnings
     * @param {string} level the level
     * @param {array} tests optional data alrady produced by this function
     */
    function _groupByTest($items, $level, $tests = array()) {
        foreach ($items as $type=>$itemTest) {
            foreach ($itemTest as $item) {
                $itemText = $item['type'];
                $data = $item['data'];
                foreach ($data as $datum) {
                    if (!isset($tests[$datum['luid']['luid']])) {
                        $tests[$datum['luid']['luid']] = array();
                    }
                    if (!isset($tests[$datum['luid']['luid']][$datum['id']])) {
                        $tests[$datum['luid']['luid']][$datum['id']] = array();
                    }
                    $tests[$datum['luid']['luid']][$datum['id']][] = array(
                        'index' => $datum['index'],
                        'type' => $type,
                        'text' => $itemText,
                        'level' => $level,
                        'identifier' => $datum['luid']['identifier']
                    );
                }
            }
        }
        return $tests;
    }


    /**
     * Groups errors data and warnings data by test
     * @param {array} errors the errors data
     * @param {array} warnings the warnings data
     */
    function groupByTest($errors, $warnings) {
        return $this->_groupByTest($warnings, 'warning', $this->_groupByTest($errors, 'error'));
    }


    /**
     * Gets translation ids from an array of test ids
     * @param {array} the test ids
     */
    function getTranslationIds($ids) {
        if (!count($ids)) {
            return array();
        }
        return array_map(function($rec) {
            return array(
                'id' => $rec->id,
                'translation_id' => $rec->translation_id
            );
        }, DB::select('SELECT id, translation_id FROM `tests` WHERE id in (' . implode(',', $ids) . ') ORDER BY FIND_IN_SET(id, \'' . implode(',', $ids) . '\');')); // https://stackoverflow.com/questions/1631723/maintaining-order-in-mysql-in-query
    }


    /**
     * Gets learning unit ids from an array of translation ids
     * @param {array} the translation ids
     */
    function getLuids($tids) {
        if (!count($tids)) {
            return array();
        }
        return array_map(function($rec) {
            return array(
                'luid' => $rec->learning_unit_id, 
                'identifier' => $rec->identifier
            );
        }, DB::select('SELECT t.learning_unit_id, lu.identifier FROM translation t left join learning_unit lu on t.learning_unit_id = lu.id WHERE t.id in (' . implode(',', $tids) . ') ORDER BY FIND_IN_SET(t.id, \'' . implode(',', $tids) . '\');'));
    }


    /**
     * Filters an array of data depending on a condition, extracting
     * a particular field from items passing the filter
     * @param {array} res the array to filter
     * @param {function} the condition to check
     * @param {string} field to extract
     */
    function filter($res, $condition, $field, $structured = true) {
        $ids = array();
        foreach ($res as $rec) {
            if ($condition($rec)) {
                if ($structured) {
                    $ids[] = array(
                        'id' => $rec->id,
                        $field => $rec->$field
                    );
                } else {
                    $ids[] = $rec->$field;
                }
            }
        }
        return $ids;
    }

    
    /**
     * Gets the index of a test among its siblings
     * @param {number} the test's id
     * @param {number} the test's translation's id
     */
    function getTestIndex($testId, $translationId) {
        $translationTests = array_map(function($item) {
            return $item->id;
        }, DB::select('select id from tests where translation_id = ' . $translationId));
        return array_search($testId, $translationTests);
    }


    /**
     * Binds test's data to the index among its siblings and to its learning unit id
     * @param {array} test' data
     */
    function bindTestIndexAndLuid($translationTestIds) {

        $translationIds = array_values(array_unique(array_map(function($item) {
            return $item['translation_id'];
        }, $translationTestIds)));
        
        $luids = $this->getLuids($translationIds);
        
        foreach ($translationTestIds as $k=>$translationTestId) {
            $testId = $translationTestId['id'];
            $translationId = $translationTestId['translation_id'];

            $translationTestIds[$k]['index'] = $this->getTestIndex($testId, $translationId);
            $translationTestIds[$k]['luid'] = $luids[array_search($translationId, $translationIds)];
        }
        return $translationTestIds;
    }


    function getGapFillWithOneAnswer($res) { // check gap fill number of answers in clozes
        $testIds = array_values(array_unique($this->filter($res, function($rec) {
            return substr_count($rec->cloze_answer, ';') === 0;
        }, 'test_id', false)));

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getGapFillWithoutPlaceholders($res) { // check gap fill with no cloze placeholders
        $translationTestIds = $this->filter($res, function($rec) {
            return substr_count($rec->cloze, '-1-') === 0;
        }, 'translation_id', true);

        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getGapFillWithWrongPlaceholders($res) { // check gap fill with ____ cloze placeholders
        $translationTestIds = $this->filter($res, function($rec) {
            return substr_count($rec->cloze, '___') !== 0;
        }, 'translation_id', true);

        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getMatchingTermsWithWithEmptyAnswer($res) { // check matching terms with empty answer
        $testIds = array_values(array_unique($this->filter($res, function($rec) {
            return !isset($rec->pair_one) || !isset($rec->pair_two) || !trim($rec->pair_one) || !trim($rec->pair_two);
        }, 'test_id', false)));

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getJumbleTextWithWithEmptyAnswer($res) { // check jumble text with empty answer
        $testIds = array_values(array_unique($this->filter($res, function($rec) {
            return !isset($rec->answer) || !trim($rec->answer);
        }, 'test_id', false)));

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getMatchingTermsWithWrongPlaceholders($res) { // check matching terms with ____ in pair
        $testIds = array_values(array_unique($this->filter($res, function($rec) {
            return  substr_count($rec->pair_one, '___') !== 0 ||
                    substr_count($rec->pair_two, '___') !== 0;
        }, 'test_id', false)));

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getWithShortDescription($res, $len) { // check description field content length
        $translationTestIds = $this->filter($res, function($rec) use ($len) {
            return strlen($rec->description) < $len;
        }, 'translation_id', true);

        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    /**
     * Filters tests depending on a condition
     * @param {array} res the tests' data
     * @param {function} condition the condition
     */
    function filter2($res, $condition) {
        $check = array();
        foreach ($res as $rec) {
            if (!isset($check[$rec->test_id])) {
                $check[$rec->test_id] = 0;
            }
            if ($condition($rec)) {
                $check[$rec->test_id]++;
            }
        }

        $testIds = array();
        foreach ($check as $id=>$n) {
            if (!$n) {
                $testIds[] = $id;
            }
        }

        return $testIds;
    }


    function getMultipleChoiceWithoutCorrect($res) { // check multiple choice without correct answers
        $testIds = $this->filter2($res, function($rec) {
            return +$rec->is_correct === 1;
        });

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }


    function getMultipleChoiceWithEmptyAnswer($res) { // check multiple choice with empty answers
        $testIds = $this->filter2($res, function($rec) {
            return $rec->answer !== null;
        });

        $translationTestIds = $this->getTranslationIds($testIds);
        return $this->bindTestIndexAndLuid($translationTestIds);
    }

    



}
