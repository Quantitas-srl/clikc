<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * A cluster.
 */
class Cluster extends Model {

    use HasFactory;
    protected $table = "cluster";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $id;
    
    
    /**
     * The constructor
     * @param {number} id the id of this cluster
     */
    function __construct($id = null) {
        $this->id = $id;
    }


    /**
     * Returns all the available clusters.
     * If user id is specified, returns the choices user made on /startup page for each cluster
     * @param {number} uid the id of the user, optional
     */
    function getAll($uid = null) {
        if (isset($uid)) {
            return DB::select(
                ' SELECT c.*, ucs.skill_value, ucs.use_for_startup ' . 
                ' FROM cluster c left join user_cluster_skill ucs on c.id=ucs.cluster_id ' . 
                ' AND ucs.user_id = ' . $uid
            );
        }
        return DB::select('SELECT * FROM cluster c');
    }


    /**
     * Returns a cluster with the user's skills and preferences
     */
    function get($uid, $clusterId) {
        $res = DB::select('SELECT id, skill_value FROM user_cluster_skill WHERE user_id = ' . $uid . ' AND cluster_id = ' . $clusterId);
        if (count($res)) {
            return $res[0];
        }
        return false;
    }


    /**
     * Sets the skill value for a cluster
     * @param {number} uid the id of the user
     * @param {number} clusterId the id of the cluster
     * @param {number} skillValue the value of the skill ([1,2,3])
     */
    function set($uid, $clusterId, $skillValue) {
        $current = $this->get($uid, $clusterId);
        if ($current) {
            DB::update('UPDATE user_cluster_skill SET skill_value = ' . $skillValue . ' WHERE user_id = ' . $uid . ' AND cluster_id = ' . $clusterId);
        } else {
            DB::insert('INSERT INTO user_cluster_skill (user_id, cluster_id, skill_value, use_for_startup) VALUES (' . $uid . ', ' . $clusterId . ', ' . $skillValue . ', 0)');
        }
    }

    
    /**
     * Sets the preference for a cluster
     * @param {number} uid the id of the user
     * @param {number} clusterId the id of the cluster
     * @param {number} value the value of the use_for_startup attribute ([0,1])
     */
    function setPreference($uid, $clusterId, $value) {
        DB::update('UPDATE user_cluster_skill SET use_for_startup = ' . $value . ' WHERE user_id = ' . $uid . ' AND cluster_id = ' . $clusterId);
    }

}
