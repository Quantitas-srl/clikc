<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * A class representing a learning unit,
 * both normal and labour market.
 */
class LearningUnit extends Model {

    use HasFactory;
    protected $table = "learning_unit";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $id;
    protected $isLabourMarket;
    

    /**
     * The constructor
     * @param {number} id the id of this learning unit
     * @param {boolean} isLabourMarket true if the learning unit is of type labour market
     */
    function __construct($id = null, $isLabourMarket = false) {
        $this->id = $id;
        $this->isLabourMarket = $isLabourMarket;
    }


    /**
     * Gets the content of this learning unit
     * @param {boolean} light true to return only information relevant to the IA-next page
     */
    public function get($light = false) {
        if ($this->isLabourMarket) {
            $sql = $light ?
                'SELECT id, title FROM learning_unit_labour_market WHERE id = ' . $this->id :
                'SELECT id, title, content FROM learning_unit_labour_market WHERE id = ' . $this->id;
            $data = DB::select($sql);
            if (count($data)) {
                return $data;
            }
            return null;
        } else {
            $sql = $light ?
                'SELECT lu.id, lu.cluster_number, t.id as translations_id, t.title, t.subtitle from learning_unit lu left join translation t on lu.id=t.learning_unit_id where lu.id=' . $this->id :
                'SELECT lu.id, lu.identifier, lu.cluster_number, lu.skill, lu.eqf_level, lu.clil_sore_unit_link, t.id as translations_id, t.title, t.subtitle, t.competence, t.cluster_title, t.keywords, t.introduction, t.text_area from learning_unit lu left join translation t on lu.id=t.learning_unit_id where lu.id=' . $this->id;
            $learning_unit = DB::select($sql);
            if (count($learning_unit)) {
                $learning_unit = $learning_unit[0];
                $translations_id = $learning_unit->translations_id;
                $dinamic_fields = DB::select('SELECT * FROM dinamic_field where translations_id = ' . $translations_id . ' order by orders asc');
                return [$learning_unit, $translations_id, $dinamic_fields];
            }
            return [null, null, null];
        }
    }


    /**
     * Checks if a learning unit exists
     * @returns {boolean} true if the learning unit exists, false otherwise
     */
    public function exists() {
        $learning_unit = DB::select(
            'SELECT id FROM ' . 
            ($this->isLabourMarket ? 'learning_unit_labour_market' : 'learning_unit') . 
            ' WHERE id = ' . $this->id
        );
        return count($learning_unit) > 0;
    }


    /**
     * Counts learning units by cluster
     * @returns {number} the amount of learning units by cluster
     */
    public function countByCluster() {
        $res = DB::select('SELECT cluster_number, count(*) as n FROM learning_unit GROUP BY cluster_number');
        return $res;
    }


}
