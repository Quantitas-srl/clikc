<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * A cluster.
 */
class Config extends Model {

    use HasFactory;
    protected $table = "config";
    
    
    /**
     * The constructor
     */
    function __construct() {
    }


    /**
     * Gets the configuration
     */
    function getConfig() {
        $res = DB::select('SELECT * FROM config');
        $data = array();
        foreach ($res as $rec) {
            $data[$rec->key] = $rec->value;
        }
        return $data;
    }


    /**
     * Gets the configuration for a value
     * @param {string} key the key
     */
    function getValue($key) {
        return DB::select('SELECT value FROM config WHERE `key`="' . $key . '"')[0];
    }

}
