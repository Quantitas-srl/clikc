<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;


/**
 * A class for users.
 */
class UserData extends Model {

    use HasFactory;
    protected $table = "user_data";
    protected $primaryKey = "id";
    public $incrementing = true;
    protected $id;
    

    /**
     * The constructor
     * @param {number} id the id of the user
     */
    
    function __construct($id) {
        $this->id = $id;
    }


    /**
     * Returns the skills and the preferences for the clusters for this user
     * @return {object} the skills and the preferences of this user
     */
    function getClusterSkills() {
        return DB::select('SELECT * FROM user_cluster_skill WHERE user_id = ' . $this->id);
    }


    /**
     * Returns user's email
     */
    function getEmail() {
        $res = DB::select('SELECT email FROM users WHERE id = ' . $this->id);
        if (isset($res) && $res && count($res)) {
            return $res[0]->email;
        }
        return '';
    }


    /**
     * Checks if current user is a super user
     * @return {boolean} true if the user is super user
     */
    function isSuperUser() {
        if (!$this->id) {
            return false;
        }
        $res = DB::select('SELECT superuser FROM user_data WHERE user_id = ' . $this->id);
        if (count($res)) {
            return +$res[0]->superuser === 1;
        }
        return false;
    }


    /**
     * Checks whether this user has data or not
     * @return {number} the id of the data if the user exists, -1 otherwise
     */
    function hasData() {
        $res = DB::select('SELECT id FROM user_data WHERE user_id = ' . $this->id);
        if (count($res)) {
            return $res[0]->id;
        }
        return -1;
    }


    /**
     * Creates a new row into the table data for the user
     * @param {number} uid the id of the user
     */
    function createNewUserData($uid) {
        DB::insert('INSERT INTO user_data (user_id) VALUES (' . $uid . ')');
    }


    /**
     * Updates this user's data
     * @param {array} data the user's data
     */
    function updateData($data) {
        $dataId = $this->hasData();
        if ($dataId < 0) {
            $this->createNewUserData($this->id);
            $dataId = $this->hasData();
        }

        $name = $data->name;
        $surname = $data->surname;
        $terms = $data->{'terms-and-conditions'};
        $profi = $data->{'data-profiling'};

        $otherData = array_filter((array)$data, fn($key) => array_search($key, array('name', 'surname', 'terms-and-conditions', 'data-profiling')) === false, ARRAY_FILTER_USE_KEY);


        $sql = 'UPDATE user_data SET name=:name, surname=:surname, terms_and_conditions=:terms, data_profiling=:profi, other=:other ' .
            ' WHERE id = ' . $dataId;
        DB::update(DB::raw($sql), array(
            'name' => $name,
            'surname' => $surname,
            'terms' => $terms,
            'profi' => $profi,
            'other' => json_encode($otherData)
        ));

        return 1;
    }


    /**
     * Returns the data of this user
     * @return {array} the data of this user
     */
    function getData() {
        return DB::select('SELECT * FROM user_data WHERE user_id = ' . $this->id);
    }


    /**
     * Returns the last interaction date of the this user
     * @return {string} the date of the last interaction
     */
    function getLastInteractionTime() {
        $nullDate = '0000-00-00 00:00:00';

        // check login time
        $lastLoginDate = $nullDate;
        $lastLoginData = DB::select('SELECT timestamp FROM user_login WHERE user_id = ' . $this->id . ' ORDER BY timestamp DESC limit 1');
        if (isset($lastLoginData) && $lastLoginData && count($lastLoginData)) {
            $lastLoginDate = $lastLoginData[0]->timestamp;
        }

        // check started, completed and test completed data of LUs
        $lastLUDate = $nullDate;
        $lastLUData = DB::select('SELECT started_on, completed_on, test_completed_on FROM user_learning_unit_tracker WHERE user_id = ' . $this->id . ' ORDER BY id desc limit 1');
        if (isset($lastLUData) && $lastLUData && count($lastLUData)) {
            $started_on = $lastLUData[0]->started_on;
            $completed_on = $lastLUData[0]->completed_on ?? $nullDate;
            $test_completed_on = $lastLUData[0]->test_completed_on ?? $nullDate;
            $lastLUDate = max(max(max($lastLUDate, $started_on), $completed_on), $test_completed_on);
        }

        // check submitted of tests
        $lastTestDate = $nullDate;
        $lastTestData = DB::select('SELECT submitted_on FROM user_test_tracker WHERE user_id = ' . $this->id . ' ORDER BY submitted_on desc limit 1');
        if (isset($lastTestData) && $lastTestData && count($lastTestData)) {
            $lastTestDate = $lastTestData[0]->submitted_on;
        }

        // check started and completed of recap tests
        $lastRecapDate = $nullDate;
        $lastRecapData = DB::select('SELECT started_on, completed_on FROM user_recap_test_tracker WHERE user_id = ' . $this->id . ' ORDER BY id desc limit 1');
        if (isset($lastRecapData) && $lastRecapData && count($lastRecapData)) {
            $started_on = $lastRecapData[0]->started_on;
            $completed_on = $lastRecapData[0]->completed_on ?? $nullDate;
            $lastRecapDate = max(max($lastRecapDate, $started_on), $completed_on);
        }

        // check recommendations
        $lastIADate = $nullDate;
        $lastIAData = DB::select('SELECT recommended_on FROM user_recommendation WHERE user_id = ' . $this->id . ' ORDER BY recommended_on desc limit 1');
        if (isset($lastIAData) && $lastIAData && count($lastIAData)) {
            $lastIADate = $lastIAData[0]->recommended_on;
        }
        
        $lastInteractionDate = max($nullDate, $lastLoginDate);
        $lastInteractionDate = max($lastInteractionDate, $lastLUDate);
        $lastInteractionDate = max($lastInteractionDate, $lastTestDate);
        $lastInteractionDate = max($lastInteractionDate, $lastRecapDate);
        $lastInteractionDate = max($lastInteractionDate, $lastIADate);

        return strtotime($lastInteractionDate);
    }


    /**
     * Inserts a new user
     * @param {string} name the name
     * @param {string} email the email
     * @param {string} pw the pw
     */
    function insertNewUser($name, $email, $pw) {
        $hash = Hash::make($pw);
        $sql = 'INSERT INTO users (name, email, password) VALUES (:name, :email, :password)';
        DB::insert(DB::raw($sql), array(
            'name' => $name,
            'email' => $email,
            'password' => $hash
        ));
    }

}
