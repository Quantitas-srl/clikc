<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\UserData;

class VerifySuperUser
{

    /**
     * Handles an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!(new UserData(Auth::id()))->isSuperUser()) {
            return redirect('switch-to-basic-user');
        }
        return $next($request);
    }
}
