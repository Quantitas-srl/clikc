<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LearningUnit;
use App\Models\LearningUnitTracker;
use App\Models\Test;
use App\Models\Cluster;
use App\Models\UserData;


/**
 * Controller for registry of learning units visited by users.
 */
class RegistryController extends Controller {


    /**
     * Shows the registry
     * @param {int} uid the user's id. If null uses current logged-in user.
     */
    public function showRegistry($uid = null) {
        $userData = (new UserData(isset($uid) ? $uid : $this->getUid()))->getData();
        return view('pages.registry', [
                    'data' => $this->list(),
                    'username' => $userData[0]->name . ' ' . $userData[0]->surname
                ]);
    }


    /**
     * Returns the learning units visited by the user
     * @param {int} uid the user's id. If null uses current logged-in user.
     * @return {array} the learning units data
     */
    public function list($uid = null) {
        $clusters = array();
        foreach ((new Cluster())->getAll() as $rawDatum) {
            $clusters[$rawDatum->id] = $rawDatum->description;
        }
        $list = (new LearningUnitTracker(isset($uid) ? $uid : $this->getUid(), null, null))->list();
        foreach ($list as $k=>$item) {
            if (isset($item->cluster_number)) {
                $list[$k]->cluster = $clusters[$item->cluster_number];
                $list[$k]->labour_market = false;
            } else {
                $list[$k]->cluster = 'Labour market';
                $list[$k]->labour_market = true;
            }

            $list[$k]->started_on_ago = $this->getAgo($item->started_on);
            $list[$k]->completed_on_ago = $this->getAgo($item->completed_on);
        }
        return $list;
    }


    /**
     * Gets a date in ago format
     * @params {string} a date in string format
     */
    private function getAgo($date) {
        if (!trim($date)) {
            return '';
        }
        $time = strtotime($date);
        $now = time();
        $td = $now - $time;
        $mins = floor($td / (60));
        $hours = floor($td / (60*60));
        $days = floor($td / (60*60*24));
        if ($mins < 60) {
            return $mins . ' minutes ago';
        } else if ($hours < 24) {
            return $hours . ' hours ago';
        } else if ($days < 5) {
            return $days . ' days ago';
        }
        return date('d-m-Y', $time) . '<br />' . date(' H:i', $time);
    }

}
