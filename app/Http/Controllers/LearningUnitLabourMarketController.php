<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LearningUnit;
use App\Models\LearningUnitTracker;
use App\Models\Test;
use App\Models\TestTracker;
use App\Models\UserData;


/**
 * Controller for learning units.
 */
class LearningUnitLabourMarketController extends Controller {


    /**
     * Shows a labour market learning unit
     * @param {number} id the id of the learning unit
     */
    public function showLearningUnitLabourMarket($id) {
        $uid = $this->getUid();
        $U = new UserData($uid);
        
        $LU = new LearningUnit($id, true);
        if ($LU->exists()) {

            $LUT = new LearningUnitTracker($uid, $id, true);
            $isInHistory = $LUT->isInHistory();

            if ($isInHistory) {
                !$LUT->isStarted() && ($LUT->setStarted());
                $userStatus = $LUT->getUserStatus();
                return view('pages.learning-unit-labour-market', [
                    'data' => $LU->get()[0],
                    'user_status' => $userStatus,
                    'editable' => !isset($userStatus->completed_on),
                    'isSuperUser' => $U->isSuperUser()
                ]);
            } else {
                return view('pages.learning-unit-unavailable');
            }

        } else {
            return view('pages.learning-unit-not-found');
        }
    }


    /**
     * Shows the completed-tests page for a learning unit
     * @param {number} id the id of the learning unit
     */
    public function confirmLearningUnitLabourMarketCompleted($id) {
        $uid = $this->getUid();
        
        if ($uid > -1) {
            $LU = new LearningUnit($id, true);

            if ($LU->exists()) {
                $LUT = new LearningUnitTracker($uid, $id, true);
                !$LUT->isCompleted() && ($LUT->setCompleted());
                
                return view('pages.learning-unit-labour-market-completed', [
                    'pendingId' => $LUT->getPending($uid)
                ]);
            }
        }
    }

}
