<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Models\UserData;


/**
 * A generic controller.
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $superUser = false;


    /**
     * The constructor
     */
    function __construct() {
        date_default_timezone_set('Europe/Rome');
        $this->superUser = $this->isSuperUser();
    }

    /**
     * Gets the user id
     * @return {number} the user id
     */
    protected function getUid() {
        return Auth::id();
    }

    /**
     * Gets the user id
     * @return {number} the user id
     */
    protected function getUemail() {
        return Auth::user()->email;
    }

    /**
     * Checks if the current user is a super user
     * @return {boolean} true if the user is a superuser
     */
    function isSuperUser() {
        return (new UserData($this->getUid()))->isSuperUser();
    }
}
