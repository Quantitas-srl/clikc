<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FPDF;
define('FPDF_FONTPATH', '../public/assets/font/');

class PDF extends FPDF {
    
    //protected $master = '';
    
    function __construct() {
        parent::__construct();
        $this->loadFonts();
        $this->SetFont('lexend-deca','',10);
    }
    
    function loadFonts() {
        $this->AddFont('lexend-deca', '', 'LexendDeca-Light.php');
        $this->AddFont('lexend-deca', 'B', 'LexendDeca-Medium.php');
    }
    
    function setMaster($master) {
        //$this->master = $master;
    }
    
    // Page header
    function Header() {
        //if (+$this->PageNo() !== 1) {
            // tiny logo
            $src = 'assets/img/logo-clikc.png';
            $this->Image($src, 10, 5, 12);

            $this->SetFont('lexend-deca', '', 8);
            $this->SetTextColor(180, 180, 180);
            $this->SetXY(20, 5);
        //}
    }

    // Page footer
    function Footer() {
        $this->SetFillColor(255,255,255);
        $this->SetY(-10); // Position at 1.0 cm from bottom
        $this->SetFont('lexend-deca', 'B', 8);
        
        $time = time();
        $date = date('d/m/Y H:i', $time);
        
        // Session code and page number
        $this->SetTextColor(120,120,120);
        $this->SetXY(10, -10);
        $this->Cell(0, 8, 'Page' . ' '.$this->PageNo().' ' . _('of') . ' {nb}' . ' - ' . $date,0,0,'C', true);
        
    }
    function Circle($x, $y, $r, $style='D')
    {
        $this->Ellipse($x,$y,$r,$r,$style);
    }

    function Ellipse($x, $y, $rx, $ry, $style='D')
    {
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $lx=4/3*(M_SQRT2-1)*$rx;
        $ly=4/3*(M_SQRT2-1)*$ry;
        $k=$this->k;
        $h=$this->h;
        $this->_out(sprintf('%.2F %.2F m %.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x+$rx)*$k,($h-$y)*$k,
            ($x+$rx)*$k,($h-($y-$ly))*$k,
            ($x+$lx)*$k,($h-($y-$ry))*$k,
            $x*$k,($h-($y-$ry))*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x-$lx)*$k,($h-($y-$ry))*$k,
            ($x-$rx)*$k,($h-($y-$ly))*$k,
            ($x-$rx)*$k,($h-$y)*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c',
            ($x-$rx)*$k,($h-($y+$ly))*$k,
            ($x-$lx)*$k,($h-($y+$ry))*$k,
            $x*$k,($h-($y+$ry))*$k));
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c %s',
            ($x+$lx)*$k,($h-($y+$ry))*$k,
            ($x+$rx)*$k,($h-($y+$ly))*$k,
            ($x+$rx)*$k,($h-$y)*$k,
            $op));
    }



    /**
     * Adds a test
     * @param {string} text the text
     * @param {number} fontSize the font size
     * @param {number} text the newline space
     * @return {number} the new y
     */
    function addText($text, $fontSize, $nl) {
        $x = $this->getX();
        $y = $this->getY();
        $this->SetFont('lexend-deca', '', $fontSize);
        $this->Cell(0, 0, $text, 0, 1, 'L', false);
        $y += $nl;
        $this->setXY($x, $y);
        return $y;
    }
    

    /**
     * Adds a h1
     * @param {string} text the text
     */
    function addH1($text) {
        $y = $this->getY();
        $this->setDrawColor(255, 165, 0);
        $this->Line(10, $y+6, 200, $y+6);
        $this->setTextColor(0, 0, 204);
        return $this->addText($text, 20, 18);
    }
    

    /**
     * Adds a h2
     * @param {string} text the text
     */
    function addH2($text) {
        $this->setTextColor(0, 0, 204);
        return $this->addText($text, 14, 9);
    }
    

    /**
     * Adds a h3
     * @param {string} text the text
     * @return {number} the new y
     */
    function addH3($text) {
        $this->setTextColor(0, 0, 204);
        return $this->addText($text, 12, 6);
    }


    /**
     * Adds an image
     * @param {number} x the x position
     * @param {number} y the y position
     * @param {number} width the width
     * @param {number} height the height
     * @param {string} src the src
     * @return {number} the new y
     */
    function addImage($x, $y, $width, $height, $src) {
        $this->Image($src, $x, $y, $width, $height);
        $y += $height + 20;
        $this->setXY(10, $y);
        return $y;
    }


    /**
     * Adds an indicator
     * @param {string} title the title
     * @param {string} value the value
     * @return {number} the new y
     */
    function addIndicator($title, $value) {
        $x = $this->getX();
        $y = $this->getY();
        $this->setTextColor(30, 30, 30);
        $this->SetFont('lexend-deca', '', 8);
        $this->Cell(0, 8, $title, 0, 1, 'L', false);
        $this->setXY($x, $y + 4);
        $value = str_replace('<br>', ' ', $value);
        $this->SetFont('lexend-deca', 'B', 8);
        $this->Cell(0, 8, $value, 0, 1, 'L', false);
        return $y;
    }


    /**
     * Adds a progress bar
     * @param {number} x the x position
     * @param {number} y the y position
     * @param {number} width the width
     * @param {number} height the height
     * @param {string} text the text
     * @param {string} src the src
     * @return {number} the new y
     */
    function addProgressBar($x, $y, $width, $height, $text, $src) {
        $this->setXY($x, $y);
        $this->SetFont('lexend-deca', 'B', 8);
        $this->setTextColor(255, 165, 0);
        if ($text) {
            $this->Cell($width, 0, $text, 0, 0, 'C');
            $y += 3;
        }
        $this->Image($src, $x, $y, $width, $height);
        $y += $height + 15;
        $this->setXY($x, $y);
        return $y;
    }


}