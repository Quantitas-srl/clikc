<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cluster;
use App\Models\Test;
use App\Models\TestTracker;
use App\Models\LearningUnitTracker;


/**
 * Controller for a test.
 */
class TestController extends Controller {

    private $data;
    private $type;
    const TITLE = array(
        'gap-fill' => 'Gap fill',
        'multiple-choice' => 'Multiple choice',
        'matching-terms' => 'Matching terms',
        'jumble-text' => 'Jumble text'
    );


    /**
     * The constructor
     * @param {array} data the data of the test, optional
     * @param {array} savedData the data saved from user submission, optional
     */
    function __construct($data = null, $savedData = null) {
        if (isset($data)) {
            $this->data = $data;
            $this->id = $data['id'];
            $this->type = $data['type'];
            $this->savedData = $savedData;
        }
    }


    /**
     * Returns a test
     * @param {number} id the id of the test
     * @param {string} type the type of the test
     * @return {array} an array with data (the general data of a test - table tests)
     * and customData (specific for that test - e.g. table gap_fill_answers)
     */
    function get($id, $type) {
        $T = new Test($id);
        $data = $T->get();
        $customData = $T->getCustomData($type);
        return array(
            'data' => $data,
            'customData' => $customData
        );
    }

    
    /**
     * Prepares data for a recap test
     * @param {number} count how many single tests to use for the recap
     */
    function recap($count = 10) {

        // these array will be returned
        $randomTestsData = array();
        $savedTestsData = array();

        // get the user id
        $uid = $this->getUid();

        // check if there is a pending recap test
        $TT = new TestTracker($uid);
        $pendingId = $TT->getPendingRecapId();

        $message = '';

        $nRecaps = +$TT->countRecaps();
        if (!isset($pendingId)) { // a new recap test has to be created
            $nRecaps++; // so this new is the next
        }
        $nRecapsLiteral = '';
        if ($nRecaps === 1) {
            $nRecapsLiteral = 'first';
        } else if ($nRecaps === 1) {
            $nRecapsLiteral = 'second';
        } else if ($nRecaps === 1) {
            $nRecapsLiteral = 'third';
        } else if ($nRecaps === 1) {
            $nRecapsLiteral = 'fourth';
        } else {
            $nRecapsLiteral = $nRecaps . '-th';
        }

        if (isset($pendingId)) { // if there is a pending recap test, show it
            $randomTestsData = $TT->getRecap($pendingId); // get data
            $tids = array_keys($randomTestsData);

            $TT = new TestTracker();
            $rawsavedTestsData = $TT->get($uid, $tids, true);

            foreach ($rawsavedTestsData as $rawsavedTestsDatum) { // prepare date
                $submitted_data = json_decode($rawsavedTestsDatum->submitted_data);
                $submittedData = $submitted_data->submittedData;
                $testId = $submitted_data->data->data[0]->id;
                $savedTestsData[$testId] = $submittedData;
            }

        } else { // no pending recap, create new one, if can create

            $LUT = new LearningUnitTracker($uid, null, false);
            $streak = $LUT->getLastLearningUnitStreak(10, false); // get the 10 last learning units

            $lastRecap = $TT->getLastRecap();
            $lastRecap_after_learning_unit_id = -1;
            $streakLuids = array();
            if (count($lastRecap)) {
                $lastRecap_after_learning_unit_id = $lastRecap[0]->after_learning_unit_id;
                $streakLuids = array_map(function($item) {
                    return $item->learning_unit_id;
                }, $streak);
            }


            if (true || array_search($lastRecap_after_learning_unit_id, $streakLuids) === false) { // can create new

                $T = new Test();
                $inserted = array();
                $stop = 10;
                while ($stop > 0 && count($inserted) < $count) {
                    foreach ($streak as $item) { // pick a test from each learning unit
                        if (count($inserted) < $count) {
                            $data = $T->randomlyGetFromLearningUnit($item->learning_unit_id); // randomly get a test from the learning unit
                            if (isset($data)) {
                                if (isset($data) && array_search($data['id'], $inserted) === false) {
                                    $randomTestsData[$data['id']] = $data;
                                    $inserted[] = $data['id'];
                                }
                            }
                        }
                    }
                    // if, for some reason, here there isn't the desired number of tests,
                    // loop and pick again some tests from the same set of learning units
                    $stop--; // anyway prevent infinite loop
                }

                $TT->insertRecap(reset($streak)->learning_unit_id, $randomTestsData);
            } else {
                $message = 'A recap test cannot be created in this moment.';
            }

        }

        return view('pages.recap-test', 
            [
                'testsData' => $randomTestsData,
                'savedTestsData' => $savedTestsData,
                'message' => $message,
                'nRecapsLiteral' => $nRecapsLiteral
            ]);
    }


    /**
     * Shows the it's recap time page
     */
    function recapMessage() {
        return view('pages.recap-test-time');
    }


    /**
     * Sets the pending recap test as completed
     */
    function setRecapCompleted() {
        $uid = $this->getUid();
        $T = new Test();
        $TT = new TestTracker($uid);
        $pendingId = $TT->getPendingRecapId();
        if (isset($pendingId)) {
            $TT->setRecapAsCompleted($pendingId);
            //return view('pages.recap-test-completed');
        }
        $lastRecap = $TT->getLastRecap();
        if (count($lastRecap)) {
            $lastRecapTestIds = array_keys($TT->getRecap($lastRecap[0]->id));
            $lastRecapTestData = $TT->get($uid, $lastRecapTestIds, true);
            $accuraciesSum = array_reduce($lastRecapTestData, function($accum, $item) {
                return $accum + +$item->accuracy;
            }, 0);
            $avgAccuracy = $accuraciesSum / count($lastRecapTestIds);
            $score = round($avgAccuracy*100);
            $score < 0 && ($score = 0);
            $score > 100 && ($score = 100);
            
            $luData = $T->getLearningUnitData($lastRecapTestIds);

            $clusters = array();
            foreach ((new Cluster())->getAll() as $cluster) {
                $clusters[$cluster->id] = $cluster->description;
            }

            $barsData = array();
            foreach ($luData as $luDatum) {
                $clu = $clusters[$luDatum->cluster_number];
                if (!isset($barsData[$clu])) {
                    $barsData[$clu] = 0;
                }
                $barsData[$clu]++;
            }

            $recaps = +$TT->countRecaps();
            $literalRecap = '';
            if ($recaps === 1) {
                $literalRecap = 'first';
            } else if ($recaps === 2) {
                $literalRecap = 'second';
            } else if ($recaps === 3) {
                $literalRecap = 'third';
            } else if ($recaps === 4) {
                $literalRecap = 'fourth';
            } else {
                $literalRecap = $recaps . '-th';
            }
            
            $recapTitle = '';
            $recapSubtitle = '';
            $recapSubtitle2 = '';
            if ($score >= 70) {
                if ($recaps === 1) {
                    $recapTitle = 'Congratulations!';
                    $recapSubtitle = 'You\'ve just earned your first trophy!';
                    $recapSubtitle2 = 'and you now know a little bit more about:';
                } else {
                    $recapTitle = 'You\'re on fire!';
                    $recapSubtitle = 'You\'ve just earned your ' . $literalRecap . ' trophy!';
                    $recapSubtitle2 = 'and you now know a little bit more about:';
                }
            } else if ($score >= 60) {
                $recapTitle = 'You\'re getting there!';
                $recapSubtitle = 'You\'ve passed the test, but didn\'t earn a trophy';
                $recapSubtitle2 = 'and you now know a little bit more about:';
            } else {
                $recapTitle = 'Oh, snap!';
                $recapSubtitle = 'You didn\'t pass the recap test!';
                $recapSubtitle2 = 'but don\'t worry, you\'ve still learned something about:';
            }

            return view('pages.recap-test-completed', [
                            'score' => $score,
                            'barsData' => $barsData,
                            'recaps' => $recaps,
                            'literalRecap' => $literalRecap,
                            'recapTitle' => $recapTitle,
                            'recapSubtitle' => $recapSubtitle,
                            'recapSubtitle2' => $recapSubtitle2
                        ]); // debug (or not?)
        }
        return redirect('/ia/next');
    }


    /**
     * Prints a test
     * @param {number} n the progressive number of the test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     */
    function show($n, $isSuperUserViewing) {
        ?>
        <div class="test" data-type="<?=$this->type?>" data-id="<?=$this->id?>" completed="<?=isset($this->savedData)?'true':'false'?>">
            <?=$this->showTitle($n)?>
            <p class="test-question">
                <?=$this->data['question']?>
            </p>
            <?=$this->showContent($isSuperUserViewing)?>
            <?php if (!$isSuperUserViewing && !isset($this->savedData)) { ?>
                <?=$this->showSubmit()?>
            <?php } ?>
        </div>
        <?php
    }


    /**
     * Prints the title of a test
     * @param {number} n the progressive number of the test
     */
    private function showTitle($n) {
        ?>
            <div>
                <div class="test-type">
                    <span class="test-number"><?=$n?>)</span>
                    <span><?=TestController::TITLE[$this->type]?></span>
                </div>
                <div class="test-hint">
                    <?=$this->data['description']?>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php
    }
    

    /**
     * Prints the submit buttons, alongside with confirm and cancel buttons
     */
    private function showSubmit() {
        $disabled = '';
        if ($this->type === 'gap-fill' || $this->type === 'matching-terms') {
            $disabled = 'disabled';
        }
        ?>
        <div class="test-submit-container" data-id="<?=$this->id?>">
            <button class="button secondary test-submit" <?=$disabled?> data-id="<?=$this->id?>" data-type="<?=$this->type?>">
                Submit
            </button>
            <div class="test-submit-confirm-message hidden" data-id="<?=$this->id?>">
                <p>
                    Are you sure?
                </p>
                <button class="button secondary test-submit-confirm-submit" data-id="<?=$this->id?>" data-type="<?=$this->type?>">
                    Yes, submit
                </button>
                &nbsp;
                <button class="button secondary cancel test-submit-cancel-submit" data-id="<?=$this->id?>" data-type="<?=$this->type?>">
                    No, cancel
                </button>
            </div>
            <div class="waiter hidden" data-id="<?=$this->id?>"> <!-- animation -->
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <?php
    }


    /**
     * Prints the content of a test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     * @see showGapFill
     * @see showMultipleChoice
     * @see showMatchingTerms
     * @see showJumbleText
     */
    private function showContent($isSuperUserViewing) {
        ?>
        <?php
        if ($this->type === 'matching-terms' || $this->type === 'jumble-text') {
            ?>
            <small class="mobile-device-hint">
                On mobile device long press an item to select it.
            </small>
            <?php
        }
        ?>
        <div class="test-content">
            <?php
            if ($this->type === 'gap-fill') {
                $this->showGapFill($isSuperUserViewing);
            } else if ($this->type === 'multiple-choice') {
                $this->showMultipleChoice($isSuperUserViewing);
            } else if ($this->type === 'matching-terms') {
                $this->showMatchingTerms($isSuperUserViewing);
            } else if ($this->type === 'jumble-text') {
                $this->showJumbleText($isSuperUserViewing);
            }
            ?>
        </div>
        <?php
    }

    
    /**
     * Prints a jumble text test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     */
    private function showJumbleText($isSuperUserViewing) {
        $shuffled = $this->data['custom'];
        shuffle($shuffled);
        foreach ($this->data['custom'] as $k=>$answer) {
            [$correctness, $draggableK2, $draggableAnswer2] = $this->getJumbleTextCorrectnessInfo($k, $answer);
            ?>
            <div class="droppable <?=$correctness?> inline-droppable" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="leaveDrop(event)" data-id="<?=$this->id?>">
                <?php
                if ($draggableAnswer2 || $isSuperUserViewing) { // use saved data
                    $this->printJumbleTextDraggable($draggableK2, $draggableAnswer2, 'false', $correctness);
                } else { // no saved data
                    $this->printJumbleTextDraggable($k, $shuffled[$k], 'true', '');
                }
                ?>
            </div>
            <?php
        }
        $this->printJumbleTextSolution();
    }


    /**
     * Gets info about correctness of a jumble text test saved data
     * @param {number} k an id
     * @param {object} answer an answer
     * @return {array} info about correctness
     */
    private function getJumbleTextCorrectnessInfo($k, $answer) {
        $correctness = '';
        $draggableK2 = '';
        $draggableAnswer2 = '';
        if (isset($this->savedData)) {
            $md5 = $this->savedData[$k];
            foreach ($this->data['custom'] as $k2=>$answer2) {
                $md52 = md5($answer2->answer);
                if ($md5 === $md52) {
                    $draggableK2 = $k2;
                    $draggableAnswer2 = $answer2;
                    $correctness = ' wrong ';
                    if ($md52 === md5($answer->answer)) {
                        $correctness = ' correct ';
                    }
                }
            }
        }
        return [$correctness, $draggableK2, $draggableAnswer2];
    }


    /**
     * Prints a jumble text test solution
     */
    private function printJumbleTextSolution() {
        ?>
        <div class="test-solution">
            <?php
            if (isset($this->savedData)) {
                echo '| ';
                foreach ($this->data['custom'] as $k=>$answer) {
                    echo $answer->answer . ' | ';
                }
                ?>
                
            <?php } ?>
        </div>
        <?php
    }


    /**
     * Prints a draggable for a jumble text test
     * @param {number} k an identifier
     * @param {object} answer the answer
     * @param {string} draggable the value for the draggable attribute
     * @param {string} correct appended to class list (e.g. "correct", "wrong")
     */
    private function printJumbleTextDraggable($k, $answer, $draggable, $correct) {
        ?>
        <div id="value-<?=$this->id?>-<?=$k?>" class="draggable <?=$correct?>" draggable="<?=$draggable?>" ondragstart="drag(event)" ondrag="dragging(event)" data-id="<?=$this->id?>" data-my-id="<?=md5($answer->answer)?>">
            <?=$answer->answer?>
        </div>
        <?php
    }

    
    /**
     * Prints a matching terms test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     */
    private function showMatchingTerms($isSuperUserViewing) {
        foreach ($this->data['custom'] as $k=>$pair) {
            [$correctness, $draggableK2, $draggablePair2] = $this->getMatchingTermsCorrectnessInfo($k, $pair);
            ?>
            <div class="pair" data-id="<?=$this->id?>">
                <p class="pair-key" data-my-id="<?=md5($pair->pair_one)?>"><?=$pair->pair_one?></p>
                <div class="droppable <?=$correctness?>" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="leaveDrop(event)" data-id="<?=$this->id?>">
                    <?php
                    if ($draggablePair2) {
                        $this->printMatchingTermsDraggable($draggableK2, $draggablePair2, 'false', $correctness);
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        if (!isset($this->savedData) && !$isSuperUserViewing) {
            shuffle($this->data['custom']);
            ?>
            <div class="draggables-list" data-id="<?=$this->id?>">
                <?php
                foreach ($this->data['custom'] as $k=>$pair) {
                    $this->printMatchingTermsDraggable($k, $pair, 'true', '');
                }
                ?>
            </div>
            <?php
        }
        $this->printMatchingTermsSolution();
    }


    /**
     * Gets info about correctness of a matching terms test saved data
     * @param {number} k an id
     * @param {object} pair a pair
     * @return {array} info about correctness
     */
    private function getMatchingTermsCorrectnessInfo($k, $pair) {
        $correctness = '';
        $draggablePair2 = '';
        $draggableK2 = '';
        if (isset($this->savedData)) {
            $md5 = explode(':', $this->savedData[$k])[1];
            foreach ($this->data['custom'] as $k2=>$pair2) {
                $md52 = md5($pair2->pair_two);
                if ($md5 === $md52) {
                    $draggableK2 = $k2;
                    $draggablePair2 = $pair2;
                    $correctness = 'wrong';
                    if ($md52 === md5($pair->pair_two)) {
                        $correctness = 'correct';
                    }
                }
            }
        }
        return [$correctness, $draggableK2, $draggablePair2];
    }


    /**
     * Prints a jumble text test solution
     */
    private function printMatchingTermsSolution() {
        ?>
        <div class="test-solution">
            <?php
            if (isset($this->savedData)) { ?>
                <?php foreach ($this->data['custom'] as $k=>$pair) {
                    ?>
                    <?=$pair->pair_one?> | <?=$pair->pair_two?> |<br />
                    <?php
                }
            } ?>
        </div>
        <?php
    }


    /**
     * Prints a draggable for a matching terms test
     * @param {number} k an identifier
     * @param {object} pair the pair
     * @param {string} draggable the value for the draggable attribute
     * @param {string} correct appended to class list (e.g. "correct", "wrong")
     */
    private function printMatchingTermsDraggable($k, $pair, $draggable, $correct) {
        ?>
        <div id="value-<?=$this->id?>-<?=$k?>" data-my-id="<?=md5($pair->pair_two)?>" class="draggable <?=$correct?>" draggable="<?=$draggable?>" ondragstart="drag(event)" ondrag="dragging(event)" data-id="<?=$this->id?>">
            <?=$pair->pair_two?>
        </div>
        <?php
    }


    /**
     * Prints a multiple choice test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     */
    private function showMultipleChoice($isSuperUserViewing) {
        $disabled = isset($this->savedData) ? ' disabled="true" ' : '';
        if ($isSuperUserViewing) {
            $disabled = 'disabled';
        }
        $expectedCorrect = array_reduce($this->data['custom'], function($accumulator, $item) {
            return $accumulator + +$item->is_correct;
        });
        ?>
        <?php /* <p class="multiple-choice-n-answers">(<?=$expectedCorrect?> possible answer<?=$expectedCorrect>1?'s':''?>)</p> */ ?>
        <input type="hidden" value="<?=$expectedCorrect?>" class="expected-correct" data-id="<?=$this->id?>" />
        <?php
        foreach ($this->data['custom'] as $i=>$answer) {
            $checked = '';
            $correct = '';
            if (isset($this->savedData)) {
                $checked = array_search(+$this->data['custom'][$i]->id, $this->savedData) !== false ? ' checked="true" ' : '';
                if ($checked) {
                    $correct = +$this->data['custom'][$i]->is_correct === 1 ? ' correct ' : ' wrong ';
                }
            }
            ?>
            <div class="checkbox-container <?=$correct?>" <?=$checked?> <?=$disabled?> data-id="<?=$this->id?>" data-my-id="<?=$this->data['custom'][$i]->id?>">
                <div class="as-checkbox"></div>
                <label class="multiple-choice-label">
                    <?=$answer->answer?>
                </label>
                <div class="clearfix"></div>
                <input <?=$checked?> <?=$disabled?> type="checkbox" class="hidden" />
            </div>
            <?php
        }
        $this->printMultipleChoiceSolution();
    }


    /**
     * Prints a multiple choice test solution
     */
    private function printMultipleChoiceSolution() {
        ?>
        <div class="test-solution">
            <?php
            if (isset($this->savedData)) { ?>
                <?=$this->data['question']?>
                <?php foreach ($this->data['custom'] as $datum) { 
                    if (+$datum->is_correct === 1) {
                        ?>
                        <br />- <?=$datum->answer?>
                        <?php
                    }
                }
            } ?>
        </div>
        <?php
    }


    /**
     * Prints a gap fill test
     * @param {boolean} isSuperUserViewing true if it's a superuser viewing
     */
    private function showGapFill($isSuperUserViewing) {
        $cloze = $this->data['cloze'];
        $parts = preg_split("/-[0-9]+-/", $cloze);
        if (1 === count($parts)) {
            ?><span class="hidden corr-test"></span><?php
            return false;
        }

        $doTheJob = false;
        $items = array();
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $answs = explode(';', $this->data['custom'][$i]->cloze_answer);
            $answs = array_filter($answs, function($ans) {
                return trim($ans);
            });
            count($answs) === 1 && ($doTheJob = true);
            $items = array_merge($items, $answs);
        }
        if ($doTheJob) {
            $items = array_unique($items);
            $items = implode(';', $items);
            for ($i = 0; $i < count($parts) - 1; $i++) {
                $this->data['custom'][$i]->cloze_answer = $items;
            }
        }


        for ($i = 0; $i < count($parts) - 1; $i++) {
            ?>
            <span>
                <?=$parts[$i]?>
            </span>
            <?php
            $orderedAnswers = explode(';', $this->data['custom'][$i]->cloze_answer);
            $orderedAnswers = array_map(function($item) {
                return trim($item);
            }, $orderedAnswers);
            $answers = array_filter($orderedAnswers, function($item) {
                return $item !== '';
            });
            shuffle($answers); // no problem if shuffle anyway (savedData or not)
            $correct = '';
            $disabled = '';
            if ($isSuperUserViewing) {
                $disabled = ' disabled="true" ';
            }
            if (isset($this->savedData)) {
                $disabled = ' disabled="true" ';
                if ($this->savedData[$i] === $orderedAnswers[$i/*0*/]) {
                    $correct = ' correct ';
                } else {
                    $correct = ' wrong ';
                }
            }
            ?>
            <div class="form-group inline">
                <select autocomplete="off" <?=$disabled?> class="cloze <?=$correct?>" name="cloze-<?=$this->data['custom'][$i]->id?>" required data-id="<?=$this->id?>">
                    <option value=""></option>
                    <?php
                    foreach ($answers as $n=>$answer) {
                        $selected = '';
                        if (isset($this->savedData)) {
                            if ($answer === $this->savedData[$i]) { //$answer === $orderedAnswers[$i]) {
                                $selected = ' selected ';
                            }
                        }
                        ?>
                        <option value="<?=$n?>" <?=$selected?>><?=$answer?></option>
                        <?php
                    }
                    ?>
                </select>
                <img class="arrow" src="<?=asset('/assets/img/caret-down.svg')?>" alt="Arrow" title="Press to show options" />
            </div>
            <?php
        } ?>
        <span>
            <?=$parts[$i]?>
        </span>
        <?php
        $this->printGapFillSolution($parts);
    }


    /**
     * Prints a multiple choice test solution
     */
    private function printGapFillSolution($parts) {
        ?>
        <div class="test-solution">
            <?php
            if (isset($this->savedData)) {
                for ($i = 0; $i < count($parts) - 1; $i++) { ?>
                    <?=$parts[$i] . ' | '?>
                    <?=explode(';', $this->data['custom'][$i]->cloze_answer)[0] . ' | '?>
                <?php } ?>
                <?=$parts[$i]?>
            <?php } ?>
        </div>
        <?php
    }


    /**
     * Checks test correctness
     * @param {number} id the id of the test
     * @param {string} type the type of the test
     * @param {object} submittedData the data submitted by the user
     * @param {boolean} isRecapTest true if it's a recap test, false otherwise
     * @return {array} info about correctness
     */
    function check($id, $type, $submittedData, $isRecapTest) {

        $data = $this->get($id, $type);
        $customData = $data['customData'];
    
        $correct = 0;
        $expected = 0;
        $correctDetails = array();
        foreach ($customData as $i=>$customDatum) {
            if ($type === 'gap-fill') {
                if (trim($customDatum->cloze_answer)) {
                    $expected++;
                    $answer = explode(';', $customDatum->cloze_answer)[0];
                    if (isset($submittedData[$i]) && $submittedData[$i] === $answer) {
                        $correct++;
                        $correctDetails[] = $answer;
                    } else {
                        $correctDetails[] = $answer;
                    }
                }
            } else if ($type === 'multiple-choice') {
                if (+$customDatum->is_correct === 1) {
                    $expected++;
                    $correctDetails[] = $customDatum->id;
                    if (array_search($customDatum->id, $submittedData) !== false) {
                        $correct++;
                    }
                }
            } else if ($type === 'matching-terms') {
                $pair = md5($customDatum->pair_one) . ':' . md5($customDatum->pair_two);
                $correctDetails[] = array(
                    'code' => $pair,
                    'text' => $customDatum->pair_one . ' ' . $customDatum->pair_two,
                    'pair_one' => $customDatum->pair_one,
                    'pair_two' => $customDatum->pair_two
                );
                $expected++;
                if (array_search($pair, $submittedData) !== false) {
                    $correct++;
                }
            } else if ($type === 'jumble-text') {
                $answer = $customDatum->answer;
                $correctDetails[] = array(
                    'code' => md5($answer),
                    'text' => $answer
                );
                $expected++;
                if ($submittedData[$i] === $answer) {
                    $correct++;
                }
            }
        }

        $logText = array(
            'correct' => $correct,
            'correctDetails' => $correctDetails,
            'expected' => $expected,
            'type' => $type,
            'data' => $data,
            'submittedData' => $submittedData
        );

        $accuracy = 0;
        if ($expected) {
            $accuracy = 1 / $expected * $correct;
        } else { // no correct answer expected
            if (count($submittedData)) {
                $accuracy = 0;
            } else { // correct only if not correct
                $accuracy = 1;
            }
        }

        $uid = $this->getUid();
        if ($uid > -1) {
            $TT = new TestTracker();
            $TT->track($uid, $id, $logText, $accuracy, $isRecapTest);
        }


        return [
            'correctDetails' => $correctDetails,
            'correct' => $correct,
            'expected' => $expected,
            'accuracy' => $accuracy
        ];

    }


    /**
     * Searches for errors on tests
     */
    function checker() {
        return view('pages.test-checker', [
            'data' => (new Test())->checker()
        ]);
    }

}