<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserDataController;
use App\Http\Controllers\RegistryController;
use App\Models\Cluster;
use App\Models\UserData;
use App\Models\LearningUnitTracker;
use App\Models\Config;


/**
 * Controller for dashboard.
 */
class DashboardController extends Controller {



    /**
     * Shows landing page
     */
    public function landing() {
        return view('pages.landing');
    }


    /**
     * Shows terms and conditions page
     */
    public function showTermsAndConditions() {
        return view('pages.terms-and-conditions');
    }


    /**
     * Shows startup page.
     * But if setup is completed, shows dashboard.
     */
    public function set() {
        $EXPECTED_SKILLS = 12;
        $EXPECTED_SKILLS_PREFERENCE = 3;
        $lastAccessTime = 0; // only used in superuser mode
        
        $uid = $this->getUid();
        $U = new UserData($uid);
        $loggedUserEmail = $this->getUemail();
        //$loggedUserEmail = json_decode($U->getData()[0]->other)->email;


        $puid = filter_input(INPUT_GET, 'uid');
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';        
        // if there's a request to see data of another user (see $puid in line above)
        // and the current logged user ($uid) is a superuser
        // and is in superuser mode (set by cookies)
        // assign $puid to $uid and continue
        if ($U->isSuperUser() && $puid && $mode === 'super') {
            $uid = $puid;
            $U = new UserData($uid);
            $lastAccessTime = $U->getLastInteractionTime();
        }



        $LUT = new LearningUnitTracker($uid);
        $hasStarted = count($LUT->getLastLearningUnitStreak(1, false)) > 0;

        $C = new Cluster();

        $clusters = $C->getAll();
        $skills = $U->getClusterSkills();

        $startupCompleted = true;
        if (count($skills) === $EXPECTED_SKILLS) {
            $usedForStartup = array_reduce($skills, function($accum, $item) {
                return $accum + (+$item->use_for_startup > 0 ? 1 : 0);
            }, 0);
            if ($usedForStartup >= $EXPECTED_SKILLS_PREFERENCE) {
                ; // nothing to do
            } else {
                $startupCompleted = false;
            }
        } else {
            $startupCompleted = false;
        }

        $registry = (new RegistryController())->list($uid);

        $userData = $U->getData();
        if ($startupCompleted) { // redirect
            return view('pages.dashboard', [
                    'loggedUserEmail' => $loggedUserEmail,             
                    'clusters' => $clusters,
                    'skills' => $skills,
                    //'lastLU' => (new LearningUnitTracker($uid, null, null))->list(5),
                    'hasStarted' => $hasStarted,
                    'personalDetails' => $userData[0] ?? array(),
                    'data' => $registry,
                    'lastAccessTime' => $lastAccessTime
                ]);
        } else {
            return redirect('welcome');
        }

    }


    /**
     * Shows the startup page
     */
    public function showStartup() {
        $uid = $this->getUid();
        $U = new UserData($uid);
        $userData = $U->getData();
        $loggedUserEmail = $this->getUemail();
        /*if ()
        

        $loggedUserEmail = json_decode($userData[0]->other ?? '{email:""}')->email;*/

        $lastAccessTime = 0; // only used in superuser mode

        $puid = filter_input(INPUT_GET, 'uid');
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';        
        // if there's a request to see data of another user (see $puid in line above)
        // and the current logged user ($uid) is a superuser
        // and is in superuser mode (set by cookies)
        // assign $puid to $uid and continue
        if ($U->isSuperUser() && $puid && $mode === 'super') {
            $uid = $puid;
            $U = new UserData($uid);
            $lastAccessTime = $U->getLastInteractionTime();
        }


        $C = new Cluster();
        $LUT = new LearningUnitTracker($uid);
        $hasStarted = count($LUT->getLastLearningUnitStreak(1, false)) > 0;

        $config = (new Config())->getConfig();

        $email = $U->getEmail();
        $personalDetails = $U->getData();
        if (count($personalDetails)) {
            $personalDetails = $personalDetails[0];
        } else {
            $personalDetails = (object)array(
                'name' => '',
                'surname' => '',
                'email' => '',
                'other' => '',
                'terms_and_conditions' => '0',
                'data_profiling' => '0'
            );
        }


        return view('pages.startup', [
                'loggedUserEmail' => $loggedUserEmail,
                'clusters' => $clusters = $C->getAll($uid),
                'skills' => $U->getClusterSkills(),
                'personalDetails' => $personalDetails,
                'email' => $email,
                'hasStarted' => $hasStarted,
                'config' => $config,
                'lastAccessTime' => $lastAccessTime
            ]);
    }


    /**
     * Sets the value for a cluster skill
     * @param {int} clusterId the id of the cluster
     * @param {int} skillValue the value of the skill
     */
    public function setSkill($clusterId, $skillValue) {
        (new Cluster())->set($this->getUid(), $clusterId, $skillValue);
    }


    /**
     * Sets the value for a cluster preference
     * @param {int} clusterId the id of the cluster
     * @param {int} skillValue the value of the preference
     */
    public function setSkillPreference($clusterId, $value) {
        (new Cluster())->setPreference($this->getUid(), $clusterId, $value);
    }


    /**
     * Shows the welcome page
     */
    public function showWelcome() {
        return view('pages.welcome');
    }


    /**
     * Gets data for selections depending on other selections
     * @param {string} key the key in the config table
     * @param {string} depending the key to extract from the value object in the config table
     */
    public function getDataDependingOn($key, $depending) {
        return json_decode((new Config())->getValue($key)->value, true)[$depending];
    }
    

    /**
     * Switches to super user
     */
    public function switchToSuperUser() {
        if ($this->isSuperUser()) { // can switch to super user
            setcookie('clikc-user-mode', 'super', time() + (86400 * 30), "/"); // 86400 = 1 day
            return redirect('superuser/users-list');
        }
        return redirect('dashboard');
    }
    

    /**
     * Switches to basic user
     */
    public function switchToBasicUser() {
        setcookie('clikc-user-mode', 'basic', time() + (86400 * 30), "/"); // 86400 = 1 day
        return redirect('dashboard');
    }
}
