<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LearningUnit;
use App\Models\LearningUnitTracker;
use App\Models\Test;
use App\Models\TestTracker;
use App\Models\Cluster;

/**
 * Class to emulate IA.
 */
class IAController extends Controller {


    /**
     * Checks if the user path needs now a recap test
     * @param {array} streak the streak with the last learning units (labour market exluded)
     * @param {object} the last recap test performed, if any
     * @return {boolean} true if the user path needs a recap test, false otherwise
     */
    private function checkIfNeedsARecapTest($streak, $lastRecapTest) {
        $needsRecapTest = true;
        if (count($lastRecapTest)) { // at least one recap test was performed
            if (!isset($lastRecapTest->completed_on)) {
                $LUPresent = false;
                foreach ($streak as $item) {
                    if (+$item->learning_unit_id === +$lastRecapTest[0]->after_learning_unit_id) {
                        $LUPresent = true;
                    }
                }
                if ($LUPresent) { // the recap test was done after one of the last 10 LU
                    $needsRecapTest = false; // no need to do another
                } else {
                    $needsRecapTest = true; // need to do a recap test
                }
            } else { // a recap test is not completed yet
                $needsRecapTest = true; // need to do a recap test
            }
        } else { // never experiences a recap test
            if (count($streak) >= 10) { // 10 or more LU were done
                $needsRecapTest = true; // need to do a recap test
            } else {
                $needsRecapTest = false; // no need to do a recap test
            }
        }
        return $needsRecapTest;
    }


    /**
     * Checks if the user path needs now a labour market learning unit
     * @param {array} streak the streak with the last learning units (labour market included)
     * @return {boolean} true if the user path needs a labour market learning unit, false otherwise
     */
    private function checkIfNeedsALabourMarketLearningUnit($streak) {
        $n = 0;
        $nlb = 0;
        foreach ($streak as $lu) {
            if ($lu->learning_unit_labour_market_id !== null) {
                $nlb++;
            }
            $n++;
        }
        return $n >= 5 && !$nlb;
    }


    public function nextEmpty() {
        return view('pages.ia-next');
    }

    /**
     * Proposes the next content to the user.
     * May vary:
     * - the current pending learning unit
     * - a test recap (every 10 learning units)
     * - a labour market learning unit (every 5 learning units, with priority on test recap)
     * - a learning unit 
     */
    public function next() {

        $uid = $this->getUid();
        
        $LUT = new LearningUnitTracker($uid, null, null);
        $TT = new TestTracker($uid);

        $pendingId = $LUT->getPending();
        $pendingRecapTestId = $TT->getPendingRecapId();
        if (isset($pendingId)) { // if there is a pending unit, i.e. there is an uncompleted learning unit
            // return view('pages.learning-unit-pending',  [ 'pendingId' => $pendingId ]);
            return json_encode([
                'pendingLU' => true,
                'pendingId' => $pendingId
            ]);
        } else if (isset($pendingRecapTestId)) {
            return json_encode([
                'recapTestTime' => true
            ]);
        } else {

            $TT = new TestTracker($uid);

            $streak = $LUT->getLastLearningUnitStreak(10, false);
            $lastRecapTest = $TT->getLastRecap($uid);
            $needsRecapTest = $this->checkIfNeedsARecapTest($streak, $lastRecapTest);
            
            $streak = $LUT->getLastLearningUnitStreak(5, true);
            $atLeastOne = count($streak) > 0;
            $needsLabourMarketLearningUnit = $this->checkIfNeedsALabourMarketLearningUnit($streak);
            $stillLearningUnitLabourMarketToDo = $LUT->hasLabourMarketLearningUnitsToDo();
            $needsLabourMarketLearningUnit = $needsLabourMarketLearningUnit && $stillLearningUnitLabourMarketToDo;


            if ($needsRecapTest && !$needsLabourMarketLearningUnit) { // if needs a recap but also doesn't need a labour market l.u.
                //return redirect('/recap-test-time');
                return json_encode([
                    'recapTestTime' => true
                ]);
            } else {

                $flattenIds = array();
                $optionIds = array();
                $isLabourMarket = null;

                $url = 'https://clikc-recsys-app.nexacenter.org/api/v1/recsys-interface/recommendations/user/' . $uid;
                $curl = curl_init();
                
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_POST, false);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('access-token:37UR/Aw-c5J&Lb-PQp6xT-EK*Dz7:=Df'));
                $certificate_location = '/var/www/vhosts/clikc.eu/httpdocs/clikc.pem_clikc-quantitas.it.pem';
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $certificate_location);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $certificate_location);
                $res = curl_exec($curl); 


                curl_close($curl);
                //var_dump($res);
                $data = json_decode($res);


                if (isset($data) && isset($data->ids)) {
                    $LUT->saveRecommendation($data);

                    $isLabourMarket = $data->is_labour_market;
                    $data = array_map(function($id) use ($isLabourMarket) {
                        return (new LearningUnit($id, $isLabourMarket))->get(true)[0];
                    }, $data->ids);

                    

                                

                    /** use commented code to randomly self-select units
                    if ($needsLabourMarketLearningUnit) {
                        $N_OPTIONS = 1;

                        $ids = $LUT->listUnstartedLabourMarketLearningUnitsId($uid);

                        $optionIds = $this->pickRandom($ids, $N_OPTIONS);
                        $isLabourMarket = true;

                    } else { // labour market learning unit already proposed in last 5 requests
                        $N_OPTIONS = 3;

                        $ids = $LUT->listUnstartedLearningUnitsId($uid);

                        $optionIds = $this->pickRandom($ids, $N_OPTIONS);
                        $isLabourMarket = false;
                        
                    }

                    $data = array_map(function($id) use ($isLabourMarket) {
                        return (new LearningUnit($id, $isLabourMarket))->get()[0];
                    }, $optionIds);*/

                    $clusters = array();
                    foreach ((new Cluster())->getAll() as $record) {
                        $clusters[$record->id] = $record->description;
                    };

                    return json_encode([
                        'data' => $data,
                        'isLabourMarket' => $isLabourMarket,
                        'clusters' => $clusters,
                        'atLeastOne' => $atLeastOne
                    ]);
                } else {
                    return json_encode([
                        'error' => $res
                    ]);
                }
            }
        }
        
    }


    /**
     * Picks some random elements from a set of elements
     * @param {array} ids the set of elements
     * @param {number} N_OPTIONS how many random elements to pick
     * @return {array} the random elements
     */
    function pickRandom($ids, $N_OPTIONS) {
        $flattenIds = array_map(function($item) {
            return $item->id;
        }, $ids);
        $len = count($flattenIds);

        $optionIds = array();
        if ($len < $N_OPTIONS) {
            $optionIds = $flattenIds;
        } else {
            for ($i = 0; $i < $N_OPTIONS; $i++) {
                $rand = rand(0, $len-1);
                $stop = 0;
                while (array_search($flattenIds[$rand], $optionIds) !== false && $stop < 100) {
                    $rand = rand(0, $len-1);
                    $stop++;
                }
                if (array_search($flattenIds[$rand], $optionIds) === false) {
                    $optionIds[] = $flattenIds[$rand];
                }
            }
        }
        return $optionIds;
    }


}


