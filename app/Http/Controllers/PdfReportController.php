<?php

namespace App\Http\Controllers;


/**
 * Controller for pdf report.
 */
class PdfReportController {

    const LEFT_MARGIN = 10;

    
    /**
     * The constructor
     */
    function __construct() {
    }


    /**
     * Adds a page to a pdf
     * @param {object} pdf the pdf
     */
    function addPage($pdf) {
        $pdf->AddPage();
        $pdf->setXY(self::LEFT_MARGIN, 20);
    }
    

    /**
     * Prints indicators to the pdf
     * @param {int} y the y position
     * @param {array} indicators the indicators
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printIndicators($y, $indicators, $pdf) {
        $x = $pdf->getX();
        foreach ($indicators as $indicator) {
            $y = $pdf->addIndicator($indicator['title'], $indicator['value']);
            $x += 30;
            if (strpos(strtolower($indicator['title']), 'learning unit') !== false) {
                $x += 12;
            }
            $pdf->setXY($x, $y);
        }
        $y += 17;
        $pdf->setXY(self::LEFT_MARGIN, $y);
        return $y;
    }


    /**
     * Access and usage section
     * @param {array} indicators the indicators
     * @param {array} imageCodes the image codes
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printAccessAndUsage($indicators, $imageCodes, $pdf) {
        $y = $pdf->addH2('Access and usage');
        $y = $this->printIndicators($y, $indicators['access-and-usage'], $pdf);
        $y = $pdf->addImage(self::LEFT_MARGIN, $y, 190, round(190/900 * 250), 'reports-img/' . $imageCodes['access-and-usage'] . '.png');
        return $y;
    }


    /**
     * Learning path overall section
     * @param {array} indicators the whole set of indicators
     * @param {array} imageCodes the image codes
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printLearningPathOverall($indicators, $imageCodes, $pdf) {
        $y = $pdf->addH3('Overall');
        $y = $this->printIndicators($y, $indicators['learning-path'], $pdf);
        $y = $pdf->addProgressBar(self::LEFT_MARGIN, $y, 190, round(190/1800 * 32), '', 'reports-img/' . $imageCodes['overall'] . '.png');
        return $y;
    }


    /**
     * Learning path by skill section
     * @param {array} skillPercentages the skills percentage
     * @param {array} imageCodes the image codes
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printLearningPathBySkill($skillPercentages, $imageCodes, $pdf) {
        $y = $pdf->addH3('Learning Path By Skill');
        $x = 15;
        $y += 3;
        $progressBarTexts = array(
            'Deal With Complexity', 'Leadership', 'Teamwork',
            'Working Autonomously', 'Cope With Stress And Uncertainty', 'Effective Time Management',
            'Critical Thinking', 'Make Decisions', 'Problem Solving',
            'Communicate Constructively', 'Create Confidence And Tollerance', 'Emotional Intelligence'
        );
        foreach ($progressBarTexts as $i=>$progressBarText) {
            $pdf->addProgressBar($x, $y, 50, round(50/522 * 20), $progressBarText . ' - ' . round($skillPercentages[$i]) . '%', 'reports-img/' . $imageCodes['skill-' . $i] . '.png');
            $x += 65;
            if ($x > 180) {
                $x = 15;
                $y += 15;
            }
        }
        return $y;
    }


    /**
     * Trophies section
     * @param {array} indicators the indicators
     * @param {array} trophies the trophies
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printTrophies($indicators, $trophies, $pdf) {
        $y = $pdf->addH3('Trophies');
        $y = $this->printIndicators($y, $indicators['trophies'], $pdf);
        $x = self::LEFT_MARGIN;
        $y -= 3;
        foreach ($trophies as $trophy) {
            $pdf->addImage($x, $y, 9.1, 10.6, $trophy);
            $x += 10;
            if ($x > 195) {
                $x = self::LEFT_MARGIN;
                $y += 11.5;
            }
            $pdf->setXY($x, $y);
        }
        return $y;
    }


    /**
     * LUs tests overall section
     * @param {array} indicators the indicators
     * @param {array} imageCodes the image codes
     * @param {number} car the correct answer rate
     * @param {number} rr the retention rate
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printLUsTestsOverall($indicators, $imageCodes, $car, $rr, $pdf) {
        $y = $pdf->addH3('Learning Units Tests Overall');
        $y = $this->printIndicators($y, $indicators['tests'], $pdf);
        $x = self::LEFT_MARGIN;

        $pdf->SetTextColor(30, 30, 30);
        $pdf->SetFont('lexend-deca', '', 9);
        $pdf->setXY($x, $y+10);
        $pdf->Cell(0, 0, 'Correct Answers Rate', 0, 1, 'L', false);
        $pdf->setXY($x, $y+15);
        $pdf->Cell(0, 0, $car, 0, 1, 'L', false);
        $pdf->setXY($x+50, $y+10);
        $pdf->Cell(0, 0, 'Correct Answers Rate', 0, 1, 'L', false);
        $pdf->setXY($x+50, $y+15);
        $pdf->Cell(0, 0, $rr, 0, 1, 'L', false);

        $pdf->setXY($x, $y);
        $y -= 20;
        $y = $pdf->addImage(100, $y, 110, round(110/440 * 250), 'reports-img/' . $imageCodes['tests-overall'] . '.png');
        return $y;
    }


    /**
     * LUs tests by skill section
     * @param {array} imageCodes the image codes
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printLUsTestsBySkill($imageCodes, $pdf) {
        $y = $pdf->addH3('Learning Units Tests By Skill');
        $pdf->setXY(self::LEFT_MARGIN, $y+10);
        $y = $pdf->addImage(self::LEFT_MARGIN, $y, 190, round(190/900 * 250), 'reports-img/' . $imageCodes['tests-by-cluster'] . '.png');
        return $y;
    }


    /**
     * Reception section
     * @param {array} indicators the indicators
     * @param {object} pdf the pdf
     * @return {number} the new y
     */
    function printReception($indicators, $pdf) {
        $y = 245;
        if (count($indicators['reception'])) {
            $pdf->setXY(self::LEFT_MARGIN, $y);
            $y = $pdf->addH2('Learning Units Reception');
            $y = $this->printIndicators($y, $indicators['reception'], $pdf);
        }
        return $y;
    }


    /**
     * Creates the pdf
     * @param {number} time the time
     * @param {string} user the user's name
     * @param {array} imageCodes the image codes
     * @param {array} skillPercentages the skills percentage
     * @param {array} indicators the indicators
     * @param {array} trophies the trophies
     * @param {number} car the correct answer rate
     * @param {number} rr the retention rate
     */
    function createPdf($time, $user, $imageCodes, $skillPercentages, $indicators, $trophies, $car, $rr) {

        $pdf = new PDF();
        $pdf->AliasNbPages();

        $this->addPage($pdf);

        $pdf->Cell(190, 0, $user, 0, 0, 'R');
        $pdf->setX(self::LEFT_MARGIN);

        $y = $pdf->addH1('Reports and statistics');
        $y = $this->printAccessAndUsage($indicators, $imageCodes, $pdf);
        $pdf->setX(self::LEFT_MARGIN);

        $pdf->addH2('Learning path');
        $y = $this->printLearningPathOverall($indicators, $imageCodes, $pdf);
        $y = $this->printLearningPathBySkill($skillPercentages, $imageCodes, $pdf);

        $this->addPage($pdf);
        $y = $pdf->addH2('Scores and performance');
        $y = $this->printTrophies($indicators, $trophies, $pdf);

        
        $pdf->setXY(self::LEFT_MARGIN, 90);
        $y = $this->printLUsTestsOverall($indicators, $imageCodes, $car, $rr, $pdf);

        $pdf->setXY(self::LEFT_MARGIN, $y-10);
        $y = $this->printLUsTestsBySkill($imageCodes, $pdf);
        
        $y = $this->printReception($indicators, $pdf);



        ob_clean();
        ob_start();
        $pdf->Output();
        $html = ob_get_contents();

        $output_file = 'reports/report-' . $time . '.pdf';
        $ifp = fopen($output_file, 'wb');
        fwrite($ifp, $html);
        fclose($ifp);

        return $output_file;
    }
}

