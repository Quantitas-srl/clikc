<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\LearningUnit;
use App\Models\LearningUnitTracker;
use App\Models\Test;
use App\Models\TestTracker;
use App\Models\Cluster;
use App\Models\UserData;


/**
 * Controller for learning units.
 */
class LearningUnitController extends Controller {


    /**
     * Shows a learning unit
     * @param {number} id the id of the learning unit
     */
    public function showLearningUnit($id) {
        return $this->learningUnit($id, 'show');
    }


    /**
     * Shows the tests of a learning unit
     * @param {number} id the id of the learning unit
     */
    public function showLearningUnitTest($id) {
        return $this->learningUnit($id, 'test');
    }


    /**
     * Shows the advice of pending learning unit
     * @param {number} id the id of the pending learning unit
     */
    public function pendingLUAdvice($id) {
        return view('pages.learning-unit-pending', [
            'pendingId' => $id
        ]);
    }


    /**
     * Shows the completed-tests page for a learning unit
     * @param {number} id the id of the learning unit
     */
    public function confirmLearningUnitTest($id) {
        return $this->learningUnit($id, 'test-completed');
    }


    /**
     * Shows what need to be shown for a learning unit
     * @param {number} id the id of the learning unit
     * @param {string} action what to do
     */
    public function learningUnit($id, $action) {
        $uid = $this->getUid();
        
        $LU = new LearningUnit($id, false);
        if ($LU->exists()) {
            $dataLU = $LU->get()[0];
            $LUT = new LearningUnitTracker($uid, $dataLU->id, false);
            if ($action === 'show') {
                return $this->_showLearningUnit($id, $uid, $LU, $LUT);
            } else if ($action === 'test') {
                return $this->_showLearningUnitTest($id, $uid, $LU, $LUT);
            } else if ($action === 'test-completed') {
                return $this->_confirmLearningUnitTest($id, $uid, $LU, $LUT);
            }
        } else {
            return view('pages.learning-unit-not-found');
        }
    }


    /**
     * Shows a learning unit
     * @param {number} id the id of the learning unit
     * @param {number} uid the id of the user
     * @param {LearningUnit} LU a reference to the class LearningUnit
     * @param {LearningUnitTracker} LU a reference to the class LearningUnitTracker
     */
    private function _showLearningUnit($id, $uid, $LU, $LUT) {
        $uid = $this->getUid();
        $U = new UserData($uid);
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
        $isSuperUserViewing = $U->isSuperUser() && $mode === 'super';

        $isInHistory = $LUT->isInHistory();
        if ($isSuperUserViewing || $isInHistory) {

            $userStatus = $LUT->getUserStatus();
            $pendingId = $LUT->getPending();

            if (!$isSuperUserViewing && isset($pendingId) && +$pendingId !== +$id && !isset($userStatus->test_completed_on)) {
                return view('pages.learning-unit-pending', [ 'pendingId' => $pendingId ]);
            } else {
                if (!$isSuperUserViewing && isset($userStatus->completed_on) && !isset($userStatus->test_completed_on)) {
                    return view('pages.learning-unit-test-invitation', [ 'id' => $id ]);
                } else {
                    if (!isset($userStatus->started_on)) {
                        $LUT->setStarted();
                    }
                    [$learning_unit, $translations_id, $dinamic_fields] = $LU->get();

                    $clustersData = (new Cluster())->getAll();
                    $clusters = array();
                    foreach ($clustersData as $clusterDatum) {
                        $clusters[+$clusterDatum->id] = $clusterDatum->description;
                    }

                    return view('pages.learning-unit', 
                        [
                            'learning_unit' => $learning_unit, 
                            'dinamic_fields' => $dinamic_fields,
                            'user_status' => $userStatus,
                            'clusters' => $clusters,
                            'editable' => !isset($userStatus->completed_on),
                            'isSuperUser' => $U->isSuperUser()
                        ]);
                }
            }

        } else {
            return view('pages.learning-unit-unavailable');
        }
    }


    /**
     * Shows the test of a learning unit
     * @param {number} id the id of the learning unit
     * @param {number} uid the id of the user
     * @param {LearningUnit} LU a reference to the class LearningUnit
     * @param {LearningUnitTracker} LU a reference to the class LearningUnitTracker
     */
    private function _showLearningUnitTest($id, $uid, $LU, $LUT) {

        $U = new UserData($uid);
        $loggedUserEmail = json_decode($U->getData()[0]->other)->email;
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
        $isSuperUserViewing = $U->isSuperUser() && $mode === 'super';

        if ($isSuperUserViewing) {
            $uid = filter_input(INPUT_GET, 'ref');
        }


        $userStatus = $LUT->getUserStatus();
        $pendingId = $LUT->getPending();
        if (!$isSuperUserViewing && isset($pendingId) && +$pendingId !== +$id && !isset($userStatus->test_completed_on)) {
            return view('pages.learning-unit-pending',  [ 'pendingId' => $pendingId ]);
        } else {

            $isInHistory = $LUT->isInHistory();
            if ($isSuperUserViewing || $isInHistory) {

                if (!$isSuperUserViewing && !isset($userStatus->completed_on)) {
                    $LUT->setCompleted();
                }
                $TEST = new Test($id);
                [$learning_unit, $translations_id, $dinamic_fields] = $LU->get($id);
                $testsData = $TEST->getByTranslationId($translations_id);
                
                $testIds = array_map(function($item) {
                    return $item['id'];
                }, $testsData);

                $TT = new TestTracker();
                $rawsavedTestsData = $TT->get($uid, $testIds, false);

                $savedTestsData = array();
                foreach ($rawsavedTestsData as $rawsavedTestsDatum) {
                    $submitted_data = json_decode($rawsavedTestsDatum->submitted_data);
                    $submittedData = $submitted_data->submittedData;
                    $testId = $submitted_data->data->data[0]->id;
                    $savedTestsData[$testId] = $submittedData;
                }

                $clustersData = (new Cluster())->getAll();
                $clusters = array();
                foreach ($clustersData as $clusterDatum) {
                    $clusters[+$clusterDatum->id] = $clusterDatum->description;
                }

                $editable = !isset($userStatus->test_completed_on);
                if ($isSuperUserViewing) {
                    $editable = false;
                }

                $userEmail = '';
                $userData = (new UserData($uid))->getData();
                if (count($userData)) {
                    $otherData = $userData[0]->other;
                    if ($otherData) {
                        $userEmail = json_decode($otherData)->email;
                    }
                }
                
                return view('pages.learning-unit-test', 
                    [
                        'loggedUserEmail' => $loggedUserEmail,
                        'userEmail' => $userEmail,
                        'learning_unit' => $learning_unit, 
                        'dinamic_fields' => $dinamic_fields,
                        'testsData' => $testsData,
                        'savedTestsData' => $savedTestsData,
                        'clusters' => $clusters,
                        'editable' => $editable,
                        'isSuperUserViewing' => $isSuperUserViewing
                    ]);
            } else {
                return view('pages.learning-unit-unavailable');
            }
        }
    }


    /**
     * Shows the completed-test page for a learning unit
     * @param {number} id the id of the learning unit
     * @param {number} uid the id of the user
     * @param {LearningUnit} LU a reference to the class LearningUnit
     * @param {LearningUnitTracker} LU a reference to the class LearningUnitTracker
     */
    public function _confirmLearningUnitTest($id, $uid, $LU, $LUT) {
        $userStatus = $LUT->getUserStatus();
        if (!isset($userStatus) || !isset($userStatus->started_on)) {
            return redirect('/learning-unit/' . $id . '/show');
        } else if (!isset($userStatus->completed_on)) {
            return redirect('/learning-unit/' . $id . '/test');
        } else {
            if (!isset($userStatus->test_completed_on)) {
                $LUT->setTestCompleted();
            }
            $accuracy = $LUT->getLearningUnitTestResults();
            return view('pages.learning-unit-test-completed', [
                'pendingId' => $LUT->getPending(),
                'accuracy' => $accuracy
            ]);
        }
    }


    /**
     * Likes a learning unit
     * @param {number} id the id of the learning unit
     */
    public function like($id) {
        $this->setLike($id, +1);
    }


    /**
     * Dislikes a learning unit
     * @param {number} id the id of the learning unit
     */
    public function unlike($id) {
        $this->setLike($id, -1);
    }


    /**
     * Sets the like status of a learning unit
     * @param {number} id the id of the learning unit
     * @param {number} value >0 to like, <0 to dislike
     */
    private function setLike($id, $value) {
        $LUT = new LearningUnitTracker($this->getUid(), $id, false);
        if ($value > 0) {
            $LUT->like();
        } else if ($value < 0) {
            $LUT->unlike();
        }
    }


    /**
     * Users recap for superuser
     */
    public function usersRecap() {
        $U = new UserData($this->getUid());
        $userData = $U->getData();
        $LUT = new LearningUnitTracker(null, null, false);
        $data = $LUT->getUsersRecap();
        return view('pages.superuser.users-status', [
            'data' => $data,
            'personalDetails' => $userData[0],
        ]);
    }

}
