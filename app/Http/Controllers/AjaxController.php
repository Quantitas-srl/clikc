<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\LearningUnitController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UserDataController;
use App\Http\Controllers\IAController;
use App\Http\Controllers\PdfReportController;


/**
 * Controller for ajax requests.
 */
class AjaxController extends Controller {

    
    /**
     * Sets a skill for a cluster (for logged in user)
     * @param {object} request the request
     */
    public function setClusterSkill(Request $request) {
        (new DashboardController())->setSkill($request->input('cluster'), $request->input('value'));
    }


    /**
     * Sets a preference for a cluster (for logged in user)
     * @param {object} request the request
     */
    public function setClusterPreference(Request $request) {
        (new DashboardController())->setSkillPreference($request->input('cluster'), $request->input('value'));
    }


    /**
     * Likes a LU (for logged in user)
     * @param {object} request the request
     */
    public function likeLU(Request $request) {
        (new LearningUnitController())->like($request->input('luid'));
    }


    /**
     * Dislikes a LU (for logged in user)
     * @param {object} request the request
     */
    public function unlikeLU(Request $request) {
        (new LearningUnitController())->unlike($request->input('luid'));
    }


    /**
     * Checks a test
     * @param {object} request the request
     * @return {array} info about correctness
     */
    public function checkTest(Request $request) {
        $test = new TestController();
        $result = $test->check($request->input('id'), $request->input('type'), json_decode($request->input('data')), $request->input('isRecapTest'));
        return response()->json($result);
    }


    /**
     * Updates user's data
     * @param {object} request the request
     */
    public function updateProfileData(Request $request) {
        $data = $request->input('data');
        $UserData = new UserDataController();
        $result = $UserData->updateData($data);
        return response()->json($result);
    }


    /**
     * Gets next LU
     */
    public function iaNext() {
        $next = (new IAController())->next();
        return $next;
    }


    /**
     * Gets data for selections depending on other selections
     * @param {Request} request the request
     * @return {object} depending data
     */
    public function startupDependingData(Request $request) {
        return (new DashboardController())->getDataDependingOn($request->input('key'), $request->input('depending'));
    }


    /**
     * Saves an image to reports-img/ folder
     * Request must contain: name, data (image as base64)
     * # symbol is replaced by + symbol
     * @param {Request} request the request
     * @return {array} the name received by $request parameter and the code associated to this image
     */
    public function saveImage(Request $request) {
        $name = $request->input('name');
        $data = $request->input('data');
        $data = base64_decode(explode(',', str_replace('#', '+', $data))[1]);
        $rand = rand(0, 99999);
        $code = md5(time().$rand);
        $output_file = 'reports-img/'.$name.'-'.$code.'.png';
        $ifp = fopen($output_file, 'wb');
        fwrite($ifp, $data);
        fclose($ifp);
        return array(
            'name' => $name,
            'code' => $code
        );
    }


    /**
     * Calls to create and save a report
     * @param {Request} request the request
     */
    public function saveReport(Request $request) {
        return (new PdfReportController())->createPdf(
            $request->hash, 
            $request->user, 
            json_decode($request->imageCodes, TRUE), 
            json_decode($request->skillPercentages, TRUE), 
            json_decode($request->indicators, TRUE), 
            json_decode($request->trophies, TRUE), 
            $request->car, 
            $request->rr
        );
    }

}