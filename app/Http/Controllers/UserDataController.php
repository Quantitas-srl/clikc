<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserData;
use Illuminate\Support\Facades\Auth;



/**
 * Controller for a test.
 */
class UserDataController extends Controller {
    

    /**
     * Updates this user's data
     * @param {array} data the user's data
     * @return {-} the result of the operation
     */
    function updateData($data) {
        return (new UserData($this->getUid()))->updateData(json_decode($data));
    }
    

    /**
     * Returns the data of this user
     * @return {array} the data of this user
     */
    function getData() {
        return (new UserData($this->getUid()))->getData();
    }


    /**
     * Creates fake emails for groups
     */
    function emailsCreator() {

        return view('pages.emails-creator', [ 'data' => array(), 'filename' => '' ]); /// remove this line to execute


        $U = new UserData(null);
        $data = array();

        $csv = 'name;email;password' . "\r\n";
        for ($g = 4; $g <= 4; $g++) {
            for ($u = 1; $u <= 31; $u++) {
                $name = 'user-' . str_pad($g, 2, '0', STR_PAD_LEFT) . '-' . str_pad($u, 3, '0', STR_PAD_LEFT);
                $email =  $name . '@clikc.eu';
                $pw = $this->createPw();
                $U->insertNewUser($name, $email, $pw);
                $data[] = array(
                    'name' => $name,
                    'email' => $email,
                    'password' => $pw
                );
                $csv .= $name . ';' . $email . ';' . $pw . "\r\n";
            }
        }

        $time = time();
        $filename = 'material/users-' . $time . '.csv';
        file_put_contents($filename, $csv);
        
        return view('pages.emails-creator', [ 'data' => $data, 'filename' => $filename ]);
    }


    /**
     * Creates a password
     * @return {string} the password
     */
    private function createPw() {
        $basket = 'bcdfghjkmnpqrstCFHJMS23456789';
        $s = '';
        for ($i = 0; $i < 8; $i++) {
            $rand = rand(0, strlen($basket)-1);
            $s .= $basket[$rand];
        }
        return $s;
    }

}