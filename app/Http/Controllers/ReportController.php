<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\UserData;
use App\Models\Cluster;
use App\Models\LearningUnit;
use App\Models\LearningUnitTracker;
use App\Models\TestTracker;
use App\Http\Controllers\PDF;
use App\Http\Controllers\FPDF;


/**
 * Controller for a report.
 */
class ReportController extends Controller {

    private $uid;

    /**
     * Shows the report page
     */
    function showReport() {
        $uid = $this->getUid();
        $U = new UserData($uid);
        $loggedUserEmail = json_decode($U->getData()[0]->other)->email;
        $lastAccessTime = 0; // only used in superuser mode

        $puid = filter_input(INPUT_GET, 'uid');
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';        
        // if there's a request to see data of another user (see $puid in line above)
        // and the current logged user ($uid) is a superuser
        // and is in superuser mode (set by cookies)
        // assign $puid to $uid and continue
        if ($U->isSuperUser() && $puid && $mode === 'super') {
            $uid = $puid;
            $U = new UserData($uid);
            $lastAccessTime = $U->getLastInteractionTime();
        }



        $C = new Cluster();
        $LU = new LearningUnit();
        $LUT = new LearningUnitTracker($uid);
        $TT = new TestTracker($uid);
        $hasStarted = count($LUT->getLastLearningUnitStreak(1, false)) > 0;


        $email = $U->getEmail();
        $personalDetails = $U->getData();
        if (count($personalDetails)) {
            $personalDetails = $personalDetails[0];
        } else {
            $personalDetails = (object)array(
                'name' => '',
                'surname' => '',
                'email' => '',
                'other' => '',
                'terms_and_conditions' => '0'
            );
        }

        $countPerDay = $LUT->countPerDay();
        $firstTime = reset($countPerDay) ? strtotime(reset($countPerDay)->date) : 0;
        $lastTime = end($countPerDay) ? strtotime(end($countPerDay)->date) : 0;
        $days = ($lastTime - $firstTime) / (60*60*24) + 1;
        $nStarted = $LUT->countStarted();
        $nCompletedByCluster = $LUT->countCompletedByCluster();
        $bestMonth = $LUT->bestMonth();
        $nLUsByCluster = $LU->countByCluster();
        $rawClusters = (new Cluster())->getAll();
        $clusters = array();
        foreach ($rawClusters as $rawCluster) {
            $clusters[$rawCluster->id] = $rawCluster->description;
        }
        $recaps = $TT->getUserRecapsSummary();
        $tests = $TT->getUserTestsSummary(0);
        $rtests = $TT->getUserTestsSummary(1);
        $testsByCluster = $TT->getUserTestsByCluster();

        
        $likedDislikedLUs = $LUT->countLikedDislikedByUser();
        $likedLUs = $likedDislikedLUs['liked'];
        $dislikedLUs = $likedDislikedLUs['disliked'];

        
        return view('pages.report', 
            [
                'loggedUserEmail' => $loggedUserEmail,
                'hasStarted' => $hasStarted,
                'personalDetails' => $personalDetails,
                'countPerDay' => $countPerDay,
                'bestMonth' => $bestMonth,
                'dayAvg' => $nStarted/$days,
                'nCompletedByCluster' => $nCompletedByCluster,
                'nLUsByCluster' => $nLUsByCluster,
                'clusters' => $clusters,
                'recaps' => $recaps,
                'tests' => $tests,
                'rtests' => $rtests,
                'testsByCluster' => $testsByCluster,
                'firstDate' => $LUT->getFirstDate() ?? 0,
                'lastDate' => $LUT->getLastDate() ?? 0,
                'lastAccessTime' => $lastAccessTime,
                'likedLUs' => $likedLUs,
                'dislikedLUs' => $dislikedLUs
            ]);
    }

}