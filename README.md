<img src="https://gitlab.com/Quantitas-srl/303-clikc/-/raw/main/docs/img/clikc_eu_logos.png" alt="Click EU logos" title="Click EU logos">

# CLIKC user interface

This document provides a technical view of the CLIKC user interface, in terms of development and production environments, requirements and actions to perform in order to setup a ready-to-use instance on a web server.

## Development environment

<p>The CLIKC user interface is a web application hosted by a Docker container, which runs a PHP instance as server-side programming language. The Docker container is managed by Docker Desktop v4.12.0, running a PHP v8.0.2.</p>
<p>Visual Studio Code v1.73.1 acted as IDE during the development phase.</p>
<p>The PHP framework is Laravel v9.</p>
References:
<ul>
<li>https://www.docker.com/</li>
<li>https://code.visualstudio.com/</li>
<li>https://laravel.com/</li>
</ul>
More references in the following part of this document.


## Components

List of components follows.

### MySQL

<p>The MySQL instance used as data storage is not hosted by the Docker container itself. It's instead available on a third party machine which is also accessed by the CLIKC recommender system application (https://github.com/clikc-eu/clikc-recsys-app).</p>
<p>An equivalent of the MySQL instance can be easily created by importing into an empty database the latest (sorted by date) sql file stored in the /dumps folder.</p>
<p>The engine used to run MySQL is MariaDB 10.3.36, port 3306.</p>
<p>The connection parameters have to be reported into a .env file in the root folder (see .env.example file as an example).</p>
References:
<ul>
<li>https://www.mysql.com/it/</li>
<li>https://mariadb.com/</li>
</ul>

### PHP

The PHP web server is provided by the Docker container. The PHP version is v8.0.2.
References:
<ul>
<li>https://www.php.net/</li>
</ul>

### JavaScript

<p>The web application requires JavaScript enabled on client-side.</p>
References:
<ul>
<li>https://www.w3schools.com/js/default.asp</li>
</ul>

### Authentication

<p>The authentication system is provided by an open source third party library, encouraged by the Laravel developers themselves, named Laravel Breeze, which accounts for the main features related to user registration and login.</p>
<p>Note: in the current usage the "forgot password" link was removed from the login form; the registration page is accessible only by direct link, since no user is supposed to register on its own.</p>
References:
<ul>
<li>https://laravel.com/docs/9.x/starter-kits#laravel-breeze</li>
<li>https://github.com/laravel/breeze</li>
</ul>

### Charts

<p>The charts are developed with the support of the open source JavaScript library ChartJS, v3.9.1.</p>
References:
<ul>
<li>https://www.chartjs.org/</li>
<li>https://www.npmjs.com/package/chartjs-adapter-date-fns</li>
</ul>

### Tables

<p>The html tables are enhanced by using the DataTables plugin v1.12.1 built for jQuery. Therefore the jQuery library (v3.6.0) is required. Both technologies are free of cost.</p>
<ul>
<li>https://datatables.net/</li>
<li>https://jquery.com/</li>
</ul>

### Additional notes

All the JavaScript libraries are copied and referenced inside the project source code, so there is no external component loading at runtime.

## How to setup a local development environment

<p>The steps to setup a local development environment are:</p>
<ul>
<li>create a Docker container with a PHP web server</li>
<li>download the current repository to the Docker container folder</li>
<li>host a database on an accessible source, filling it with a dump file</li>
<li>setup the parameters in the .env file</li>
</ul>

## How to setup a production environment (e.g. on Seeweb or Aruba providers)

<p>There is no need to use a Docker technology to host the web application to an online server: the files in the Docker container folder can be transferred via FTP to the server public folder; just update the .env file in case (e.g. if there is a different production database).</p>
<p>As for the local setup, a PHP web server must be available on the server.</p>

