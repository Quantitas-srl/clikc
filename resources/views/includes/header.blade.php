@if (Auth::id() > 0)
    <?php
    $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
    if (request()->request->get('isSuperUser')) {
        if ($mode === 'basic') {
            ?>
            <a href="/dashboard" title="Dashboard" id="header-logo">
                <img src="<?=asset('assets/img/logo-clikc.png')?>" alt="Logo Clikc" title="Logo Clikc" />
            </a>
            <?php
        } else {
            ?>
            <a href="/superuser/users-list" title="Users List" id="header-logo">
                <img src="<?=asset('assets/img/logo-clikc.png')?>" alt="Logo Clikc" title="Logo Clikc" />
            </a>
            <?php
        }
    } else {
        ?>
        <div id="header-logo">
            <img src="<?=asset('assets/img/logo-clikc.png')?>" alt="Logo Clikc" title="Logo Clikc" />
        </div>
        <?php
    }
    ?>
@else
    <div id="header-logo">
        <img src="<?=asset('assets/img/logo-clikc.png')?>" alt="Logo Clikc" title="Logo Clikc" />
    </div>
@endif
<div id="eu-logo-container">
    <img src="<?=asset('assets/img/logo-eu.png')?>" alt="Logo EU" title="Logo EU" />
</div>
<div id="header-buttons-cta">
    &#9776
</div>


<div id="header-buttons">
    @if (Auth::id() > 0)
        <?php
        $mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
        if (request()->request->get('isSuperUser')) {
            if ($mode === 'basic') {
                ?><a href="/switch-to-super-user" title="Market Operator mode" alt="Market Operator mode" class="header-button-item">Market Operator mode</a><?php
            } else {
                ?><a href="/switch-to-basic-user" title="Basic user mode" alt="Basic user mode" class="header-button-item">Back to basic user mode</a><?php
            }
        } else {
            $mode = 'basic';
        }
        ?>
        <?php if ($mode === 'basic') { ?>
            <a href="/dashboard" title="Dashboard" alt="Dashboard" class="header-button-item">
                Dashboard
            </a>
        <?php } ?>
        <form method="POST" action="{{ route('logout') }}" id="header-logout" class="header-button-item">
            @csrf
            <button title="Logout">
                <span>Sign out</span>
                <img src="<?=asset('assets/img/login.svg')?>" alt="Logout" title="Logout" />
            </button>
        </form>
    @endif
</div>
