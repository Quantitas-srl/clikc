<table id="registry-table">
    <thead>
        <tr>
            <th>Learning Unit Title</th>
            <th>Skill</th>
            <th>L/D</th>
            <th>Started</th>
            <th>Completed</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data as $datum) {
            $link = '';
            $name = '';
            if (isset($datum->learning_unit_id)) {
                $link = '/learning-unit/' . $datum->learning_unit_id . '/show';
            } else {
                $link = '/learning-unit-labour-market/' . $datum->learning_unit_labour_market_id . '/show';
            }
            ?>
            <tr data-link="<?=$link?>">
                <td>
                    <div class="td-content">
                        <?=$datum->title?>
                    </div>
                </td>
                <td>
                    <div class="td-content">
                        <?=$datum->cluster?>
                    </div>
                </td>
                <td data-order="<?=$datum->liked?>">
                    <?php if (+$datum->liked === 1) { ?>
                        <img src="{{ asset('assets/img/liked.svg') }}" alt="Liked" title="Liked" />
                    <?php } else { ?>
                        <img src="{{ asset('assets/img/disliked.svg') }}" alt="Disliked" title="Disliked" />
                    <?php } ?>
                </td>
                <td data-order="<?=$datum->started_on?>">
                    <?=$datum->started_on_ago?>
                </td>
                <td data-order="<?=$datum->completed_on?>">
                    <?=$datum->completed_on_ago?>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<?php
if (!count($data) && !$isSuperUserRequestingForAnotherUser) {
    ?>
    <br />
    <br />
    <div class="text-center">
        <a class="button primary" title="Start your first Learning Unit" href="ia/next">
            Start your first Learning Unit
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
    <?php
}
?>