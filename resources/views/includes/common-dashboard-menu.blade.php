<?php

$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = request()->request->get('isSuperUser') && filter_input(INPUT_GET, 'uid') && ($mode === 'super');
$now = time();

$links = array(
    array(
        'base-href' => 'dashboard',
        'title' => 'Learning Units Registry',
        'id' => 'registry-nav-button',
        'img' => 'dashboard-link-to-learning-unit',
        'alt' => 'Learning Units'
    ),
    array(
        'base-href' => 'report',
        'title' => 'View report',
        'id' => 'report-nav-button',
        'img' => 'dashboard-report',
        'alt' => 'ViewReport'
    ),
    array(
        'base-href' => 'profile',
        'title' => 'Profile',
        'id' => 'profile-nav-button',
        'img' => 'dashboard-profile',
        'alt' => 'Profile'
    )
);

foreach ($links as $l=>$link) {
    $links[$l]['href'] = $links[$l]['base-href'];
}

if ($isSuperUserRequestingForAnotherUser) {
    $puid = filter_input(INPUT_GET, 'uid');
    foreach ($links as $l=>$link) {
        $links[$l]['href'] .= '?uid=' . $puid;
    }
}



?>

<div class="text-center">

    @if ($isSuperUserRequestingForAnotherUser)
        <div id="back-to-users-container">
            <a class="button primary" id="back-to-users" href="/superuser/users-list" title="Back to users">
                Back to users
                <img class="arrow" src="{{asset('assets/img/arrow-left.svg')}}" alt="Back to users" title="Back to users" />
            </a>
        </div>
        <style>
            #back-to-users-container {
                display: inline-block;
                margin-top: 20px;
                vertical-align: middle;
            }
            #navigation-container {
                display: inline-block;
                vertical-align: middle;
                width: 520px;
            }
            @media (max-width: 700px) {
                #navigation-container {
                    width: 100%;
                }
            }
        </style>
    @endif

    <div id="navigation-container">
        @foreach ($links as $link)
            @php
            $img = '/assets/img/' . $link['img'] . '.svg';
            $imgHover = '/assets/img/' . $link['img'] . '-hover.svg';
            $selected = Request::path() === $link['base-href'] || Request::path() === 'superuser/' . $link['base-href'];
            @endphp
            <a href="{{$link['href']}}" title="{{$link['title']}}" id="{{$link['id']}}" class="<?=$selected?'selected':''?>">
                <img class="normal-state" src="{{$img}}" alt="{{$link['alt']}}" title="{{$link['alt']}}" />
                <img class="hover-state" src="{{$imgHover}}" alt="{{$link['alt']}}" title="{{$link['alt']}}" />
                <button class="button primary">
                    {{$link['alt']}}
                </button>
            </a>
        @endforeach
    </div>

</div>