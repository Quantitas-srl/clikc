<?php if (!isset($_COOKIE['clikc-cookies'])) { ?>
    <div id="cookies-message">
        <p>This website uses technical cookies for its proper functioning.</p>
        <button class="button secondary" onclick="cookies()">Understood</button>
        <a class="button secondary" href="terms-and-conditions">Read more</a>
    </div>
    <style>
        #cookies-message {
            background: #000;
            bottom: 0;
            color: #fff;
            left: 0;
            padding: 30px 10px;
            position: fixed;
            width: 100%;
            z-index: 5;
        }
        #cookies-message button {
            margin: 0px 4px;
        }
        #cookies-message.removed {
            transform: translate(0, 100%);
            transition: transform 1s;
        }
    </style>
    <script>
        function setCookie(cname, cvalue, exdays) {
            const d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            let expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        function cookies() {
            setCookie('clikc-cookies', 1, 30);
            document.getElementById('cookies-message').classList.add('removed');
        }
    </script>
<?php } ?>