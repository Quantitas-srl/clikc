<?php
date_default_timezone_set('Europe/Rome');
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Lexend+Deca:wght@200;250;300;350;365;375;400;500;550;700&display=swap" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.png') }}">
        <title>@yield('title')</title>
    </head>
    <body class="font-sans antialiased">
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
        <div class="min-h-screen bg-gray-100">
            <script src="{{ asset('assets/js/app.js') }}"></script>
            <div class="container">
                <header class="row">
                    @include('includes.header')
                </header>
                <div id="main" class="row">
                    @yield('content')
                </div>
                <footer class="row">
                </footer>
                <script src="{{ asset('assets/js/events.js') }}"></script>
            </div>
        </div>
        @include('includes.cookies')
        @yield('css')
    </body>
</html>