@extends('layouts.default')

@section('title')
emails creator
@stop

@section('content')

    <a href="{{$filename}}" title="Download">Download</a>

    <table>
    @foreach ($data as $datum)
        <tr>
            <td>{{ $datum['name'] }}</td>
            <td>{{ $datum['email'] }}</td>
            <td>{{ $datum['password'] }}</td>
        </tr>
    @endforeach
    </table>

@stop