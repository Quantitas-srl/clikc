<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Recap test
@stop


@section('content')

<link href="{{ asset('assets/css/test.css') }}" rel="stylesheet">



<div class="lu-content">
    
    @if ($message)
        <p class="alert">{{ $message }}</p>
    @else
        <img src="{{asset('assets/img/circled-confirm.svg')}}" alt="Recap test" title="Recap test" class="pull-right" />

        <div class="lu-title">
            <h1>Welcome to your {{ $nRecapsLiteral }} recap test!</h1>
        </div>

        <form method="POST" action="">
            <input type="hidden" value="1" id="is-recap-test" />

            @csrf <!--https://stackoverflow.com/questions/52583886/post-request-in-laravel-error-419-sorry-your-session-419-your-page-has-exp-->
            <?php
            $t = new TestController(null, null);
            $n = 0;
            foreach ($testsData as $testData) {
                $savedData = isset($savedTestsData[$testData['id']]) ? $savedTestsData[$testData['id']] : null;
                $t = new TestController($testData, $savedData);
                $t->show(++$n, false); // always false, i.e. always not-superuser
            }
            ?>
        </form>

        <div id="tests-recap">
            Completed tests:
            <span id="completed-tests-amount">
            </span>
            <div id="tests-continue">
                <a class="button primary hidden" title="Continue" href="/recap-test-completed" id="continue">
                    Continue
                </a>
            </div>
        </div>
        <script src="{{ asset('assets/js/test.js') }}"></script>
    @endif

</div>

@stop


