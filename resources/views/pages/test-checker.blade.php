<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Test checker
@stop


@section('content')

<style>
    .test {
        margin: 50px 0 100px 0;
    }
    h2 {
        border-top: 1px solid #303030;
        margin: 0 0 20px 0;
    }
    h3 {
        margin: 15px 0 5px 0;
    }
    .test a {
        display: block;
        margin-bottom: 10px;
    }
    .test table {
        border-collapse: collapse;
    }
    .test td {
        border: 1px solid #e0e0e0;
        padding: 10px;
    }
    .test td:nth-child(1) {
        white-space: nowrap;
    }
    .test .error {
        background: #faf0f0;
    }
    .test .warning {
        background: #fafaf0;
    }
</style>

<div id="test-checker">
    <?php

    $identifierMap = array(
        '1a' => 'Tecum',
        '1b' => 'Acción Laboral',
        '1c' => 'Tecum',
        '2a' => 'bitSC',
        '2b' => 'Tecum',
        '2c' => 'Tecum',
        '3a' => 'bitSC',
        '3b' => 'Acción Laboral',
        '3c' => 'Acción Laboral',
        '4a' => 'bitSC',
        '4b' => 'bitSC',
        '4c' => 'Acción Laboral'
    );
    foreach ($data as $luid=>$lu) {
        $temp = reset($lu);
        $first = reset($temp);
        $name = $identifierMap[strtolower(substr(str_replace(' ', '', str_replace('.', '', $first['identifier'])), 0, 2))] ?? '??';
        ?>
        <div class="test" data-order="<?=$first['identifier']?>">
            <h2>Learning Unit <?=$luid?> - <?=$first['identifier']?> - <?=$name?></h2>       
            <?php foreach ($lu as $testId=>$test) {
                $first = reset($test);
                $typeLink = $first['type'];
                if ($typeLink === 'matching-terms') {
                    $typeLink = 'matching-term';
                }
                ?> 
                <h3><?=ucwords(str_replace('-', ' ', $first['type']))?> <?=+$first['index']+1?></h3>
                <a href="https://clikc-quantitas.it/en/edit/<?=$luid?>#<?=$typeLink?>-<?=$testId?>" target="_blank">
                    https://clikc-quantitas.it/en/edit/<?=$luid?>#<?=$typeLink?>-<?=$testId?>
                </a>
                <table>
                    <?php foreach ($test as $error) { ?> 
                        <tr class="<?=$error['level']?>">
                            <td>
                                <?=$error['level']?>
                            </td>
                            <td>
                                <?=$error['text']?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            <?php } ?>
        </div>
        <?php
    }
    ?>
</div>

<script>
    var items = Array.from(document.querySelectorAll('.test'));
    for (let i = 0; i < items.length-1; i++) {
        for (let j = i+1; j < items.length; j++) {
            if (items[i].getAttribute('data-order') > items[j].getAttribute('data-order')) {
                const temp = items[i];
                items[i] = items[j];
                items[j] = temp;
            }
        }
    }
    var s = items.map(item => item.outerHTML).join('');
    document.getElementById('test-checker').innerHTML = s;
</script>


@stop