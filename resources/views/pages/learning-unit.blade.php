<?php
namespace App;
use App\Http\Controllers\TestController;


header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.

?>
@extends('layouts.default')

@section('title')
Learning Unit - {{ $learning_unit->title }}
@stop


@section('content')

<?php
$uid = filter_input(INPUT_GET, 'ref');
$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = $isSuperUser && ($mode === 'super');
?>

<?php
function clearStyle($text) {
    //return preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
    return preg_replace('/style="[^"]+"/i', '$1', $text);
}
?>

<link href="{{ asset('assets/css/lu.css') }}" rel="stylesheet">

<div id="tabs-selector">
    <div class="tab-selector" tab-index="1">
        <div class="tab-selector-number">1</div>
        <span>Learning Unit</span>
    </div>
    <div class="tab-selector-separator"></div>
    <div class="tab-selector disabled" tab-index="2">
        <div class="tab-selector-number">2</div>
        <span>Learning Unit Tests</span>
    </div>
</div>

<div class="lu-cluster">
    <p>
        {{ $clusters[+$learning_unit->cluster_number] }}
    </p>
</div>


<div class="lu-content">
    
    <div class="lu-title">
        <h1>{{ $learning_unit->title }}</h1>
    </div>

    <div class="lu-subtitle">
        <h2>{{ $learning_unit->subtitle }}</h2>
    </div>

    <?php if (isset($learning_unit->introduction) && $learning_unit->introduction) { ?>
        <div class="lu-introduction">
            <?= clearStyle($learning_unit->introduction); ?>
        </div>
    <?php } ?>

    @if ($learning_unit->clil_sore_unit_link)
        <div class="lu-clil">
            <a title="View Unit in a multilingual environment for word translations and grammar hints" href="{{ $learning_unit->clil_sore_unit_link }}" target="_blank" rel="noopener">View Learning Unit in CLILSTORE</a>
        </div>
    @endif

    <?php if (isset($learning_unit->text_area) && $learning_unit->text_area) { ?>
        <div class="lu-paragraph">
            <?= clearStyle($learning_unit->text_area); ?>
        </div>
    <?php } ?>



    <?php
    foreach ($dinamic_fields as $dinamic_field) {
        $type = $dinamic_field->type;
        if ($type === 'image') {
            showImage($dinamic_field->content);
        } else if ($type === 'memory-box') {
            showMemoryBox($dinamic_field->content);
        } else if ($type === 'paragraph') {
            showParagraph($dinamic_field->content);
        } else if ($type === 'references') {
            showReferences($dinamic_field->content);
        } else if ($type === 'note') {
            showLanguagePoint($dinamic_field->content);
        } else if ($type === 'audio') {
            showAudio($dinamic_field->content);
        }
    }
    ?>



</div> <!-- end of .lu-content -->

<?php if (!$isSuperUserRequestingForAnotherUser && $editable) { ?>
    <div id="lu-liked-box" class="lu-liked-box text-center">
        <p id="lu-liked-box-title">
            Did you like this learning unit?
        </p>


        <div id="lu-liked-box-buttons-container">
            <button id="lu-liked" data-luid="{{ $learning_unit->id }}" title="Yes" class="<?= (isset($user_status) && +$user_status->liked === 1) ? 'active' : '' ?>">
                <img class="like" src="{{ asset('assets/img/like.png') }}" alt="Like" title="Like" />
                <img class="like-hover" src="{{ asset('assets/img/like-hover.png') }}" alt="Like" title="Like" />
                <img class="like-pressed" src="{{ asset('assets/img/like-pressed.png') }}" alt="Like" title="Like" />
                <img class="like-selected" src="{{ asset('assets/img/like-selected.png') }}" alt="Like" title="Like" />
                <span>Yes I did</span>
            </button>
            <button id="lu-not-liked" data-luid="{{ $learning_unit->id }}" title="No" class="<?= (isset($user_status) && +$user_status->liked === -1) ? 'active' : '' ?>">
                <img class="dislike" src="{{ asset('assets/img/dislike.png') }}" alt="Dislike" title="Dislike" />
                <img class="dislike-hover" src="{{ asset('assets/img/dislike-hover.png') }}" alt="Dislike" title="Dislike" />
                <img class="dislike-pressed" src="{{ asset('assets/img/dislike-pressed.png') }}" alt="Dislike" title="Dislike" />
                <img class="dislike-selected" src="{{ asset('assets/img/dislike-selected.png') }}" alt="Dislike" title="Dislike" />
                <span>No I didn't</span>
            </button>
        </div>

        

    </div>
<?php } ?>



<?php
$disabled = '';
if (!isset($user_status)) {
    $disabled = 'disabled';
} else if (isset($user_status) && +$user_status->liked === 0) {
    $disabled = 'disabled';
}
?>

@if (!$isSuperUserRequestingForAnotherUser && $editable)
    <div id="lu-go-to-test-container" class="lu-go-to-test-container">
        <button id="lu-go-to-test" href="test" title="Go to Learning Unit test" class="button primary" <?=$disabled?>>
            Go To Learning Unit Test
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </button>
    </div>
@else
    @if ($isSuperUserRequestingForAnotherUser)
        <a class="button primary" title="Dashboard" href="/superuser/dashboard?uid=<?=$uid?>">
            Dashboard
        </a>
        <a class="button primary" title="View tests" href="test?ref=<?=$uid?>">
            View tests
        </a>
    @else
        <a class="button primary" title="Dashboard" href="/dashboard">
            Dashboard
        </a>
        <a class="button primary" title="View tests" href="test">
            View tests
        </a>
    @endif
@endif

<script src="{{ asset('assets/js/lu.js') }}"></script>



@stop



<?php



function showImage($src) {
    $url = 'https://clikc-quantitas.it/public/assets/images/upload/' . $src;
    list($width, $height, $type, $attr) = getimagesize($url);
    $ratio = $width / $height;
    $wh = '';
    $class = 'lu-image';
    if ($width < 700) {
        $wh = 'width="' . $width . '" height="' . $height . '"';
        $class .= ' lu-image-small';
    }
    ?>
    <div class="<?=$class?>">
        <img src="<?=$url?>" alt="Image" title="Image" <?=$wh?> />
    </div>
    <?php
}

function showAudio($src) {
    $url = 'https://clikc-quantitas.it/public/assets/images/upload/' . $src;
    ?>
    <div class="lu-audio">
        <audio controls>
            <source src="<?=$url?>" type="audio/mpeg">
            Your browser does not support the audio element.
        </audio>
    </div>
    <?php
}

function showMemoryBox($text) {
    ?>
    <div class="lu-memory-box">
        <img loading="lazy" class="lu-memory-box-lamp" src="<?=asset('assets/img/lamp.svg')?>" alt="Lamp image" title="Memory box" />
        <?= clearStyle($text) ?>
    </div>
    <?php
}

function showParagraph($text) {
    ?>
    <div class="lu-paragraph">
        <?= clearStyle($text) ?>
    </div>
    <?php
}

function showReferences($text) {
    $text = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $text);
    ?>
    <div class="lu-references">
        <?= clearStyle($text)?>
    </div>
    <?php
}

function showLanguagePoint($text) {
    ?>
    <div id="language-point-opener" class="language-point-cta">
        <img loading="lazy" src="<?=asset('assets/img/language-point.svg')?>" alt="Language Point" title="Language Point" />
        <p>Language Point</p>
        <span>View language point</span>
    </div>
    <div id="language-point" class="hidden">
        <div id="language-point-container">
            <div id="language-point-closer" class="language-point-cta">
                <img loading="lazy" src="<?=asset('assets/img/language-point.svg')?>" alt="Language Point" title="Language Point" />
                <p>Language Point</p>
                <span>Close language point</span>
            </div>
            <div id="language-point-content">
                <?= clearStyle($text) ?>
            </div>
        </div>
    </div>
    <?php
}



?>
