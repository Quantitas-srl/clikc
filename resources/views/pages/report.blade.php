@extends('layouts.default')

@section('title')
Dashboard
@stop

@section('content')

<link rel="stylesheet" href="{{ asset('assets/css/common-dashboard-menu.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/report.css') }}" />


<?php
$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = request()->request->get('isSuperUser') && filter_input(INPUT_GET, 'uid') && ($mode === 'super');
$now = time();


$isGroupSuperuser = false;
$group = '';
if ($isSuperUserRequestingForAnotherUser) {
    $superuserEmailParts1 = explode('@', $loggedUserEmail);
    $superuserEmailParts2 = explode('-', $superuserEmailParts1[0]);
    $group = $superuserEmailParts2[1] ?? '';
    $user = $superuserEmailParts2[2] ?? '';
    if (intval($group) > 0 && intval($group) <= 99) {
        $isGroupSuperuser = true;
    }
}


if ($isGroupSuperuser && strpos((json_decode($personalDetails->other ? $personalDetails->other : '{"email":""}'))->email, '-'.$group.'-') === false) {
    echo 'You cannot view this user.';
} else {

    ?>



    <?php
    function writeChartIndicator($title, $value, $border) {
        ?>
        <div class="chart-indicator">
            <p class="chart-indicator-title" style="border-bottom:<?=$border?>"><?=$title?></p>
            <p class="chart-indicator-value"><?=$value?></p>
        </div>
        <?php
    }

    function writeChartIndicators($data, $id = '') {
        ?><div class="chart-indicators" id="<?=$id?>"><?php
        foreach ($data as $datum) {
            writeChartIndicator($datum['title'], $datum['value'], $datum['border'] ?? '');
        }
        ?></div><?php
    }


    // data for access and usage
    $firstTime = $firstDate ? strtotime($firstDate) : 0;
    $lastTime = $lastDate ? strtotime($lastDate) : 0;

    // data for progress bar
    $nCompleted = array_reduce($nCompletedByCluster, function($acc, $item) { return $acc + $item->n; }, 0);
    $nLUs = array_reduce($nLUsByCluster, function($acc, $item) { return $acc + $item->n; }, 0);
    $percCompleted = round(100/$nLUs*$nCompleted, 1);

    // count trophies
    $totTrophies = floor($nLUs/10);
    $accTrophies = 0;
    $naccTrophies = 0;
    foreach ($recaps as $recap) {
        if ($recap->score >= 60) {
            $accTrophies++;
        } else {
            $naccTrophies++;
        }
    }

    // compute tests results
    $okTests = 0;
    $koTests = 0;
    $testsSumResult = 0;
    foreach ($tests as $test) {
        $testsSumResult += $test->score;
        if ($test->score >= 60) {
            $okTests++;
        } else {
            $koTests++;
        }
    }
    $correctAnswerRate = $okTests + $koTests > 0 ? round($testsSumResult/($okTests + $koTests), 1) : 0;

    // compute tests results on recap test page
    $rokTests = 0;
    $rkoTests = 0;
    $rtestsSumResult = 0;
    foreach ($rtests as $rtest) {
        $rtestsSumResult += $rtest->score;
        if ($rtest->score >= 60) {
            $rokTests++;
        } else {
            $rkoTests++;
        }
    }
    $correctRetentionAnswerRate = $rokTests + $rkoTests > 0 ? round($rtestsSumResult/($rokTests + $rkoTests), 1) : 0;

    // compute data for tests by cluster
    $zeros = array_fill(0, count($clusters)+1, 0);
    $dataByCluster = array($zeros, $zeros);
    foreach ($testsByCluster as $testByCluster) {
        $dataByCluster[$testByCluster->score < 60 ? 0 : 1][$testByCluster->cluster_number]++;
    }


    ?>




    <div class="title-container">
        @if ($isSuperUserRequestingForAnotherUser)
            <h1 class="title">{{ $personalDetails->name }} {{ $personalDetails->surname }}</h1>
        @else
            @if ($hasStarted)
                <h1 class="title">Welcome back!</h1>
            @else
                <h1 class="title">Welcome!</h1>
            @endif
        @endif
    </div>
    <div id="welcome-subtitle">
        @if ($isSuperUserRequestingForAnotherUser)
            <div>
                Last access
            </div>
            <div class="date">
                {{ date('d/m/y', $lastAccessTime) }}
            </div>
            <div class="date">
                {{ date('H:i', $lastAccessTime) }}
            </div>
        @else
            <div>
                {{ $personalDetails->name }} {{ $personalDetails->surname }}
            </div>
            <div class="date">
                {{ date('d/m/y', time()) }}
            </div>
            <div class="date">
                {{ date('H:i', time()) }}
            </div>
        @endif
    </div>



    @include('includes.common-dashboard-menu')




    <div id="report-container" data-hash="<?=md5(time().rand(0,999).Auth::id())?>">

        <h2>
            Reports and statistics
            <button class="button secondary pull-right" id="hide-details">
                Hide details <img src="{{ asset('assets/img/hide.svg') }}" alt="Hide" title="Hide" />
            </button>
            <button class="button secondary pull-right hidden" id="show-details">
                Show details <img src="{{ asset('assets/img/show.svg') }}" alt="Show" title="Show" />
            </button>
        </h2>


        <h3>Access and usage</h3>
        <?php
        writeChartIndicators(array(
            array('title' => 'First access', 'value' => $firstTime ? (date('d/m/Y', $firstTime).'<br />'.date('H:i', $firstTime)) : '-'),
            array('title' => 'Last access', 'value' => $lastTime ? (date('d/m/Y', $lastTime).'<br />'.date('H:i', $lastTime)) : '-'),
            array('title' => 'Top month', 'value' => $bestMonth === -1 ? '-' : $bestMonth->month.'/'.$bestMonth->year.' ('.$bestMonth->n.')'),
            array('title' => 'Learning Units per week (avg)', 'value' => round($dayAvg*7, 2)),
            array('title' => 'Learning Units per day (avg)', 'value' => round($dayAvg, 2), 'border' => '2px solid #FE9900')
        ), 'access-and-usage-indicators');
        ?>
        <div class="chart-container detail" style="height:250px;">
            <canvas id="chart-access-and-usage"></canvas>
        </div>



        <h3>Learning path</h3>

        <h4>Overall</h4>
        <?php
        writeChartIndicators(array(
            array('title' => 'Completed Learning Units', 'value' => $nCompleted),
            array('title' => 'Total Learning Units', 'value' => $nLUs),
            array('title' => 'Completed Learning Units (%)', 'value' => $percCompleted, 'border' => '2px solid #FE9900')
        ), 'learning-path-indicators');
        ?>
        <div class="chart-container">
            <progress title="<?=$percCompleted?>%" data-value="<?=$percCompleted?>" max="100" id="overall-progress"></progress>
        </div>

        <div class="detail">
            <div id="learning-path-by-skill-legend">
                <progress data-value="100" max="100"></progress>
                <span>Completed</span>
                <progress data-value="0" max="100"></progress>
                <span>Total</span>
            </div>
            <h4>Learning path by skill</h4>
            <div class="chart-container">
                <?php
                foreach ($nLUsByCluster as $k=>$ldatum) {
                    $perc = 0;
                    foreach ($nCompletedByCluster as $datum) {
                        if (+$ldatum->cluster_number === +$datum->cluster_number) {
                            $perc = 100/$ldatum->n*$datum->n;
                            break;
                        }
                    }
                    ?>
                    <div class="short-progress-container">
                        <label><?=$clusters[$ldatum->cluster_number]?></label>
                        <progress id="progress-skill-<?=$k?>" class="short" title="<?=$perc?>%" data-value="<?=$perc?>" max="100"></progress>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>



        <h3>Scores and performance</h3>

        <h4>Trophies</h4>
        <?php
        writeChartIndicators(array(
            array('title' => 'Total trophies', 'value' => $totTrophies),
            array('title' => 'Trophies accomplished', 'value' => $accTrophies . '/' . ($accTrophies+$naccTrophies), 'border' => '2px solid #FE9900')
        ), 'trophies-indicators');
        ?>
        <div id="trophies">
            <div class="detail">
                <?php
                $n = 0;
                foreach ($recaps as $recap) {
                    if ($recap->score >= 60) {
                        ?><img data-src="little-trophy" src="{{ asset('assets/img/little-trophy.svg') }}" alt="Trophy gained" title="Trophy gained. Score: <?=round($recap->score)?>/100" /><?php
                    } else {
                        ?><img data-src="little-trophy-no" src="{{ asset('assets/img/little-trophy-no.svg') }}" alt="Trophy missed" title="Trophy missed. Score: <?=round($recap->score)?>/100" /><?php
                    }
                    $n++;
                }
                while ($n < $totTrophies) {
                    ?><img data-src="little-trophy-not-yet" src="{{ asset('assets/img/little-trophy-not-yet.svg') }}" alt="Trophy next" title="Trophy next" /><?php
                    $n++;
                }
                ?>
            </div>
        </div>



        <h4>Learning Units tests overall</h4>
        <?php
        writeChartIndicators(array(
            array('title' => 'Total tests', 'value' => $okTests + $koTests),
            array('title' => 'Tests failed', 'value' => $koTests, 'border' => '3px solid #FFD8D8'),
            array('title' => 'Tests passed', 'value' => $okTests, 'border' => '3px solid #D8FFD8')
        ), 'tests-indicators');
        ?>
        <div class="detail">
            <div id="tests-overall-left">
                <div>
                    <p>Correct Answers Rate</p>
                    <p><?=$correctAnswerRate?>/100</p>
                </div>
                <div>
                    <p>Retention Rate</p>
                    <p><?=$correctRetentionAnswerRate?>/100</p>
                </div>
            </div>
            <div id="tests-overall-right">
                <div class="chart-container" style="height:250px;">
                    <canvas id="chart-tests-overall"></canvas>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>


        <div class="detail">
            <h4>Learning Units tests by skill</h4>
            <div class="chart-container" style="height:350px;">
                <canvas id="chart-tests-by-cluster"></canvas>
            </div>
        </div>




        @if ($isSuperUserRequestingForAnotherUser)

            <br />
            <br />

            <h3>Learning Units Reception</h3>
            <?php
            writeChartIndicators(array(
                array('title' => 'Learning Units completed', 'value' => $nCompleted),
                array('title' => 'Learning Units liked', 'value' => $likedLUs),
                array('title' => 'Learning Units disliked', 'value' => $dislikedLUs),
                array('title' => 'Positive reception rate', 'value' => ($likedLUs+$dislikedLUs > 0 ? round((100/($likedLUs+$dislikedLUs)) * $likedLUs) : 0) . '%', 'border' => '3px solid #FE9900')
            ), 'reception-indicators');
            ?>
            <div class="detail text-center">

                <svg class="thumb-svg" width="250" height="249" viewBox="0 0 250 249" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g filter="url(#filter0_d_2822_13875)"> <!-- shadow -->
                        <path d="M140.893 170.121C92.5519 170.121 92.1767 168.116 86.1904 163.608C83.015 161.219 77.0235 159.563 55.1361 158.137C53.5457 158.033 52.0378 157.424 51.0246 156.196C50.5591 155.629 39.6235 139.68 39.6235 114.139C39.6235 88.9673 47.4502 77.4917 47.7852 76.9572C48.2632 76.1929 48.9278 75.5628 49.7163 75.1259C50.5048 74.6891 51.3915 74.4599 52.293 74.46C70.2629 74.46 95.1741 51.5221 111.923 11.4786C114.82 4.55338 115.415 0.000556301 126.056 0.000557231C132.106 0.00055776 138.888 4.33282 142.241 9.89109C149.108 21.247 145.621 45.6371 142.393 58.6713C153.363 58.5833 173.221 58.4319 182.67 58.4319C195.813 58.4319 204.51 66.2692 204.778 77.729C204.864 81.5107 204.369 86.3429 203.273 88.8375C206.164 91.723 209.898 96.0767 210.06 101.68C210.265 108.911 205.526 114.419 202.505 117.581C203.196 119.685 204.584 122.475 204.451 125.81C204.116 134.011 197.787 139.298 193.84 142.056C194.165 144.293 194.42 148.529 193.702 151.8C190.856 164.869 171.695 170.121 140.893 170.121ZM57.9599 147.348C77.023 148.925 87.2133 151.061 92.5881 155.108C96.7475 158.239 96.1039 159.48 140.892 159.48C154.551 159.48 181.265 158.911 183.313 149.537C184.129 145.787 180.175 142.098 180.159 142.072C179.079 139.404 180.284 136.279 182.917 135.128C182.959 135.107 193.576 131.301 193.82 125.365C193.977 121.57 191.374 119.291 191.313 119.198C189.765 116.794 190.331 113.493 192.68 111.858C192.706 111.836 199.589 107.666 199.419 101.978C199.288 97.4646 193.339 94.2839 193.158 94.1775C191.829 93.4169 190.861 92.1005 190.56 90.5926C190.26 89.0925 190.597 87.5129 191.528 86.2976C191.528 86.2976 194.233 81.9972 194.137 77.9681C193.94 69.3355 184.659 69.0643 182.667 69.0643C169.25 69.0643 134.792 69.2052 134.792 69.2052C132.933 69.2105 131.324 68.3329 130.326 66.8463C129.329 65.3596 129.252 63.5111 129.84 61.8224C135.034 46.8576 137.935 23.3799 133.116 15.3589C131.699 12.9947 131.372 10.6331 126.053 10.6331C125.316 10.6331 123.561 11.2155 121.731 15.5772C103.947 58.0966 77.4006 82.6966 55.6567 84.9143C53.7818 89.1907 50.2553 98.052 50.2553 114.134C50.2606 130.545 55.4599 143.239 57.9599 147.348Z" fill="#FEF5E8"/>
                        <path d="M57.9599 147.348C77.023 148.925 87.2133 151.061 92.5881 155.108C96.7475 158.239 96.1039 159.48 140.892 159.48C154.551 159.48 181.265 158.911 183.313 149.537C184.129 145.787 180.175 142.098 180.159 142.072C179.079 139.404 180.284 136.279 182.917 135.128C182.959 135.107 193.576 131.301 193.82 125.365C193.977 121.57 191.374 119.291 191.313 119.198C189.765 116.794 190.331 113.493 192.68 111.858C192.706 111.836 199.589 107.666 199.419 101.978C199.288 97.4646 193.339 94.2839 193.158 94.1775C191.829 93.4169 190.861 92.1005 190.56 90.5926C190.26 89.0925 190.597 87.5129 191.528 86.2976C191.528 86.2976 194.233 81.9972 194.137 77.9681C193.94 69.3355 184.659 69.0643 182.667 69.0643C169.25 69.0643 134.792 69.2052 134.792 69.2052C132.933 69.2105 131.324 68.3329 130.326 66.8463C129.329 65.3596 129.252 63.5111 129.84 61.8224C135.034 46.8576 137.935 23.3799 133.116 15.3589C131.699 12.9947 131.372 10.6331 126.053 10.6331C125.316 10.6331 123.561 11.2155 121.731 15.5772C103.947 58.0966 77.4006 82.6966 55.6567 84.9143C53.7818 89.1907 50.2553 98.052 50.2553 114.134C50.2606 130.545 55.4599 143.239 57.9599 147.348Z" fill="#FEF5E8"/>
                    </g>
                    <mask id="mask0_2822_13875" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="39" y="0" width="172" height="171">
                        <path d="M140.893 170.121C92.5519 170.121 92.1767 168.116 86.1904 163.608C83.015 161.219 77.0234 159.563 55.1361 158.137C53.5457 158.033 52.0378 157.424 51.0246 156.196C50.5591 155.629 39.6235 139.68 39.6235 114.139C39.6235 88.9673 47.4503 77.4917 47.7852 76.9572C48.2632 76.1929 48.9278 75.5628 49.7163 75.1259C50.5048 74.6891 51.3915 74.4599 52.293 74.46C70.2629 74.46 95.1741 51.5221 111.923 11.4786C114.82 4.55338 115.415 0.000551498 126.056 0.000552428C132.106 0.000552957 138.888 4.33281 142.241 9.89109C149.108 21.247 145.621 45.6371 142.393 58.6712C153.363 58.5833 173.221 58.4319 182.67 58.4319C195.813 58.4319 204.51 66.2692 204.778 77.729C204.864 81.5107 204.369 86.3429 203.273 88.8375C206.164 91.723 209.898 96.0767 210.06 101.68C210.265 108.911 205.526 114.419 202.505 117.581C203.196 119.685 204.584 122.475 204.451 125.81C204.116 134.011 197.787 139.298 193.84 142.056C194.165 144.293 194.42 148.529 193.702 151.8C190.856 164.869 171.695 170.121 140.893 170.121ZM57.9599 147.348C77.023 148.925 87.2133 151.061 92.5881 155.108C96.7475 158.239 96.1039 159.48 140.892 159.48C154.551 159.48 181.265 158.911 183.313 149.537C184.129 145.787 180.175 142.098 180.159 142.072C179.079 139.404 180.284 136.279 182.917 135.128C182.959 135.107 193.576 131.301 193.82 125.365C193.977 121.57 191.374 119.291 191.313 119.198C189.765 116.794 190.331 113.493 192.68 111.858C192.706 111.836 199.589 107.666 199.419 101.978C199.288 97.4646 193.339 94.2839 193.158 94.1775C191.829 93.4169 190.861 92.1005 190.56 90.5926C190.26 89.0925 190.597 87.5129 191.528 86.2976C191.528 86.2976 194.233 81.9972 194.137 77.9681C193.94 69.3355 184.659 69.0643 182.667 69.0642C169.25 69.0642 134.792 69.2052 134.792 69.2052C132.933 69.2105 131.324 68.3329 130.326 66.8463C129.329 65.3596 129.252 63.5111 129.84 61.8224C135.034 46.8576 137.935 23.3799 133.116 15.3589C131.699 12.9947 131.372 10.6331 126.053 10.6331C125.316 10.6331 123.561 11.2155 121.731 15.5772C103.947 58.0966 77.4006 82.6966 55.6567 84.9143C53.7818 89.1907 50.2553 98.052 50.2553 114.134C50.2606 130.545 55.4599 143.239 57.9599 147.348Z" fill="#000033"/>
                        <path d="M57.9599 147.348C77.023 148.925 87.2133 151.061 92.5881 155.108C96.7475 158.239 96.1039 159.48 140.892 159.48C154.551 159.48 181.265 158.911 183.313 149.537C184.129 145.787 180.175 142.098 180.159 142.072C179.079 139.404 180.284 136.279 182.917 135.128C182.959 135.107 193.576 131.301 193.82 125.365C193.977 121.57 191.374 119.291 191.313 119.198C189.765 116.794 190.331 113.493 192.68 111.858C192.706 111.836 199.589 107.666 199.419 101.978C199.288 97.4646 193.339 94.2839 193.158 94.1775C191.829 93.4169 190.861 92.1005 190.56 90.5926C190.26 89.0925 190.597 87.5129 191.528 86.2976C191.528 86.2976 194.233 81.9972 194.137 77.9681C193.94 69.3355 184.659 69.0643 182.667 69.0642C169.25 69.0642 134.792 69.2052 134.792 69.2052C132.933 69.2105 131.324 68.3329 130.326 66.8463C129.329 65.3596 129.252 63.5111 129.84 61.8224C135.034 46.8576 137.935 23.3799 133.116 15.3589C131.699 12.9947 131.372 10.6331 126.053 10.6331C125.316 10.6331 123.561 11.2155 121.731 15.5772C103.947 58.0966 77.4006 82.6966 55.6567 84.9143C53.7818 89.1907 50.2553 98.052 50.2553 114.134C50.2606 130.545 55.4599 143.239 57.9599 147.348Z" fill="#FE9900"/>
                    </mask>
                    <g mask="url(#mask0_2822_13875)">
                        <rect x="-66.7744" y="160" width="383.238" height="160" fill="#FE9900" id="thumb-up" />
                    </g>
                    <g filter="url(#filter1_d_2822_13875)"> <!-- thumb's border -->
                        <path d="M140.893 170.121C92.5519 170.121 92.1767 168.116 86.1905 163.608C83.0151 161.219 77.0235 159.563 55.1361 158.137C53.5457 158.033 52.0378 157.424 51.0246 156.196C50.5592 155.629 39.6235 139.68 39.6235 114.139C39.6235 88.9673 47.4503 77.4918 47.7852 76.9572C48.2633 76.193 48.9278 75.5628 49.7163 75.126C50.5049 74.6891 51.3915 74.4599 52.293 74.46C70.263 74.46 95.1741 51.5221 111.923 11.4786C114.82 4.55341 115.415 0.000571559 126.056 0.00057249C132.106 0.000573018 138.888 4.33285 142.241 9.89112C149.108 21.247 145.621 45.6371 142.393 58.6713C153.363 58.5833 173.221 58.4319 182.67 58.4319C195.813 58.4319 204.51 66.2692 204.778 77.729C204.864 81.5107 204.369 86.3429 203.273 88.8375C206.164 91.723 209.898 96.0767 210.06 101.68C210.265 108.911 205.526 114.419 202.505 117.581C203.196 119.685 204.584 122.475 204.451 125.81C204.116 134.011 197.787 139.298 193.84 142.056C194.165 144.293 194.42 148.529 193.702 151.8C190.856 164.869 171.695 170.121 140.893 170.121ZM57.96 147.348C77.023 148.925 87.2134 151.061 92.5881 155.108C96.7475 158.239 96.1039 159.48 140.892 159.48C154.551 159.48 181.265 158.911 183.313 149.537C184.129 145.787 180.175 142.098 180.159 142.072C179.079 139.404 180.284 136.279 182.917 135.128C182.959 135.107 193.576 131.301 193.82 125.365C193.977 121.57 191.374 119.291 191.313 119.198C189.765 116.794 190.331 113.493 192.68 111.858C192.706 111.836 199.589 107.666 199.419 101.978C199.288 97.4646 193.339 94.2839 193.158 94.1776C191.829 93.417 190.861 92.1005 190.56 90.5926C190.26 89.0925 190.597 87.5129 191.528 86.2976C191.528 86.2976 194.233 81.9972 194.137 77.9681C193.94 69.3355 184.659 69.0643 182.667 69.0643C169.25 69.0643 134.792 69.2052 134.792 69.2052C132.933 69.2105 131.324 68.3329 130.326 66.8463C129.329 65.3596 129.252 63.5111 129.84 61.8224C135.034 46.8576 137.935 23.3799 133.116 15.359C131.699 12.9947 131.372 10.6331 126.053 10.6331C125.316 10.6331 123.561 11.2155 121.731 15.5772C103.947 58.0966 77.4006 82.6966 55.6567 84.9143C53.7818 89.1907 50.2553 98.052 50.2553 114.134C50.2607 130.545 55.4599 143.239 57.96 147.348Z" fill="#FEF5E8"/>
                    </g>
                    <defs>
                        <filter id="filter0_d_2822_13875" x="0.664658" y="0.000976562" width="248.36" height="248.037" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dy="38.9584"/>
                        <feGaussianBlur stdDeviation="19.4792"/>
                        <feComposite in2="hardAlpha" operator="out"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2822_13875"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2822_13875" result="shape"/>
                        </filter>
                        <filter id="filter1_d_2822_13875" x="35.623" y="0.000976562" width="178.443" height="178.12" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dy="4"/>
                        <feGaussianBlur stdDeviation="2"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2822_13875"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2822_13875" result="shape"/>
                        </filter>
                    </defs>
                </svg>

                <svg class="thumb-svg" width="250" height="235" viewBox="0 0 250 235" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M109.107 6.04734e-06C157.448 1.02734e-05 157.823 2.00558 163.81 6.51353C166.985 8.90172 172.977 10.5584 194.864 11.984C196.454 12.0878 197.962 12.6968 198.975 13.9254C199.441 14.4919 210.377 30.4407 210.377 55.982C210.377 81.1538 202.55 92.6293 202.215 93.1639C201.737 93.9282 201.072 94.5583 200.284 94.9951C199.495 95.432 198.608 95.6612 197.707 95.6611C179.737 95.6611 154.826 118.599 138.077 158.642C135.18 165.568 134.585 170.121 123.944 170.121C117.894 170.121 111.112 165.788 107.759 160.23C100.892 148.874 104.379 124.484 107.607 111.45C96.6369 111.538 76.7787 111.689 67.3298 111.689C54.1867 111.689 45.4901 103.852 45.2215 92.3921C45.1364 88.6104 45.6311 83.7782 46.7268 81.2836C43.836 78.3981 40.1021 74.0444 39.94 68.4409C39.7351 61.2098 44.4742 55.7021 47.4954 52.54C46.8039 50.4363 45.4157 47.6466 45.5487 44.3114C45.8836 36.1098 52.2131 30.8228 56.1599 28.065C55.8353 25.8283 55.58 21.5918 56.2982 18.3207C59.1438 5.2521 78.3052 3.35456e-06 109.107 6.04734e-06ZM192.04 22.7733C172.977 21.1962 162.787 19.0604 157.412 15.0127C153.252 11.8825 153.896 10.6407 109.108 10.6407C95.4491 10.6407 68.7348 11.2098 66.687 20.5844C65.8706 24.3342 69.8252 28.0229 69.8412 28.0495C70.9209 30.7169 69.7162 33.8418 67.0833 34.9933C67.0408 35.0145 56.4242 38.8203 56.1795 44.7562C56.0226 48.5513 58.6262 50.8304 58.6874 50.9234C60.2352 53.3275 59.6686 56.6279 57.3203 58.2635C57.2937 58.2847 50.411 62.4548 50.5814 68.1434C50.7117 72.6565 56.6609 75.8372 56.8417 75.9436C58.1715 76.7042 59.1395 78.0206 59.4399 79.5285C59.7404 81.0286 59.4026 82.6082 58.4718 83.8235C58.4718 83.8235 55.7671 88.1239 55.8629 92.153C56.0597 100.786 65.341 101.057 67.3331 101.057C80.7501 101.057 115.208 100.916 115.208 100.916C117.067 100.911 118.676 101.788 119.674 103.275C120.671 104.761 120.748 106.61 120.16 108.299C114.966 123.264 112.065 146.741 116.884 154.762C118.301 157.126 118.628 159.488 123.947 159.488C124.684 159.488 126.439 158.906 128.269 154.544C146.053 112.024 172.599 87.4245 194.343 85.2068C196.218 80.9304 199.745 72.0691 199.745 55.9873C199.739 39.5758 194.54 26.8822 192.04 22.7733Z" fill="#FEF5E8"/>
                    <path d="M192.04 22.7733C172.977 21.1962 162.787 19.0604 157.412 15.0127C153.252 11.8825 153.896 10.6407 109.108 10.6407C95.4491 10.6407 68.7348 11.2098 66.687 20.5844C65.8706 24.3342 69.8252 28.0229 69.8412 28.0495C70.9209 30.7169 69.7162 33.8418 67.0833 34.9933C67.0408 35.0145 56.4242 38.8203 56.1795 44.7562C56.0226 48.5513 58.6262 50.8304 58.6874 50.9234C60.2352 53.3275 59.6686 56.6279 57.3203 58.2635C57.2937 58.2847 50.411 62.4548 50.5814 68.1434C50.7117 72.6565 56.6609 75.8372 56.8417 75.9436C58.1715 76.7042 59.1395 78.0206 59.4399 79.5285C59.7404 81.0286 59.4026 82.6082 58.4718 83.8235C58.4718 83.8235 55.7671 88.1239 55.8629 92.153C56.0597 100.786 65.341 101.057 67.3331 101.057C80.7501 101.057 115.208 100.916 115.208 100.916C117.067 100.911 118.676 101.788 119.674 103.275C120.671 104.761 120.748 106.61 120.16 108.299C114.966 123.264 112.065 146.741 116.884 154.762C118.301 157.126 118.628 159.488 123.947 159.488C124.684 159.488 126.439 158.906 128.269 154.544C146.053 112.024 172.599 87.4245 194.343 85.2068C196.218 80.9304 199.745 72.0691 199.745 55.9873C199.739 39.5758 194.54 26.8822 192.04 22.7733Z" fill="#FEF5E8"/>
                    <mask id="mask0_2822_13882" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="39" y="0" width="172" height="171">
                        <path d="M109.107 0.00098261C157.448 0.000986836 157.823 2.00655 163.81 6.5145C166.985 8.9027 172.977 10.5594 194.864 11.985C196.454 12.0887 197.962 12.6978 198.975 13.9264C199.441 14.4929 210.377 30.4417 210.377 55.983C210.377 81.1548 202.55 92.6303 202.215 93.1649C201.737 93.9291 201.072 94.5593 200.284 94.9961C199.495 95.433 198.608 95.6621 197.707 95.6621C179.737 95.6621 154.826 118.6 138.077 158.643C135.18 165.569 134.585 170.122 123.944 170.122C117.894 170.122 111.112 165.789 107.759 160.231C100.892 148.875 104.379 124.485 107.607 111.451C96.6369 111.539 76.7787 111.69 67.3298 111.69C54.1867 111.69 45.4901 103.853 45.2215 92.3931C45.1364 88.6114 45.6311 83.7791 46.7268 81.2846C43.836 78.399 40.1021 74.0453 39.94 68.4419C39.7351 61.2108 44.4742 55.703 47.4954 52.5409C46.8039 50.4373 45.4157 47.6475 45.5487 44.3124C45.8836 36.1108 52.2131 30.8238 56.1599 28.0659C55.8353 25.8293 55.58 21.5928 56.2982 18.3217C59.1438 5.25308 78.3052 0.000979917 109.107 0.00098261ZM192.04 22.7743C172.977 21.1972 162.787 19.0613 157.412 15.0136C153.252 11.8835 153.896 10.6417 109.108 10.6417C95.4491 10.6417 68.7348 11.2108 66.687 20.5854C65.8706 24.3352 69.8252 28.0239 69.8412 28.0505C70.9209 30.7179 69.7162 33.8428 67.0833 34.9943C67.0408 35.0154 56.4242 38.8213 56.1795 44.7572C56.0226 48.5523 58.6262 50.8314 58.6874 50.9243C60.2352 53.3285 59.6686 56.6289 57.3203 58.2644C57.2937 58.2857 50.411 62.4558 50.5814 68.1443C50.7117 72.6574 56.6609 75.8382 56.8417 75.9445C58.1715 76.7051 59.1395 78.0216 59.4399 79.5295C59.7404 81.0296 59.4026 82.6091 58.4718 83.8245C58.4718 83.8245 55.7671 88.1249 55.8629 92.1539C56.0597 100.787 65.341 101.058 67.3331 101.058C80.7501 101.058 115.208 100.917 115.208 100.917C117.067 100.912 118.676 101.789 119.674 103.276C120.671 104.762 120.748 106.611 120.16 108.3C114.966 123.264 112.065 146.742 116.884 154.763C118.301 157.127 118.628 159.489 123.947 159.489C124.684 159.489 126.439 158.907 128.269 154.545C146.053 112.025 172.599 87.4254 194.343 85.2078C196.218 80.9314 199.745 72.07 199.745 55.9883C199.739 39.5768 194.54 26.8831 192.04 22.7743Z" fill="#000033"/>
                        <path d="M192.04 22.7743C172.977 21.1972 162.787 19.0613 157.412 15.0136C153.252 11.8835 153.896 10.6417 109.108 10.6417C95.4491 10.6417 68.7348 11.2108 66.687 20.5854C65.8706 24.3352 69.8252 28.0239 69.8412 28.0505C70.9209 30.7179 69.7162 33.8428 67.0833 34.9943C67.0408 35.0154 56.4242 38.8213 56.1795 44.7572C56.0226 48.5523 58.6262 50.8314 58.6874 50.9243C60.2352 53.3285 59.6686 56.6289 57.3203 58.2644C57.2937 58.2857 50.411 62.4558 50.5814 68.1443C50.7117 72.6574 56.6609 75.8382 56.8417 75.9445C58.1715 76.7051 59.1395 78.0216 59.4399 79.5295C59.7404 81.0296 59.4026 82.6091 58.4718 83.8245C58.4718 83.8245 55.7671 88.1249 55.8629 92.1539C56.0597 100.787 65.341 101.058 67.3331 101.058C80.7501 101.058 115.208 100.917 115.208 100.917C117.067 100.912 118.676 101.789 119.674 103.276C120.671 104.762 120.748 106.611 120.16 108.3C114.966 123.264 112.065 146.742 116.884 154.763C118.301 157.127 118.628 159.489 123.947 159.489C124.684 159.489 126.439 158.907 128.269 154.545C146.053 112.025 172.599 87.4254 194.343 85.2078C196.218 80.9314 199.745 72.07 199.745 55.9883C199.739 39.5768 194.54 26.8831 192.04 22.7743Z" fill="#FE9900"/>
                    </mask>
                    <g mask="url(#mask0_2822_13882)">
                        <rect x="-66.7744" y="160" width="383.238" height="160" fill="#FE9900" id="thumb-down" />
                    </g>
                    <g filter="url(#filter0_d_2822_13882)">
                        <path d="M109.107 -0.000970515C157.448 -0.000966289 157.823 2.0046 163.81 6.51255C166.985 8.90075 172.977 10.5574 194.864 11.9831C196.454 12.0868 197.962 12.6958 198.975 13.9245C199.441 14.4909 210.377 30.4397 210.377 55.981C210.377 81.1528 202.55 92.6284 202.215 93.1629C201.737 93.9272 201.072 94.5573 200.284 94.9942C199.495 95.431 198.608 95.6602 197.707 95.6602C179.737 95.6602 154.826 118.598 138.077 158.641C135.18 165.567 134.585 170.12 123.944 170.12C117.894 170.12 111.112 165.787 107.759 160.229C100.892 148.873 104.379 124.483 107.607 111.449C96.6369 111.537 76.7787 111.688 67.3298 111.688C54.1867 111.688 45.4901 103.851 45.2215 92.3912C45.1364 88.6094 45.6311 83.7772 46.7268 81.2826C43.836 78.3971 40.1021 74.0434 39.94 68.4399C39.7351 61.2088 44.4742 55.7011 47.4954 52.539C46.8039 50.4354 45.4157 47.6456 45.5487 44.3104C45.8836 36.1088 52.2131 30.8218 56.1599 28.064C55.8353 25.8274 55.58 21.5908 56.2982 18.3197C59.1438 5.25113 78.3052 -0.000973208 109.107 -0.000970515ZM192.04 22.7723C172.977 21.1953 162.787 19.0594 157.412 15.0117C153.252 11.8815 153.896 10.6397 109.108 10.6397C95.4491 10.6397 68.7348 11.2088 66.687 20.5834C65.8706 24.3333 69.8252 28.0219 69.8412 28.0485C70.9209 30.716 69.7162 33.8408 67.0833 34.9924C67.0408 35.0135 56.4242 38.8193 56.1795 44.7552C56.0226 48.5503 58.6262 50.8295 58.6874 50.9224C60.2352 53.3265 59.6686 56.6269 57.3203 58.2625C57.2937 58.2838 50.411 62.4538 50.5814 68.1424C50.7117 72.6555 56.6609 75.8362 56.8417 75.9426C58.1715 76.7032 59.1395 78.0196 59.4398 79.5275C59.7404 81.0276 59.4026 82.6072 58.4718 83.8225C58.4718 83.8225 55.7671 88.1229 55.8629 92.152C56.0597 100.785 65.341 101.056 67.3331 101.056C80.7501 101.056 115.208 100.915 115.208 100.915C117.067 100.91 118.676 101.787 119.674 103.274C120.671 104.76 120.748 106.609 120.16 108.298C114.966 123.263 112.065 146.74 116.884 154.761C118.301 157.125 118.628 159.487 123.947 159.487C124.684 159.487 126.439 158.905 128.269 154.543C146.053 112.023 172.599 87.4235 194.343 85.2058C196.218 80.9294 199.745 72.0681 199.745 55.9863C199.739 39.5748 194.54 26.8812 192.04 22.7723Z" fill="#FEF5E8"/>
                    </g>
                    <defs>
                        <filter id="filter0_d_2822_13882" x="35.9336" y="-0.000976562" width="178.443" height="178.12" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dy="4"/>
                        <feGaussianBlur stdDeviation="2"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2822_13882"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_2822_13882" result="shape"/>
                        </filter>
                    </defs>
                </svg>

            </div>

        @endif


        <canvas id="custom" class="hidden"></canvas>


        <br /><br />


        <div class="pull-right">
            <button class="button primary" id="download">
                Download
                <img class="arrow" src="{{asset('assets/img/download.svg')}}" alt="Download" title="Download" />
            </button>
            <p id="download-message" class="hidden">
                Please wait while producing your report.
            </p>
            <button class="button primary" id="to-top">
                Top
                <img class="arrow" src="{{asset('assets/img/arrow-up.svg')}}" alt="Top" title="Top" />
            </button>
        </div>
        <div class="clearfix"></div>

    </div>




    <script>
        const isSURFA = <?=$isSuperUserRequestingForAnotherUser?'1':'0'?>;
    </script>
    <script src="{{ asset('assets/js/chartjs.js') }}"></script>
    <script src="{{ asset('assets/js/chartjs-adapter-date-fns.bundle.min.js') }}"></script>
    <script>
        var dataByCluster = JSON.parse('<?=json_encode($dataByCluster)?>');
        var clusters = Object.values(JSON.parse('<?=json_encode($clusters)?>'));
        const ok = <?=$okTests?>;
        const ko = <?=$koTests?>;
        const countPerDay = JSON.parse('<?=json_encode($countPerDay)?>');
        const liked = <?=$likedLUs?>;
        const disliked = <?=$dislikedLUs?>;
    </script>
    <script src="{{ asset('assets/js/report.js') }}"></script>
<?php } ?>
@stop


@section('css')
@stop