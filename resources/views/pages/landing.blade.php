<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Clikc</title>
    <meta charset="utf-8" />
    <meta
      name="description"
      content="CLIKC is a project that aims to test an innovative methodology to train unemployed people on transversal skills in an effective way."
    />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="{{asset('assets/js/landing-script.js')}}"></script>
    <link
      href="https://fonts.googleapis.com/css?family=Lexend Deca"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="{{asset('assets/css/app.css')}}"
      type="text/css"
      media="screen"
    />
    <link
      rel="stylesheet"
      href="{{asset('assets/css/landing-style.css')}}"
      type="text/css"
      media="screen"
    />
  </head>
  <body>

    <header class="row"> <!-- on update, update views/includes/header.blade -->
      <a href="/dashboard" title="Home" id="header-logo">
          <img src="<?=asset('assets/img/logo-clikc.png')?>" alt="Logo Clikc" title="Logo Clikc" />
      </a>
    </header>

    <div class="mainDiv">
      <img
        src="{{asset('assets/img/landing-page/logo-clikc-transparent.png')}}"
        class="titleImg"
        title='CLIKC "Content and Language Integrated learning for Key Competences"'
        alt='CLIKC "Content and Language Integrated learning for Key Competences"'
      />
      <div class="pageContent">
        <p class="welcomeP">
          <span>
            <b>Welcome to CLIKC!</b>
            <br />
            <br />
            CLIKC "Content and Language Integrated learning for Key Competences"
          </span>
          is a project that aims to test an innovative methodology to train
          unemployed people (without any special needs) on transversal skills in
          an effective way, in order to reduce the period of unemployment and
          allow the active labour market policies to focus on most fragile or
          vulnerable groups.
        </p>


        <div id="central-buttons">

          <button class="readBtn button secondary" onclick="readMoreOrLess();">
            <span>Read More</span>
            <img src="{{asset('assets/img/landing-page/Vector-down.png')}}" class="arrow" alt="arrow" />
          </button>

          <a href="/login" class="button primary">
            Log in
            <img src="{{asset('assets/img/login.svg')}}" class="arrow" alt="Login" title="Login" />
          <a>

        </div>

        <div class="objectivesP" hidden>
          <span>Specific objectives</span>
          of the project are the realization and experimentation of an
          innovative didactic catalogue and of a new distance learning tool. In
          particular:
          <div class="ulGrid">
            <div class="ulGridElem">
              <img src="{{asset('assets/img/landing-page/Ellipse.png')}}" alt="&bull;" />
              <p>
                the catalog will focus on 4 specific competences relating to the
                area of basic personal and relational skills.
              </p>
            </div>
            <div class="ulGridElem">
              <img src="{{asset('assets/img/landing-page/Ellipse.png')}}" alt="&bull;" />
              <p>
                contents will be created through integration with foreign
                language training through the "Content-Language Integrated
                Learning" (CLIL) system and will be available in Italian,
                English, Spanish and German.
              </p>
            </div>
            <div class="ulGridElem">
              <img src="{{asset('assets/img/landing-page/Ellipse.png')}}" alt="&bull;" />
              <p>
                contents will be available in 15-minute pills (micro-learnings)
                usable through devices such as PCs or smartphones.
              </p>
            </div>
            <div class="ulGridElem">
              <img src="{{asset('assets/img/landing-page/Ellipse.png')}}" alt="&bull;" />
              <p>
                the courses will be made available through a web application
                (asynchronous e-learning) that will use two technologies
                (adaptive learning and advanced analytics) allowing the user to
                enjoy content suitable for him / her and updated on the basis of
                his/her learning results.
              </p>
            </div>
          </div>
        </div>
        <p class="infoP" hidden>
          The project has a duration of 24 months, from June 2021 until May 2023
          and has been finances within the the framework of the Erasmus Plus
          programme. The lead partner is Veneto Lavoro. The partnership
          comprehends organizations from Italy (Tecum srl, Politecnico di Torino
          and Quantitas srl), Spain (Acción Laboral), Austria (Bit
          Schulungscenter GmbH), Malta (ETI Executive Training Institute), and
          an European network based in Belgium (EVTA - Association Européen pour
          la formation professionnelle).
          <br />
          <br />
          For further information go to the website:
          <a
            href="https://erasmus-plus.ec.europa.eu/projects/search/details/2020-1-IT01-KA226-VET-009021"
            >Erasmus Plus CLIKC.</a
          >
        </p>
        <div style="display:table;width:100%;">
          <p id="eu-disclaimer">
            The content of this website does not reflect the official opinion of the European Union. 
            Responsibility for the information and views expressed therein lies entirely with the author(s).
          </p>
          <img
            id="eu-logo"
            src="{{asset('assets/img/landing-page/image_2022_05_26T16_05_34_026Z.png')}}"
            class="supportImg"
            title="Erasmus Plus EU programme for education, training, youth and sport"
            alt="Erasmus Plus EU programme for education, training, youth and sport"
          />
        </div>
        <div class="logos">
          <a class="logoHref" href="https://www.accionlaboral.com/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/AL_LOGO.png')}}"
              title="Acción Laboral"
              alt="Acción Laboral"
            />
          </a>
          <a class="logoHref" href="https://www.bitschulungscenter.at/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/_Logo_bitsc_COLOR_large_BGwhite.png')}}"
              title="bit schulungscenter"
              alt="bit schulungscenter"
            />
          </a>
          <a class="logoHref" href="https://etimalta.com/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/ETI_logo_cmyk.png')}}"
              title="ETI Malta"
              alt="ETI Malta"
            />
          </a>
          <a class="logoHref" href="https://www.evta.eu/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/logo-evta.png')}}"
              title="EVTA"
              alt="EVTA"
            />
          </a>
          <a class="logoHref" href="https://nexa.polito.it/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/logo_nexa.png')}}"
              title="Nexa"
              alt="Nexa"
            />
          </a>
          <a class="logoHref" href="https://www.quantitas.it/it/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/logopervegaincube.png')}}"
              title="Quantitas"
              alt="Quantitas"
            />
          </a>
          <a class="logoHref" href="https://www.ergongroup.it/tecum-srl-struttura-societaria/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/TECUM 1.png')}}"
              title="Tecum"
              alt="Tecum"
            />
          </a>
          <a class="logoHref" href="https://www.venetolavoro.it/" target="_blank" rel="noopener">
            <img
              class="logo"
              src="{{asset('assets/img/landing-page/Logo_VenetoLavoro_def.png')}}"
              title="Veneto Lavoro"
              alt="Veneto Lavoro"
            />
          </a>
        </div>
      </div>
    </div>
  </body>
</html>
