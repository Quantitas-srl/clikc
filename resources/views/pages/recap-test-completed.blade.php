<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Recap test completed
@stop


@section('content')



<!-- image "assets/img/trophy.svg" -->
<svg id="trophy" class="hidden" width="488" height="534" viewBox="0 0 488 534" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_ddd_1842_6538)">
<path d="M259.156 381.74C259.947 391.464 260.706 400.8 260.801 407.031C268.492 410.228 286.716 418.657 296.859 428.16C278.361 429.992 192.453 427.906 182.535 426.266C195.242 418.741 209.741 411.88 223.731 407.177C223.502 395.943 224.2 385.355 224.94 374.124C225.525 365.249 226.136 355.972 226.337 345.656C224.938 345.396 222.627 344.803 220.339 344.218C218.199 343.668 216.08 343.126 214.748 342.866C198.135 339.63 186.03 330.563 177.283 318.789C165.385 323.941 150.53 317.755 140.755 311.139C120.237 297.25 92.9339 269.401 106.329 242.632C114.323 226.656 140.624 221.615 156.754 220.907C157.649 213.464 159.666 206.222 164.357 201.393C172.862 192.636 187.49 188.964 199.188 187.295C225.196 183.585 251.809 183.244 277.626 189.168C278.251 189.31 278.913 189.459 279.606 189.615C291.217 192.228 312.011 196.907 324.754 206.862C327.653 209.33 329.974 212.162 331.459 215.432C332.785 218.357 333.554 223.991 333.581 231.334C335.668 231.894 337.932 232.449 340.307 233.032C355.833 236.841 376.08 241.808 382.466 256.742C388.529 270.922 380.842 284.359 372.416 295.536C356.667 316.432 327.045 343.257 298.079 329.013C292.977 333.745 287.232 337.631 280.785 340.36C274.328 343.091 266.028 345.32 257.36 346.417C256.916 354.186 258.067 368.334 259.156 381.74ZM128.122 240.736C105.555 248.512 120.92 281.011 133.773 291.03C137.681 294.075 141.837 296.832 146.064 299.421C151.011 302.452 162.093 306.518 168.113 304.269C165.221 297.807 163.02 290.649 160.952 283.539C156.665 268.804 154.722 252.464 154.115 237.106C145.352 236.725 136.449 237.867 128.122 240.736ZM367.557 273.377C357.935 296.73 335.422 319.181 307.835 316.508C319.932 295.542 331.971 270.375 334.801 246.258C335.538 246.413 336.281 246.557 337.027 246.701C338.799 247.044 340.581 247.387 342.298 247.901C353.372 251.22 374.026 257.677 367.557 273.377Z" fill="#FEF5E8" stroke="#FE9900" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M279.508 308.289C252.778 303.342 226.1 302.486 204.568 305.376C195.619 306.577 186.217 307.26 177.83 310.911" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M305.395 292.271C297.742 289.951 290.203 287.597 282.332 286.074C251.217 280.056 220.451 279.463 189.296 284.298" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-1" d="M149.429 130.541C148.525 138.228 147.426 146.206 147.271 153.929" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-1" d="M126.606 158.995C131.052 158.694 135.517 158.759 139.968 158.907" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-1" d="M154.465 158.602C159.552 159.453 164.468 159.223 169.585 159.304" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-1" d="M146.645 183.352C145.752 178.451 144.852 171.485 146.15 166.519" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-2" d="M318.433 352.528C317.657 356.63 317.302 360.795 316.914 364.948" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-2" d="M303.506 370.258C305.576 370.036 307.636 369.814 309.714 369.668" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-2" d="M316.144 375.399C315.904 379.862 316.033 384.317 315.89 388.779" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-2" d="M321.451 371.405C324.641 371.211 327.847 370.976 331.041 370.951" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-3" d="M319.524 154.766C318.011 142.498 317.749 133.144 305.728 126.751C316.226 123.955 323.459 110.767 325.167 100.714C327.046 110.307 328.58 121.511 336.339 128.421C326.504 132.37 320.695 143.561 319.526 153.494" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="trophy-blink trophy-blink-4" d="M176.199 378.816C175.637 373.756 174.302 367.787 168.805 366.051C173.78 364.062 176.303 358.689 176.929 353.667C177.07 357.937 178.118 363.648 182.639 365.367C177.578 367.383 177.366 374.149 176.199 378.816Z" stroke="#0000CC" stroke-width="4.84965" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
<defs>
<filter id="filter0_ddd_1842_6538" x="-1" y="0" width="490" height="534.615" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="2"/>
<feGaussianBlur stdDeviation="2.5"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1842_6538"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="-0.5"/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="effect1_dropShadow_1842_6538" result="effect2_dropShadow_1842_6538"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="2.5"/>
<feGaussianBlur stdDeviation="50"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.996078 0 0 0 0 0.6 0 0 0 0 0 0 0 0 0.15 0"/>
<feBlend mode="normal" in2="effect2_dropShadow_1842_6538" result="effect3_dropShadow_1842_6538"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect3_dropShadow_1842_6538" result="shape"/>
</filter>
</defs>
</svg>

<!-- image "assets/img/exclamation.svg" -->
<svg id="exclamation" class="hidden" width="396" height="474" viewBox="0 0 396 474" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_ddd_2149_3226)">
    <g id="exclamation-1">
        <path class="exclamation-1" fill-rule="evenodd" clip-rule="evenodd" d="M165.875 273.409C151.289 276.221 137.224 278.771 122.907 283.137C114.121 225.451 107.697 167.441 104.75 109.165C125.556 102.866 146.638 101.38 168.272 102.595C165.229 159.456 168.195 216.475 166.021 273.384C165.974 273.391 165.926 273.401 165.875 273.409Z" fill="#FEF5E8" stroke="#FE9900" stroke-width="8.53918" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <path class="exclamation-1" fill-rule="evenodd" clip-rule="evenodd" d="M154.249 314.683C125.924 307.336 108.642 346.09 133.099 362.49C154.732 376.995 182.761 348.099 168.71 326.729" fill="white"/>
        <path class="exclamation-1" d="M154.249 314.683C125.924 307.336 108.642 346.09 133.099 362.49C154.732 376.995 182.761 348.099 168.71 326.729" fill="#FEF5E8"/>
        <path class="exclamation-1" d="M154.249 314.683C125.924 307.336 108.642 346.09 133.099 362.49C154.732 376.995 182.761 348.099 168.71 326.729" stroke="#FE9900" stroke-width="8.53918" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
    <g id="exclamation-2">
        <path class="exclamation-2" d="M291.383 143.2C284.38 189.33 278.832 235.974 269.279 281.681L222.708 274.253C222.707 232.148 222.584 181.701 225.819 135.713C247.613 135.552 270.304 137.327 291.383 143.2Z" fill="#FEF5E8" stroke="#0000CC" stroke-width="8.53918" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
        <path class="exclamation-2" fill-rule="evenodd" clip-rule="evenodd" d="M250.057 318.768C226.396 312.074 210.125 347.435 231.116 361.207C253.245 375.727 275.808 342.837 259.943 324.463" fill="white"/>
        <path class="exclamation-2" d="M250.057 318.768C226.396 312.074 210.125 347.435 231.116 361.207C253.245 375.727 275.808 342.837 259.943 324.463" fill="#FEF5E8"/>
        <path class="exclamation-2" d="M250.057 318.768C226.396 312.074 210.125 347.435 231.116 361.207C253.245 375.727 275.808 342.837 259.943 324.463" stroke="#0000CC" stroke-width="8.53918" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    </g>
</g>
<defs>
<filter id="filter0_ddd_2149_3226" x="-47" y="-30" width="490" height="534.615" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="2"/>
<feGaussianBlur stdDeviation="2.5"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2149_3226"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="-0.5"/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="effect1_dropShadow_2149_3226" result="effect2_dropShadow_2149_3226"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="2.5"/>
<feGaussianBlur stdDeviation="50"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.996078 0 0 0 0 0.6 0 0 0 0 0 0 0 0 0.15 0"/>
<feBlend mode="normal" in2="effect2_dropShadow_2149_3226" result="effect3_dropShadow_2149_3226"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect3_dropShadow_2149_3226" result="shape"/>
</filter>
</defs>
</svg>

<!-- image "assets/img/getting-there.svg"  -->
<svg id="getting-there" class="hidden" width="490" height="535" viewBox="0 0 490 535" fill="none" xmlns="http://www.w3.org/2000/svg">
<g clip-path="url(#clip0_2406_2123)" filter="url(#filter0_ddd_2406_2123)">
<path class="unstroked" pathLength="1" fill-rule="evenodd" clip-rule="evenodd" d="M141.828 429.794C141.862 430.906 122.713 399.289 102.428 368.887C102.628 356.698 104.516 326.767 122.994 307.349C139.195 290.325 155.524 279.638 177.448 277.985C204.729 275.928 228.223 284.493 252.475 264.963C277.022 245.197 259.879 211.808 267.535 185.253C276.14 155.4 298.509 146.008 319.873 131.174C317.876 122.487 314.899 114.12 312.379 105.624C337.466 102.196 363.276 101.331 388.538 99.39C379.313 128.525 366.631 155.205 355.769 183.467C349.822 179.625 346.196 173.683 342.131 167.607C294.314 182.04 317.383 255.399 296.698 292.048C269.521 340.197 195.47 314.39 167.242 351.989C154.377 369.124 140.99 403.379 141.828 429.794Z" fill="#FEF5E8" stroke="#FE9900" stroke-width="4" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M359.752 101.379C358.819 125.522 353.681 155.952 351.502 179.986" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M334.213 103.242C335.374 125.34 332.811 149.882 332.586 171.723" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M311.191 136.96C312.268 157.113 314.394 181.414 312.701 201.631" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M299.473 286.291C299.148 266.486 295.778 244.091 294.484 224.36C292.882 199.881 293.098 175.949 290.156 151.558" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M265.521 209.508C268.436 242.549 276.406 277.556 279.654 310.558" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M245.02 270.163C245.711 286.116 249.002 306.303 250.777 322.197" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M217.627 277.969C217.904 294.978 220.723 310.802 221.371 327.769" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M190.516 277.672C190.566 295.038 191.443 316.899 193.64 334.153" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M163.611 280.407C163.121 301.996 167.427 329.511 167.865 351.142" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M136.521 294.845C137.469 326.378 145.802 361.657 147.517 393.062" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path class="getting-there-line hidden" d="M115.791 316.703C117.422 337.192 121.16 359.415 124.083 379.71C125.262 387.9 127.596 400.227 128.12 408.52" stroke="#0000CC" stroke-miterlimit="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
<defs>
<filter id="filter0_ddd_2406_2123" x="0" y="0" width="490" height="534.615" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
<feFlood flood-opacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="2"/>
<feGaussianBlur stdDeviation="2.5"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_2406_2123"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dx="1" dy="-0.5"/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="effect1_dropShadow_2406_2123" result="effect2_dropShadow_2406_2123"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="2.5"/>
<feGaussianBlur stdDeviation="50"/>
<feColorMatrix type="matrix" values="0 0 0 0 0.996078 0 0 0 0 0.6 0 0 0 0 0 0 0 0 0.15 0"/>
<feBlend mode="normal" in2="effect2_dropShadow_2406_2123" result="effect3_dropShadow_2406_2123"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect3_dropShadow_2406_2123" result="shape"/>
</filter>
<clipPath id="clip0_2406_2123">
<rect width="290" height="334.615" fill="white" transform="translate(100 97.5)"/>
</clipPath>
</defs>
</svg>



<div id="congratulations-div">
    <div class="title-container">
        <h1 class="title">{{ $recapTitle }}</h1>
    </div>
    <p class="page-info">
        {{ $recapSubtitle }}
    </p>

    <p id="score-info">
        Your score is <span id="score-value" data-score="{{ $score }}">0</span> out of 100 in the recap test!
    </p>
    <p id="know-more">
        {{ $recapSubtitle2 }}
    </p>

    <div id="clusters">
        <?php foreach ($barsData as $cluster=>$barDatum) {
            ?>
            <div>
                <label>{{ $cluster }}</label>
                <progress data-value="{{ $barDatum*10 }}" max="100">
                </progress>
            </div>
            <?php
        } ?>
    </div>

    <div id="what-to-do-next-container">
        <p>What would you like to do next?</p>
        <a href="/dashboard" title="Dashboard" class="button secondary">
            <img class="arrow arrow-back" src="{{asset('assets/img/arrow-double-left.svg')}}" alt="Arrow" title="Registry" />
            Review previous Learning Units
        </a>
        <a href="/ia/next" title="Continue" class="button primary">
            Keep learning
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Continue" />
        </a>
    </div>

</div>


<script src="{{ asset('assets/js/recap-test-completed.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/css/recap-test-completed.css') }}" />











    













<script>
/**
* FROM HERE ON
* 
* JUST A TEST FOR ANIMATION
* 
* NOT USED
* 
* KEPT JUST IN CASE OF MORE TESTS
*
*/
</script>


<!-- just testing -->
<div id="cards-parent-container">
    <div id="cards-container">
    </div>
</div>

<script>

    //initCards();


    /**
     * Returns the HTML of a card
     * @param {number} screenWidth the screen width
     * @param {number} color the 
     */
    /*function getCardHTML(extent, color, i, max_per_max) {
        var left =  extent.width*2 / (max_per_max) * (Math.random() * i*i);
        var top = extent.height*3 / (max_per_max) * (Math.random() * i*i);
        return  '<div class="card-container" style="left:' + left + 'px;top:' + top + 'px;">' + 
                    '<div class="card" style="background:' + color + ';">' + 
                    '</div>' + 
                '</div>';
    }*/



/*
    function initCards() {
        const N_CARDS = 1000;
        const N_CARDS_per_N_CARDS = N_CARDS*N_CARDS;
        var s = '';
        var width = window.innerWidth;
        var colors = ['#6951a4', '#b28988', '#ebb573', '#4b39af', 'orange'];
        var ncolors = colors.length;
        for (var i = 0; i < N_CARDS; i++) {
            var color = colors[i % ncolors];
            s += getCardHTML({width: window.innerWidth, height: window.innerHeight}, color, i, N_CARDS_per_N_CARDS);
        }
        document.getElementById('cards-container').innerHTML = s;

        setTimeout(() => {
            document.querySelector('#cards-parent-container').classList.add('running');
            document.querySelector('#cards-container').classList.add('running');
        }, 50);
    }*/
</script>



@stop