@extends('layouts.default')

@section('title')
Learning Unit not found
@stop


@section('content')

<div class="alert">
    This learning unit doesn't exist.
</div>

@stop