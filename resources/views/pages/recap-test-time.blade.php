<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Recap test
@stop


@section('content')


<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Confirm" title="Recap test time" src="{{asset('assets/img/circled-confirm.svg')}}" />
    </div>

    <div class="title-container">
        <h1 class="title">Well done! It's time for the recap test!</h1>
    </div>
    <p class="page-info">
        Every 10 completed Learning Units, the recap test is a good change to consolidate your knowledge.
    </p>
    <div class="text-center">
        <a href="/recap-test" title="Go to the recap test" class="button primary">
            Go to recap test
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
    
</div>

@stop


