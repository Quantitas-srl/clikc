@extends('layouts.default')

@section('title')
Welcome
@stop


@section('content')

<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Happy face" title="Happy face" src="<?=asset('assets/img/happy-face.svg')?>" />
    </div>

    <div class="title-container">
        <h1 class="title">Hi! Nice to meet you!</h1>
    </div>
    <p class="page-info">
        Before you start your CLIKC journey, please register your
        profile. It'll help us to improve your recommendations.
    </p>
    
    <div class="text-center">
        <a href="/startup" title="Go to the running learning unit" class="button primary">
            Profile set-up
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
</div>

<style> #header-buttons a { display: none; } </style>

@stop

