@extends('layouts.default')

@section('title')
Users
@stop


@section('content')

<div class="title-container">
    <h1 class="title">Welcome back!</h1>
</div>
<div id="welcome-subtitle">
    <div>
        {{ $personalDetails->name }} {{ $personalDetails->surname }}
    </div>
    <div class="date">
        {{ date('d/m/y', time()) }}
    </div>
    <div class="date">
        {{ date('H:i', time()) }}
    </div>
</div>

<?php

$superuserEmail = json_decode($personalDetails->other)->email;
$superuserEmailParts1 = explode('@', $superuserEmail);
$superuserEmailParts2 = explode('-', $superuserEmailParts1[0]);
$group = $superuserEmailParts2[1] ?? '';
$user = $superuserEmailParts2[2] ?? '';
$isGroupSuperuser = false;
if (intval($group) > 0 && intval($group) <= 99) {
    $isGroupSuperuser = true;
}
?>

<h2>Users / Trainees</h2>
<table id="registry-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Started</th>
            <th>Completed Learning Units</th>
            <th>Learning Units per week</th>
            <th>Correct Answers Rate</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $todayTime = time();
    foreach ($data as $id=>$datum) {
        if (isset($datum['created_at'])) {
            $email = $datum['email'];
            if (!$isGroupSuperuser || ($isGroupSuperuser && strpos($email, '-'.$group.'-') !== false)) {
                if ($datum['surname'] && $datum['name']) { // avoid users not completely registered, otherwise startup popup is shown to the superuser
                    $createdAtTime = strtotime($datum['created_at']);
                    $createdAt = date('d-m-Y', $createdAtTime).'<br />'.date('H:i', $createdAtTime);
                    $elapsedTime = $todayTime - $createdAtTime;
                    $elapsedDays = $elapsedTime / (60*60*24) + 1;
                    $nLUs = $datum['nlu'] ?? 0;
                    //$nLUsPerWeek = round(($nLUs/$elapsedDays)*7, 1);
                    $nLUsPerWeek = round($nLUs / ceil($elapsedDays/7), 1);
                    $accuracy = isset($datum['accuracy']) ? round($datum['accuracy'], 1) : '-';
                    ?>
                    <tr data-id="<?=$id?>">
                        <td><?=$datum['surname']?> <?=$datum['name']?></td>
                        <td data-order="<?=$createdAtTime?>"><?=$createdAt?></td>
                        <td><?=$nLUs?></td>
                        <td><?=$nLUsPerWeek?></td>
                        <td><?=$accuracy?><small>/100</small></td>
                    </tr>
                <?php
                }
            }
        }
    }
    ?>
    </tbody>
</table>

<link rel="stylesheet" href="{{ asset('assets/css/common-dashboard-menu.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/registry.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.1.12.1.min.css') }}" />
<script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.dataTables-1.12.1.min.js') }}"></script>

<style>
    h2 {
        color: #0000CC;
        font-family: 'Lexend Deca';
        font-size: 22.5px;
        font-style: normal;
        font-weight: 365;
        letter-spacing: 0.01em;
        line-height: 165%;
    }

    #registry-table td:nth-child(3), #registry-table td:nth-child(4), #registry-table td:nth-child(5) {
        color: #000033;
        font-size: 15px;
        font-weight: 365;
        letter-spacing: 0.01em;
        line-height: 120%;
        text-align: center;
        text-transform: capitalize;
    }
</style>


<script src="{{ asset('assets/js/users-status.js') }}"></script>



@stop