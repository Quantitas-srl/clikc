@extends('layouts.default')

@section('title')
Pending Learning Unit
@stop


@section('content')

<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Bolt" title="Bolt" src="<?=asset('assets/img/bolt.svg')?>" />
    </div>

    <div class="title-container">
        <h1 class="title">Wait! Pending Learning Unit!</h1>
    </div>
    <p class="page-info">
        You have a pending Learning Unit. Please complete that before starting a new one.
    </p>
    
    <div class="text-center">
        <a href="/learning-unit/{{$pendingId}}/show" title="Go to the pending Learning Unit" class="button primary">
            Complete Learning Unit
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
</div>

@stop