@extends('layouts.default')

@section('title')
@if ($hasStarted)
    Profile
@else
    Startup
@endif
@stop

@section('content')

<?php
$ref = $_SERVER['HTTP_REFERER'] ?? '';
if (strpos($ref, 'dashboard') !== false || strpos($ref, 'report') !== false) {
    $hasStarted = true;
}

$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = request()->request->get('isSuperUser') && filter_input(INPUT_GET, 'uid') && ($mode === 'super');
$now = time();


$isGroupSuperuser = false;
$group = '';
if ($isSuperUserRequestingForAnotherUser) {
    $superuserEmailParts1 = explode('@', $loggedUserEmail);
    $superuserEmailParts2 = explode('-', $superuserEmailParts1[0]);
    $group = $superuserEmailParts2[1] ?? '';
    $user = $superuserEmailParts2[2] ?? '';
    if (intval($group) > 0 && intval($group) <= 99) {
        $isGroupSuperuser = true;
    }
}

if ($isGroupSuperuser && strpos((json_decode($personalDetails->other ? $personalDetails->other : '{"email":""}'))->email, '-'.$group.'-') === false) {
    echo 'You cannot view this user.';
} else {

    function printSelect($isSuperUserRequestingForAnotherUser, $label, $id, $options, $value, $class = '') {
        ?>
        <label for="<?=$id?>"><?=$label?></label>
        <select id="<?=$id?>" class="form-input <?=$class?>" required data-init-value="<?=$value?>" <?=$isSuperUserRequestingForAnotherUser?'disabled':''?>>
            <option value=""></option>
            <?php
            foreach ($options as $key=>$text) {
                ?>
                <option <?=$key===$value?'selected':''?> value="<?=$key?>"><?=$text?></option>
                <?php
            }
            ?>
        </select>
        <span class="placeholder"><?=$label?></span>
        <img class="arrow" src="{{asset('assets/img/caret-down.svg')}}" alt="Arrow" title="Press to show options" />
        <?php
    }


    $otherData = array();
    if (isset($personalDetails) && isset($personalDetails->other)) {
        $otherData = (array)json_decode($personalDetails->other);
    }

    ?>

    @if ($hasStarted || $isSuperUserRequestingForAnotherUser)
        <link rel="stylesheet" href="{{ asset('assets/css/common-dashboard-menu.css') }}" />


        <div class="title-container">
            @if ($isSuperUserRequestingForAnotherUser)
                <h1 class="title">{{ $personalDetails->name }} {{ $personalDetails->surname }}</h1>
            @else
                @if ($hasStarted)
                    <h1 class="title">Welcome back!</h1>
                @else
                    <h1 class="title">Welcome!</h1>
                @endif
            @endif
        </div>
        <div id="welcome-subtitle">
            @if ($isSuperUserRequestingForAnotherUser)
                <div>
                    Last access
                </div>
                <div class="date">
                    {{ date('d/m/y', $lastAccessTime) }}
                </div>
                <div class="date">
                    {{ date('H:i', $lastAccessTime) }}
                </div>
            @else
                <div>
                    {{ $personalDetails->name }} {{ $personalDetails->surname }}
                </div>
                <div class="date">
                    {{ date('d/m/y', time()) }}
                </div>
                <div class="date">
                    {{ date('H:i', time()) }}
                </div>
            @endif
        </div>

        @include('includes.common-dashboard-menu')
    @else
        @if (!$isSuperUserRequestingForAnotherUser)
            <div id="tabs-selector">
                <div class="tab-selector disabled" tab-index="1">
                    <div class="tab-selector-number">1</div>
                    <span>Personal details</span>
                </div>
                <div class="tab-selector-separator"></div>
                <div class="tab-selector disabled" tab-index="2">
                    <div class="tab-selector-number">2</div>
                    <span>Self assessment</span>
                </div>
                <div class="tab-selector-separator"></div>
                <div class="tab-selector disabled" tab-index="3">
                    <div class="tab-selector-number">3</div>
                    <span>Learning preferences</span>
                </div>
            </div>
        @endif
    @endif







    <div id="tab-personal-details" tab-index="1" class="tab hidden">

        @if (!$hasStarted)
            <p class="page-title">Personal Details</p>
            <p class="page-info">Please fill in the following form</p>
        @else
            <br />
            <br />
        @endif


        <div id="data-block-1">
            <div class="form-group inline">
                <label for="email">Email</label>
                <input type="email" id="email" class="form-input" value="<?=$email?>" disabled />
                <span class="placeholder">Email Address</span>
            </div>
            
            <div class="form-group inline">
                <label for="name">Name</label>
                <input type="text" id="name" class="form-input" value="<?=$personalDetails->name?>" required <?=$isSuperUserRequestingForAnotherUser?'disabled':''?> />
                <span class="placeholder">Name</span>
            </div>
            
            <div class="form-group inline">
                <label for="surname">Surname</label>
                <input type="text" id="surname" class="form-input" value="<?=$personalDetails->surname?>" required <?=$isSuperUserRequestingForAnotherUser?'disabled':''?> />
                <span class="placeholder">Surname</span>
            </div>
        </div>
        <div class="form-group-separator"></div>

        
        <div id="data-block-2" class="hidden">
            <div class="form-group inline">
                <?php printSelect($isSuperUserRequestingForAnotherUser, "Age", "age", json_decode($config['age']), $otherData['age'] ?? ''); ?>
            </div>
            <div class="form-group inline">
                <?php printSelect($isSuperUserRequestingForAnotherUser, "Gender", "gender", json_decode($config['gender']), $otherData['gender'] ?? ''); ?>
            </div>
            <div class="form-group inline">
                <?php printSelect($isSuperUserRequestingForAnotherUser, "Country", "country", json_decode($config['country']), $otherData['country'] ?? ''); ?>
            </div>
        </div>

        <div class="form-group-separator"></div>
        
        <div id="data-block-3" class="hidden">
            <div class="form-group inline">
                <?php printSelect($isSuperUserRequestingForAnotherUser, "Highest Educational Attainment", "highest-educational-attainment", array(), $otherData['highest-educational-attainment'] ?? '', "long-select"); // will be JS ?>
            </div>
            
            <div class="form-group inline">
                <?php printSelect($isSuperUserRequestingForAnotherUser, "Employment Status", "employment-status", json_decode($config['employment-status']), $otherData['employment-status'] ?? '', "long-select"); ?>
            </div>

            <div class="form-group inline">
                <label for="social-security-number">Social Security Number (optional)</label>
                <input type="text" id="social-security-number" class="form-input" value="<?=$otherData['social-security-number'] ?? ''?>" <?=$isSuperUserRequestingForAnotherUser?'disabled':''?> />
                <span class="placeholder">Social Security Number</span>
            </div>
        </div>
            

        <div class="form-group centered">
            <label for="terms-and-conditions">
                <input required type="checkbox" <?=($hasStarted||$isSuperUserRequestingForAnotherUser)?'disabled':''?> id="terms-and-conditions" <?=$personalDetails->terms_and_conditions ? 'checked' : ''?> />
                I agree to register to CLIKC in compliance with the <a href="/terms-and-conditions" target="_blank" title="Read CLIKC Privacy Policy*">CLIKC Privacy Policy*</a>
            </label>
            <label for="data-profiling">
                <input required type="checkbox" <?=($hasStarted||$isSuperUserRequestingForAnotherUser)?'disabled':''?> id="data-profiling" <?=$personalDetails->data_profiling ? 'checked' : ''?> />
                I agree to data profiling as illustrated in the <a href="/terms-and-conditions" target="_blank" title="Read CLIKC Privacy Policy*">CLIKC Privacy Policy*</a>
            </label>
        </div>


        @if (!$isSuperUserRequestingForAnotherUser)
            @if ($hasStarted)
                <div class="button-container">
                    <button id="go-to-dashboard"  class="button primary" disabled>
                        Save
                    </button>
                </div>
            @else
                <div class="button-container">
                    <button id="go-to-tab-2" class="button primary next" data-goto="2" disabled>
                        Next
                        <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                    </button>
                </div>
            @endif
        @endif
        

    </div>





    @if (!$hasStarted || $isSuperUserRequestingForAnotherUser)
        <div id="tab-self-assessment" tab-index="2" class="tab hidden">
            <p class="page-title">Self Assessment</p>
            <p class="page-info">Please select your competence level with regards to the following soft skills</p>
            
            <br />

            <?php
            $n = 0;
            $cn = 0;
            foreach ($clusters as $cluster) {
                ?>

                @if ($n % 3 === 0)
                    <div class="clusters-block-accorder hidden" data-n="{{ $cn }}">
                        <?=(1+$n/3)?>/<?=count($clusters)/3?>
                        <span>&#9660;</span>
                    </div>
                    <div class="clusters-block hidden" data-n="{{ $cn++ }}">
                @endif

                <triple-choice title="{{ $cluster->description }}" data-cluster="{{ $cluster->id }}" value="{{ $cluster->skill_value }}" data-range="2,3,4"></triple-choice>

                
                @if (($n-2) % 3 === 0)
                    </div>
                @endif
                
                <?php
                $n++;
            }
            ?>

            <div class="button-container">
                <button class="button secondary inner-next hidden">
                    Next
                    <img class="arrow" src="{{asset('assets/img/arrow-down.svg')}}" alt="Arrow" title="Next" />
                </button>
            </div>
            <div class="button-container">
                <button class="button primary next hidden" data-goto="3">
                    Next
                    <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                </button>
            </div>

        </div>






        <div id="tab-learning-preferences" tab-index="3" class="tab hidden">
            <p class="page-title">Learning preferences</p>
            <p class="page-info">Please select 3 soft skills you'd like to learn the most</p>

            <div id="soft-skill-preferences-container">
                <?php
                foreach ($clusters as $cluster) {
                    $active = +$cluster->use_for_startup === 1 ? ' active ' : '';
                    ?>
                    <button class="soft-skill {{ $active }}" data-cluster="{{ $cluster->id }}">
                        {{ $cluster->description }}
                    </button>
                    <?php
                }
                ?>
            </div>

            <div class="button-container" id="dashboard-button-container">
                <button id="register" title="Register" class="button primary">
                    Register
                    <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Register" />
                </button>
            </div>
        </div>


        <div id="modal-confirm-message" class="hidden">
            <div id="confirm-message">
                <img src="{{asset('assets/img/orange-rectangle.png')}}" alt="Border" title="Registered" />
                <span>Your profile has been successfully registered</span>
                <img src="{{asset('assets/img/confirm.png')}}" alt="Confirmation image" title="Registered" />
                <p>You will be redirected in <span id="redirection-seconds">5</span> seconds...</p>
            </div>
        </div>

        @if (!$isSuperUserRequestingForAnotherUser)
            <style> #header-buttons a { display: none; } </style>
        @endif
    @else
    @endif


    <script>
        const isSURFA = <?=$isSuperUserRequestingForAnotherUser?'1':'0'?>;
    </script>

    <script src="{{ asset('assets/js/triple-choice-component.js') }}"></script>
    <script src="{{ asset('assets/js/startup.js') }}"></script>

<?php } ?>
@stop

@section('css')
<link href="{{ asset('assets/css/startup.css') }}" rel="stylesheet">
@stop