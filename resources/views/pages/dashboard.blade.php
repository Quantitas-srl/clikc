@extends('layouts.default')

@section('title')
Dashboard
@stop

@section('content')
<link rel="stylesheet" href="{{ asset('assets/css/common-dashboard-menu.css') }}" />

<?php
$uid = filter_input(INPUT_GET, 'uid');
$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = request()->request->get('isSuperUser') && $uid && ($mode === 'super');
$now = time();

$isGroupSuperuser = false;
$group = '';
if ($isSuperUserRequestingForAnotherUser) {
    $superuserEmailParts1 = explode('@', $loggedUserEmail);
    $superuserEmailParts2 = explode('-', $superuserEmailParts1[0]);
    $group = $superuserEmailParts2[1] ?? '';
    $user = $superuserEmailParts2[2] ?? '';
    if (intval($group) > 0 && intval($group) <= 99) {
        $isGroupSuperuser = true;
    }
}


if ($isGroupSuperuser && strpos((json_decode($personalDetails->other ? $personalDetails->other : '{"email":""}'))->email, '-'.$group.'-') === false) {
    echo 'You cannot view this user.';
} else {

    ?>

    <div class="title-container">
        @if ($isSuperUserRequestingForAnotherUser)
            <h1 class="title">{{ $personalDetails->name ?? '' }} {{ $personalDetails->surname ?? '' }}</h1>
        @else
            @if ($hasStarted)
                <h1 class="title">Welcome back!</h1>
            @else
                <h1 class="title">Welcome!</h1>
            @endif
        @endif
    </div>
    <div id="welcome-subtitle">
        @if ($isSuperUserRequestingForAnotherUser)
            <div>
                Last access
            </div>
            <div class="date">
                {{ date('d/m/y', $lastAccessTime) }}
            </div>
            <div class="date">
                {{ date('H:i', $lastAccessTime) }}
            </div>
        @else
            <div>
                {{ $personalDetails->name }} {{ $personalDetails->surname }}
            </div>
            <div class="date">
                {{ date('d/m/y', $now) }}
            </div>
            <div class="date">
                {{ date('H:i', $now) }}
            </div>
        @endif
    </div>



    @include('includes.common-dashboard-menu')



    <?php
    $lastLU = reset($data);
    ?>

    @if (isset($lastLU) && $lastLU)
        <h2 id="last-lu-title">
            Last Learning Unit
        </h2>
        <div id="last-lu">
            <div id="last-lu-info">
                <span><?=$lastLU->title?></span>
                <span><?=date('d-m-Y', strtotime($lastLU->started_on))?></span>
                <span><?=date('H:i', strtotime($lastLU->started_on))?></span>
                <?php
                $completed = false;
                if ($lastLU->labour_market === false) {
                    if (!isset($lastLU->completed_on)) { ?>
                        <span>Test to start</span>
                    <?php } else if (!isset($lastLU->test_completed_on)) { ?>
                        <span>Test to complete</span>
                    <?php } else {
                        $completed = true;
                        ?>
                        <span>Completed</span>
                    <?php }
                } else {
                    if (!isset($lastLU->completed_on)) { ?>
                        <span>To complete</span>
                    <?php } else {
                        $completed = true;
                        ?>
                        <span>Completed</span>
                    <?php }
                }?>
            </div>
            <?php if ($isSuperUserRequestingForAnotherUser) {
                ; // avoid showing button in else statement below
            } else { ?>
                @if ($completed)
                    <a href="/ia/next" title="Start new Learning Unit" class="button primary">
                        Next
                        <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                    </a>
                @else
                    @if ($lastLU->labour_market === true)
                        <a href="/learning-unit-labour-market/<?=$lastLU->learning_unit_labour_market_id?>/show" title="Start new Learning Unit" class="button primary">
                    @else
                        <a href="/learning-unit/<?=$lastLU->learning_unit_id?>/<?=isset($lastLU->completed_on) ? 'test' : 'show'?>" title="Start new Learning Unit" class="button primary">
                    @endif
                            Resume
                            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                        </a>
                @endif
            <?php } ?>
        </div>
    @endif


    <?php
    function printDate($time, $format = 'd-m-Y H:i', $nullText = 'No date') {
        if (isset($time)) {
            return date('d-m-Y', strtotime($time)) . '<br />' . date('H:i', strtotime($time));
        }
        return $nullText;
    }
    ?>


    <h3 id="completed-lus">
        Completed Learning Units
    </h3>
    @include('includes.registry-table')


    <script>
        var uid = <?=($isSuperUserRequestingForAnotherUser ? $uid : '""')?>;
    </script>
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.1.12.1.min.css') }}" />
    <script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.dataTables-1.12.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard.js') }}"></script>


<?php } ?>

@stop

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/registry.css') }}" />
@stop