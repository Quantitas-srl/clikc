<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Terms and conditions
@stop


@section('content')


<h1>CLIKC Platform Privacy Policy</h1>

<p>
The CLIKC Project is a project that aims to test an
innovative methodology to train unemployed people on
transversal skills in an effective way, in order to reduce the
period of unemployment and allow the active labour
market policies to focus on most fragile or vulnerable
groups. This privacy policy will explain how our platform
uses the personal data we collect from you when you use
the CLIKC Platform (<a href="https://clikc.eu" target="_blank" rel="noopener" title="Clikc">https://clikc.eu</a>).
</p>

<h2>What data do we collect?</h2>

<p>
CLIKC Project collects the following data: 
</p>
<ul>
    <li>Personal identification information (Name and
Surname, Social Security Number and email address)</li>
    <li>Detailed data about the training skills and the learning
path of registered users</li>
</ul>



<h2>How do we collect your data?</h2>

<p>
You directly provide the CLIKC Project through the CLIKC
Platform. We collect data and process data when you:
</p>
<ul>
    <li>Register online to the Platform.</li>
    <li>Voluntarily complete a set of learning units and learning
unit tests or provide feedback on any of our message
boards or via email.</li>
    <li>Use or view our website via your browser's cookies.</li>
</ul>



<h2>How will we use your data?</h2>

<p>
The CLIKC project collects your data so that we can:
</p>
<ul>
    <li>Process our services, manage your account.</li>
    <li>Provide users with insights regarding their own
learning path.</li>
</ul>
<p>
If you agree, the CLIKC Project may share your data with
our CLIKC Project’s partners for analytical purposes within
the Project context.
</p>
<ul>
<li>Acción Laboral</li>
<li>Bit schulungscenter</li>
<li>ETI Malta</li>
<li>European Vocational training Association – EVTA</li>
<li>Nexa Center for Internet & Society</li>
<li>Quantitas S.r.l.</li>
<li>Tecum S.r.l.</li>
<li>Veneto Lavoro</li>
</ul>


<h2>How do we store your data?</h2>

<p>
The CLIKC project securely stores your data in servers
that are GDPR compliant, with all the security measures
needed, and located within the EU.
</p>
<p>
The CLIKC project will keep your personal data for all the
duration of the Project. Once this time period has expired,
we will delete your data.
</p>
<p>
Profiling: The CLIKC project will collect information about
your own training skills and learning path, and may share
that with 1) you, via Graphical User interface 2) CLIKC
project partners: 
</p>
<ul>
<li>Acción Laboral</li>
<li>Bit schulungscenter</li>
<li>ETI Malta</li>
<li>European Vocational training Association – EVTA</li>
<li>Nexa Center for Internet & Society</li>
<li>Quantitas S.r.l.</li>
<li>Tecum S.r.l.</li>
<li>Veneto Lavoro</li>
</ul>
<p>
If you have agreed to receive profiling, you may always
opt out at a later date by writing an email at
<a href="mailto:dpo@venetolavoro.it" title="dpo@venetolavoro.it">dpo@venetolavoro.it</a>.
</p>
<p>
You have the right at any time to stop the CLIKC Project
from profiling you or giving your data to other partners of
the CLIKC Project.
</p>



<h2>What are your data protection rights?</h2>

<p>
The CLIKC Project would like to make sure you are fully
aware of all of your data protection rights. Every user is
entitled to the following:
</p>

<p>
<strong>The right to access</strong> - You have the right to request the
CLIKC Project for copies of your personal data.
</p>
<p>
<strong>The right to rectification</strong> - You have the right to request
that the CLIKC Project correct any information you believe
is inaccurate. You also have the right to request the
CLIKC Project to complete information you believe is
incomplete.
</p>
<p>
<strong>The right to erasure</strong> - You have the right to request that
the CLIKC Project erase your personal data, under certain
conditions.
</p>
<p>
<strong>The right to restrict processing</strong> - You have the right to
request that CLIKC Project restrict the processing of your
personal data, under certain conditions.
</p>
<p>
<strong>The right to object to processing</strong> - You have the right to
object to the CLIKC Project's processing of your personal
data, under certain conditions.
</p>
<p>
<strong>The right to data portability</strong> - You have the right to
request that the CLIKC Project transfer the data that we
have collected to another organization, or directly to you,
under certain conditions.
</p>
<p>
If you make a request, we have one month to respond to
you. If you would like to exercise any of these rights,
please contact us at our email: <a href="mailto:dpo@venetolavoro.it" title="dpo@venetolavoro.it">dpo@venetolavoro.it</a>.
</p>



<h2>What are cookies?</h2>
<p>
Cookies are text files placed on your computer to collect
standard Internet log information and visitor behavior
information. When you visit our websites, we may collect
information from you automatically through cookies or
similar technology.
</p>
<p>
For further information, visit <a href="https://allaboutcookies.org/" target="_blank" rel="noopener" title="https://allaboutcookies.org/">https://allaboutcookies.org/</a>.
</p>



<h2>How do we use cookies?</h2>
<p>
The CLIKC Project uses cookies in a range of ways to
improve your experience on our website, including:
</p>
<ul>
    <li>Keeping you signed in</li>
    <li>Understanding how you use our website</li>
</ul>


<h2>What types of cookies do we use?</h2>

<p>
There are a number of different types of cookies, however,
our website uses:
</p>
<p>
Functionality – the CLIKC Platform uses these cookies so
that we recognize you on our website and remember your
previously selected preferences. These may include what
language you prefer and location you are in.
</p>



<h2>How to manage cookies</h2>
<p>
You can set your browser not to accept cookies, and the
above website tells you how to remove cookies from your
browser. However, in a few cases, some of our website
features may not function as a result.
</p>




<h2>Privacy policies of other websites</h2>
<p>
The CLIKC Platform contains links to other websites. Our
privacy policy applies only to our website, so if you click on
a link to another website, you should read their privacy
policy.
</p>




<h2>Changes to our privacy policy</h2>
<p>
The CLIKC Project keeps its privacy policy under regular
review and places any updates on this web page. This
privacy policy was last updated on November 16, 2022.
</p>




<h2>How to contact us </h2>
<p>
If you have any questions about the CLIKC Project’s
privacy policy, the data we hold on you, or you would like
to exercise one of your data protection rights, please do
not hesitate to contact us.
</p>
<p>
Email us at: <a href="mailto:dpo@venetolavoro.it" title="dpo@venetolavoro.it">dpo@venetolavoro.it</a>
</p>




<h2>How to contact the appropriate authority</h2>
<p>
Should you wish to report a complaint or if you feel that
Veneto Lavoro has not addressed your concern in a
satisfactory manner, you may contact the Information
Commissioner's Office.
</p>

<p>
Email: Address
</p>

<p>
    <a href="https://www.garanteprivacy.it/web/guest/home/footer/contatti" title="Garante privacy" target="_blank" rel="noopener">https://www.garanteprivacy.it/web/guest/home/footer/contatti</a>
</p>
<p>
    <a href="https://edps.europa.eu/about-edps/contact_en#contactus" title="EDPS" target="_blank" rel="noopener">https://edps.europa.eu/about-edps/contact_en#contactus</a>
</p>


<style>
    h1 {
        margin-bottom: 50px;
    }
    h2 {
        margin-top: 40px;
    }
</style>



@stop