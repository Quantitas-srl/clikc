@extends('layouts.default')

@section('title')
Unavailable Learning Unit
@stop


@section('content')

<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Sad face" title="Sad face" src="<?=asset('assets/img/sad-face.svg')?>" />
    </div>

    <div class="title-container">
        <h1 class="title">This learning unit isn't available to you yet</h1>
    </div>
    <p class="page-info">
        Sorry! This learning unit is not available for you at this moment.
    </p>
    
    <div class="text-center">
        <a href="/ia/next" title="Go to the running learning unit" class="button primary">
            Choose another learning unit
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
</div>


@stop