@extends('layouts.default')

@section('title')
Info
@stop


@section('content')

<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Bolt" title="Bolt" src="{{asset('assets/img/bolt.svg')}}" />
    </div>

    <div class="title-container">
        <h1 class="title">You already studied this Learning Unit.</h1>
    </div>
    <p class="page-info">
        You can now go to the test section, before proceeding with other Learning Units.
    </p>
    
    <div class="text-center">
        <a href="/learning-unit/{{$id}}/test" title="Go to the running Learning Unit" class="button primary">
            Go to the tests
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
</div>



@stop