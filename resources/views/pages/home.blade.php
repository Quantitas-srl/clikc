@extends('layouts.default')

@section('title')
CLIKC - Home page
@stop


@section('content')

<h1>Welcome to CLIKC</h1>

<ul>
    <li>
        <a href="registry" title="See your registry">Your registry</a>
    </li>
    <li>
        <a href="ia/next" title="New learning unit">New learning unit</a>
    </li>
    @if ($pendingId)
        <li>
            <a href="learning-unit/{{$pendingId}}/show" title="Pending learning unit">Pending learning unit</a>
        </li>
    @endif
</ul>


@stop