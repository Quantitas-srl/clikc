<?php
namespace App;
use App\Http\Controllers\TestController;
?>

@extends('layouts.default')

@section('title')
Learning Unit completed
@stop


@section('content')

<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Bolt" title="Bolt" src="<?=asset('assets/img/bolt.svg')?>" />
    </div>

    @if (isset($pendingId))
        <div class="title-container">
            <h1 class="title">You completed this Learning Unit.</h1>
        </div>
        <p class="page-info">
            There is another Learning Unit running.
        </p>
        <div class="text-center">
            <a href="/learning-unit/{{$pendingId}}/show" title="Go to the running Learning Unit" class="button primary">
                Go to running Learning Unit
                <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
            </a>
        </div>
    @else
        <div class="title-container">
            <h1 class="title">You completed this Learning Unit.</h1>
        </div>
        <p class="page-info">
            Go to the next Learning Unit selection.
        </p>
        <div class="text-center">
            <p>You will be redirected in <span id="redirection-counter"></span> seconds...</p>
            <small>Click the button below if you are not redirected.</small>
            <a href="/ia/next" title="Select next Learning Unit" class="button primary">
                Select next Learning Unit
                <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
            </a>
        </div>

        <script src="{{ asset('assets/js/lu-completed.js') }}"></script>
    @endif
    
</div>


@stop
