<?php
namespace App;

?>
@extends('layouts.default')

@section('title')
Next learning unit
@stop


@section('content')

<div id="ia-next-container">

    <div class="text-center">
        <img id="ia-next-info" src="{{asset('assets/img/info-yellow.svg')}}" alt="Info" title="The following Learning Units are selected by the CLIKC Algorithm and specifically tailored on your profile." />
        <img id="ia-next-icon" class="arrow" src="{{asset('assets/img/man-reading.svg')}}" alt="Icon" title="Icon" />
    </div>

    <div class="title-container">
        <h1 class="title"></h1>
    </div>
    <p class="page-info">
        Select a Learning Unit you want to proceed with
    </p>

    <div id="lus">
        <div id="waiters-container" class="text-center"> <!-- this content will be overwritten by JS when data are received -->
            <div class="waiter"></div>
            <div class="waiter"></div>
            <div class="waiter"></div>
            <div class="waiter"></div>
            <p>Please wait...</p>
        </div>
    </div>


    <div id="try-again-button-container" class="text-center hidden">
        <a href="" title="Try again" class="button secondary">
            Try again
        </a>
    </div>

</div>


<link href="{{ asset('assets/css/ia-next.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/ia-next.js') }}"></script>



@stop