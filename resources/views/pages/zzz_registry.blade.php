@extends('layouts.default')

@section('title')
Registry
@stop

@section('content')



<img src="{{ asset('assets/img/table.svg') }}" alt="Table" title="Table" id="registry-page-icon" />

<h1>Learning Units Registry</h1>

<div id="registry-top-info">
    <span class="username">{{ $username }}</span><br />
    <span class="date"><?= date('d/m/y', time()) ?></span>
</div>


@include('includes.registry-table')



<link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.1.12.1.min.css') }}" />
<script src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery.dataTables-1.12.1.min.js') }}"></script>
<script src="{{ asset('assets/js/registry.js') }}"></script>
<link rel="stylesheet" href="{{ asset('assets/css/registry.css') }}" />

@stop