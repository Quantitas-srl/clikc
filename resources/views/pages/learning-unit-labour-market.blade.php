<?php
namespace App;
use App\Http\Controllers\TestController;

?>
@extends('layouts.default')

@section('title')
LU - {{ $data->title }}
@stop


@section('content')

<?php
$uid = filter_input(INPUT_GET, 'ref');
$mode = $_COOKIE['clikc-user-mode'] ?? 'basic';
$isSuperUserRequestingForAnotherUser = $isSuperUser && ($mode === 'super');
?>

<link href="{{ asset('assets/css/lu.css') }}" rel="stylesheet">


<div class="info <?=$editable?'':'expanded'?>">

    <div class="text-center">
        <img class="info-icon <?=$editable?'':'bordered-image'?>" alt="Megaphone" title="Megaphone" src="<?=asset('assets/img/megaphone.svg')?>" />
    </div>

    <?php if ($editable) { ?>
        <div class="title-container">
            <h1 class="title">Labour tip!</h1>
        </div>
        <p class="page-info">
            Every 5 Learning Units you're presented with some handy tips on the Labour Market. Wanna have a look?
        </p>
    <?php } ?>
    <?php if ($editable) { ?>
        <div class="text-center" id="page-info-buttons-container">
            <button id="skip" title="Skip" class="button secondary" onclick="skipLab()">
                Skip
            </button>
            <button id="see-tip" title="See tip" class="button primary" onclick="seeTip()">
                See tip
                <img class="arrow" src="{{asset('assets/img/arrow-down.svg')}}" alt="Arrow" title="See tip" />
            </button>
        </div>
     <?php } ?>
    

    <div id="lulb-content" class="<?=$editable?'hidden':''?>">
        <div id="lulb-content-content">
            <?=$data->content?>
        </div>

        <?php if ($editable) { ?>
            <div id="lu-continue" class="lu-continue text-center">
                <a href="completed" title="Continue" class="button primary">
                    Select Next Learning Unit
                    <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                </a>
            </div>
        <?php } else { ?>
            @if ($isSuperUserRequestingForAnotherUser)
                <a href="/superuser/dashboard?uid=<?=$uid?>" title="Dashboard" class="button primary">
                    Dashboard
                </a>
            @else
                <a href="/dashboard" title="Dashboard" class="button primary">
                    Dashboard
                </a>
            @endif
        <?php } ?>
    </div>

</div>







<style>
    #lulb-content .button {
        margin-top: 30px;
    }
    #lulb-content-content li {
        margin-bottom: 5px;
    }
    #lulb-content-content img {
        display: block;
        margin: 30px auto;
        max-width: 500px;
    }
    #lulb-content-content a {
        word-break: break-all;
    }

    .info .button {
        margin: 0 5px;
    }
    .info.expanded {
        max-width: 800px;
    }
    #lu-continue .button {
        margin-top: 40px;
    }
    .bordered-image {
        border-bottom: 2px solid #0000CC;
        box-sizing: content-box;
        margin-bottom: 40px;
        padding: 0 80px 10px 80px;
    }

</style>

<script>
/**
 * Shows the LU
 */
function seeTip() {
    document.querySelector('.info').classList.add('expanded');
    document.getElementById('lulb-content').classList.remove('hidden');
    document.querySelector('h1.title').classList.add('hidden');
    document.querySelector('.page-info').classList.add('hidden');
    document.getElementById('page-info-buttons-container').classList.add('hidden');
    document.querySelector('.info-icon').classList.add('bordered-image');
}
/**
 * Skips a labour market learning unit by emulating click on the continue button
 */
function skipLab() {
    document.querySelector('#lu-continue .button').click();
}
</script>

@stop