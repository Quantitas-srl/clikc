@extends('layouts.default')

@section('title')
Reserved content
@stop


@section('content')



<div class="info">

    <div class="text-center">
        <img class="info-icon" alt="Bolt" title="Bolt" src="<?=asset('assets/img/bolt.svg')?>" />
    </div>

    <div class="title-container">
        <h1 class="title">Reserved content</h1>
    </div>
    <p class="page-info">
        This content is reserved to authenticated users.<br />
        Please login to view this content.
    </p>
    
    <div class="text-center">
        <a href="/login" title="Login" class="button primary">
            Login
            <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
        </a>
    </div>
</div>

@stop