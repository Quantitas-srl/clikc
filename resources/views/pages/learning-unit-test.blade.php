<?php
namespace App;
use App\Http\Controllers\TestController;
?>
@extends('layouts.default')

@section('title')
Learning Unit tests
@stop


@section('content')


<link href="{{ asset('assets/css/lu.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/test.css') }}" rel="stylesheet">


<div id="tabs-selector">
    <div class="tab-selector" tab-index="1">
        <div class="tab-selector-number">1</div>
        <span>Learning Unit</span>
    </div>
    <div class="tab-selector-separator"></div>
    <div class="tab-selector" tab-index="2">
        <div class="tab-selector-number">2</div>
        <span>Learning Unit Tests</span>
    </div>
</div>

<?php

$isGroupSuperuser = false;
$group = '';
if ($isSuperUserViewing ) {
    $superuserEmailParts1 = explode('@', $loggedUserEmail);
    $superuserEmailParts2 = explode('-', $superuserEmailParts1[0]);
    $group = $superuserEmailParts2[1] ?? '';
    $user = $superuserEmailParts2[2] ?? '';
    if (intval($group) > 0 && intval($group) <= 99) {
        $isGroupSuperuser = true;
    }
}

if ($isGroupSuperuser && strpos($userEmail, '-'.$group.'-') === false) {
    echo 'You cannot view this user.';
} else {

    ?>

    <div class="lu-cluster">
        <p>
            {{ $clusters[+$learning_unit->cluster_number] }}
        </p>
    </div>

    <div class="lu-content">
        
        <div class="lu-title">
            <h1>{{ $learning_unit->title }}</h1>
        </div>

    </div>



    <div>
        <?php
        $n = 0;
        foreach ($testsData as $testData) {
            $savedData = isset($savedTestsData[$testData['id']]) ? $savedTestsData[$testData['id']] : null;
            $t = new TestController($testData, $savedData);
            $t->show(++$n, $isSuperUserViewing);
        }
        ?>
    </div>


    <div id="tests-recap">
        Completed tests:
        <span id="completed-tests-amount">
        </span>
        <?php if ($editable) { ?>
            <div id="tests-continue">
                <a class="button primary hidden" title="Continue" href="/learning-unit/{{ $learning_unit->id }}/test-completed">
                    Continue
                    <img class="arrow" src="{{asset('assets/img/arrow-double-right.svg')}}" alt="Arrow" title="Next" />
                </a>
            </div>
        <?php } else if (!$isSuperUserViewing) { ?>
            <div id="tests-continue">
                <a class="button primary hidden" title="Learning Unit" href="/learning-unit/{{ $learning_unit->id }}/show">
                    Learning Unit
                </a>
                <a class="button primary hidden" title="Dashboard" href="../../dashboard">
                    Dashboard
                </a>
            </div>
        <?php } else { // is superuser viewing ?>
            <div id="tests-continue">
                <a class="button primary hidden" title="Learning Unit" href="../../learning-unit/{{ $learning_unit->id }}/show?ref=<?=filter_input(INPUT_GET, 'ref')?>">
                    Learning Unit
                </a>
                <a class="button primary hidden" title="Dashboard" href="../../dashboard?uid=<?=filter_input(INPUT_GET, 'ref')?>">
                    Dashboard
                </a>
            </div>
        <?php } ?>
    </div>
    <script src="{{ asset('assets/js/test.js') }}"></script>

<?php } ?>

@stop


