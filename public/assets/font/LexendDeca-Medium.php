<?php
$type = 'TrueType';
$name = 'LexendDeca-Medium';
$desc = array('Ascent'=>1000,'Descent'=>-250,'CapHeight'=>700,'Flags'=>32,'FontBBox'=>'[-209 -245 1385 1080]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>584);
$up = -100;
$ut = 50;
$cw = array(
	chr(0)=>584,chr(1)=>584,chr(2)=>584,chr(3)=>584,chr(4)=>584,chr(5)=>584,chr(6)=>584,chr(7)=>584,chr(8)=>584,chr(9)=>584,chr(10)=>584,chr(11)=>584,chr(12)=>584,chr(13)=>584,chr(14)=>584,chr(15)=>584,chr(16)=>584,chr(17)=>584,chr(18)=>584,chr(19)=>584,chr(20)=>584,chr(21)=>584,
	chr(22)=>584,chr(23)=>584,chr(24)=>584,chr(25)=>584,chr(26)=>584,chr(27)=>584,chr(28)=>584,chr(29)=>584,chr(30)=>584,chr(31)=>584,' '=>294,'!'=>262,'"'=>435,'#'=>698,'$'=>628,'%'=>885,'&'=>752,'\''=>221,'('=>374,')'=>374,'*'=>406,'+'=>593,
	','=>274,'-'=>413,'.'=>238,'/'=>570,'0'=>631,'1'=>522,'2'=>548,'3'=>551,'4'=>635,'5'=>578,'6'=>565,'7'=>546,'8'=>586,'9'=>560,':'=>238,';'=>272,'<'=>524,'='=>586,'>'=>525,'?'=>520,'@'=>965,'A'=>698,
	'B'=>688,'C'=>684,'D'=>757,'E'=>625,'F'=>608,'G'=>771,'H'=>768,'I'=>505,'J'=>650,'K'=>734,'L'=>602,'M'=>869,'N'=>798,'O'=>793,'P'=>644,'Q'=>793,'R'=>697,'S'=>627,'T'=>604,'U'=>749,'V'=>714,'W'=>995,
	'X'=>692,'Y'=>670,'Z'=>668,'['=>409,'\\'=>567,']'=>409,'^'=>584,'_'=>689,'`'=>500,'a'=>642,'b'=>640,'c'=>539,'d'=>641,'e'=>586,'f'=>393,'g'=>656,'h'=>609,'i'=>293,'j'=>283,'k'=>589,'l'=>270,'m'=>936,
	'n'=>609,'o'=>624,'p'=>641,'q'=>640,'r'=>421,'s'=>485,'t'=>389,'u'=>618,'v'=>584,'w'=>812,'x'=>578,'y'=>595,'z'=>507,'{'=>416,'|'=>262,'}'=>416,'~'=>527,chr(127)=>584,chr(128)=>658,chr(129)=>584,chr(130)=>274,chr(131)=>532,
	chr(132)=>499,chr(133)=>652,chr(134)=>440,chr(135)=>411,chr(136)=>500,chr(137)=>1247,chr(138)=>627,chr(139)=>288,chr(140)=>1045,chr(141)=>584,chr(142)=>668,chr(143)=>584,chr(144)=>584,chr(145)=>253,chr(146)=>253,chr(147)=>469,chr(148)=>466,chr(149)=>346,chr(150)=>543,chr(151)=>891,chr(152)=>500,chr(153)=>825,
	chr(154)=>485,chr(155)=>289,chr(156)=>1012,chr(157)=>584,chr(158)=>507,chr(159)=>670,chr(160)=>294,chr(161)=>262,chr(162)=>545,chr(163)=>620,chr(164)=>554,chr(165)=>670,chr(166)=>269,chr(167)=>558,chr(168)=>500,chr(169)=>788,chr(170)=>374,chr(171)=>513,chr(172)=>670,chr(173)=>415,chr(174)=>507,chr(175)=>424,
	chr(176)=>532,chr(177)=>603,chr(178)=>353,chr(179)=>347,chr(180)=>500,chr(181)=>605,chr(182)=>730,chr(183)=>238,chr(184)=>500,chr(185)=>284,chr(186)=>370,chr(187)=>517,chr(188)=>803,chr(189)=>800,chr(190)=>812,chr(191)=>520,chr(192)=>698,chr(193)=>698,chr(194)=>698,chr(195)=>698,chr(196)=>698,chr(197)=>698,
	chr(198)=>1007,chr(199)=>684,chr(200)=>625,chr(201)=>625,chr(202)=>625,chr(203)=>625,chr(204)=>505,chr(205)=>505,chr(206)=>505,chr(207)=>505,chr(208)=>757,chr(209)=>798,chr(210)=>793,chr(211)=>793,chr(212)=>793,chr(213)=>793,chr(214)=>793,chr(215)=>501,chr(216)=>783,chr(217)=>749,chr(218)=>749,chr(219)=>749,
	chr(220)=>749,chr(221)=>670,chr(222)=>644,chr(223)=>665,chr(224)=>642,chr(225)=>642,chr(226)=>642,chr(227)=>642,chr(228)=>642,chr(229)=>642,chr(230)=>899,chr(231)=>539,chr(232)=>586,chr(233)=>586,chr(234)=>586,chr(235)=>586,chr(236)=>259,chr(237)=>259,chr(238)=>259,chr(239)=>259,chr(240)=>582,chr(241)=>609,
	chr(242)=>624,chr(243)=>624,chr(244)=>624,chr(245)=>624,chr(246)=>624,chr(247)=>597,chr(248)=>624,chr(249)=>618,chr(250)=>618,chr(251)=>618,chr(252)=>618,chr(253)=>595,chr(254)=>641,chr(255)=>595);
$enc = 'cp1252';
$uv = array(0=>array(0,128),128=>8364,130=>8218,131=>402,132=>8222,133=>8230,134=>array(8224,2),136=>710,137=>8240,138=>352,139=>8249,140=>338,142=>381,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),152=>732,153=>8482,154=>353,155=>8250,156=>339,158=>382,159=>376,160=>array(160,96));
$file = 'LexendDeca-Medium.z';
$originalsize = 19864;
$subsetted = true;
?>
