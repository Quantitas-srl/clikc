(function() {



    /**
     * Checks if the personal details tab is completed
     * @returns true if the personal details tab is completed, false otherwise
     */
    function isPersonalDetailsTabCompleted() {
        return true;
    }


    /**
     * Checks if the self assessment tab is completed
     * @returns true if the assessment tab is completed, false otherwise
     */
    function isSelfAssessmentTabCompleted() {
        var firstMissing = -1;
        [...document.querySelectorAll('.clusters-block')].map((block, b) => {
            const n = block.querySelectorAll('triple-choice:not([value=""])').length;
            (n < 3 && firstMissing < 0) && (firstMissing = b);
            return n;
        });
        return firstMissing;
    }


    /**
     * Shows the n-th tab
     * @param {number} n the n-th tab to show
     */
    function showTab(n) {
        [...document.querySelectorAll('.tab')].forEach(tab => {
            tab.classList.toggle('hidden', n !== +tab.getAttribute('tab-index'));
        });
    }


    /**
     * Checks if all is completed
     */
    function checkCompleted() {
        if (isSURFA || missingMandatory || !isPersonalDetailsTabCompleted()) {
            showTab(1);
            disableTabSelectorsUntil(1);
        } else if ((nm = isSelfAssessmentTabCompleted()) > -1) {
            showTab(2);
            expandSoftSkills(true);
            disableTabSelectorsUntil(2);
        } else {
            showTab(3);
            disableTabSelectorsUntil(3);
        }
    }


    /**
     * Disables an amount of tab selectors
     * @param {number} n how many tab selectors to disable
     */
    function disableTabSelectorsUntil(n) {
        [...document.querySelectorAll('.tab-selector')].forEach((tab, i) => {
            if (i < n) {
                if (i === 0 || !missingMandatory) {
                    tab.classList.remove('disabled');
                } else {
                    tab.classList.add('disabled');
                }
            } else {
                tab.classList.add('disabled');
            }
        });
    }


    /**
     * Further expands soft skills list
     * @param {boolean} plusOne set this to true on first call
     */
    function expandSoftSkills(plusOne) {
        var n = 0;
        [...document.querySelectorAll('triple-choice')].forEach(choice => {
            choice.getAttribute('value') && (n++);
        });
        !n && (n = 1);
        plusOne && (n++);
        const nBlocks = Math.ceil(n / 3);
        
        for (let i = 0; i < nBlocks; i++) {
            document.querySelector('.clusters-block[data-n="' + i + '"]').classList.remove('hidden');
            document.querySelector('.clusters-block-accorder[data-n="' + i + '"]').classList.remove('hidden');
            if (i < nBlocks-1) {
                document.querySelector('.clusters-block[data-n="' + i + '"]').classList.add('compressed');
                document.querySelector('.clusters-block-accorder[data-n="' + i + '"] span').classList.remove('rotated');
            } else {
                document.querySelector('.clusters-block-accorder[data-n="' + i + '"] span').classList.add('rotated');
            }
        }
        if (nBlocks > 3) {
            document.querySelector('#tab-self-assessment .inner-next').classList.add('hidden');
            if (n % 3 === 0) {
                document.querySelector('#tab-self-assessment .next').classList.remove('hidden');
            } else {
                document.querySelector('#tab-self-assessment .next').classList.add('hidden');
            }
        } else {
            document.querySelector('#tab-self-assessment .next').classList.add('hidden');
            if (n % 3 === 0) {
                document.querySelector('#tab-self-assessment .inner-next').classList.remove('hidden');
            } else {
                document.querySelector('#tab-self-assessment .inner-next').classList.add('hidden');
            }
        }
    }



    /**
     * Event on choice
     * @param {event} e the event
     */
    function onchoice(e) {
        const target = e.target;
        const cluster = +target.getAttribute('data-cluster');
        const value = +e.detail.value;
        target.setAttribute('value', value);
        expandSoftSkills(false);
        scrollTillEnd();        
        ajax({
            url: 'set-cluster-skill',
            data: 'cluster=' + cluster + '&value=' + value,
            onsuccess: res => { }
        });
    }



    /**
     * Event on click
     * @param {event} e the event
     */
    function click(e) {
        const target = e.target;
        const id = target.id;
        const classList = target.classList;
        if (id === 'go-to-dashboard') {
            !target.hasAttribute('disabled') && (window.location.href = 'dashboard');
        } else if (id === 'register') {
            document.getElementById('modal-confirm-message').classList.remove('hidden');
            setTimeout(() => {
                document.getElementById('confirm-message').classList.add('shown');
                let seconds = 4;
                const intv = setInterval(_ => {
                    document.getElementById('redirection-seconds').textContent = seconds;
                    seconds--;
                    if (!seconds) {
                        clearInterval(intv);
                        window.location.href = 'ia/next';
                        return ;
                    }
                }, 1000);
            }, 10);
        } else if (id === 'modal-confirm-message') {
            window.location.href = 'ia/next';
        } else if (classList.contains('soft-skill')) {
            if (!isLearningPreferencesTabCompleted() || target.classList.contains('active')) {
                setSoftSkill(target);
                showOrHideDashboardButton();
            }
        } else if (classList.contains('tab-selector')) {
            if (!classList.contains('disabled')) {
                const index = +target.getAttribute('tab-index');
                showTab(index);
                index === 2 && (expandSoftSkills(false));
            }
        } else if (classList.contains('next')) {
            if (!target.hasAttribute('disabled')) {
                clickOnTab(+target.getAttribute('data-goto'));
                scrollToTop();
            }
        } else if (classList.contains('inner-next')) {
            expandSoftSkills(true);
            scrollTillEnd();
        } else if (classList.contains('clusters-block-accorder')) {
            const n = target.getAttribute('data-n');
            document.querySelector('.clusters-block[data-n="' + n + '"]').classList.toggle('compressed');
            document.querySelector('.clusters-block-accorder[data-n="' + n + '"] span').classList.toggle('rotated');
        }
    }



    /**
     * What to do when click on tab occurs
     * @param {number} n the index of the tab
     */
    function clickOnTab(n) {
        const item = document.querySelector('.tab-selector[tab-index="' + n + '"]');
        item.classList.remove('disabled');
        item.click();
    }


    /**
     * Counts the selected sofr skills
     * @returns the selected soft skills
     */
    function countSetSoftSkills() {
        return [...document.querySelectorAll('.soft-skill')].reduce((accum, skill) => {
            return accum + (skill.classList.contains('active') ? 1 : 0);
        }, 0);
    }


    /**
     * Sets the value of a soft skill
     * @param {dom} target the clicked element
     */
    function setSoftSkill(target) {
        target.classList.toggle('active');
        const cluster = +target.getAttribute('data-cluster');
        const isSet = target.classList.contains('active') ? 1 : 0;
        ajax({
            url: 'set-cluster-preference',
            data: 'cluster=' + cluster + '&value=' + isSet,
            onsuccess: res => { }
        });
    }


    /**
     * Shows the dashboard button
     */
    function showDashboardButton() {
        document.getElementById('dashboard-button-container').classList.remove('hidden');
        scrollTillEnd();
    }


    /**
     * Hides the dashboard button
     */
    function hideDashboardButton() {
        document.getElementById('dashboard-button-container').classList.add('hidden');
    }


    /**
     * If the preferences tab is completed, shows the dashboard button
     * Else hides it
     */
    function showOrHideDashboardButton() {
        if (isLearningPreferencesTabCompleted()) {
            showDashboardButton();
        } else {
            hideDashboardButton();
        }
    }


    /**
     * Checks if the preferences tab is completed
     * @returns true if the preferences tab is completed, false otherwise
     */
    function isLearningPreferencesTabCompleted() {
        return countSetSoftSkills() === 3;
    }


    /**
     * Listens to changes on personal details fields
     * When a field blurs, updates information on db
     */
    function listenToPersonalDetailsBlur() {
        document.querySelectorAll('#tab-personal-details input').forEach(item => {
            if (!item.disabled) {
                item.addEventListener('blur', blur);
                item.addEventListener('change', blur);
                item.addEventListener('keyup', keyup);
            }
        });
        document.querySelectorAll('#tab-personal-details select').forEach(item => {
            if (!item.disabled) {
                item.addEventListener('change', change);
            }
        });
    }


    /**
     * Listens to keyup events
     * @param {event} e the event
     */
    function keyup(e) {
        toggleFormDataBlocks();
    }


    /**
     * Listens to blur events
     * @param {event} e the event
     */
    function blur(e) {
        savePersonalDetails();
        toggleFormDataBlocks();
    }


    /**
     * Listens to change events
     * @param {event} e the event
     */
    function change(e) {
        savePersonalDetails();
        if (e.target.id === 'country' && e.target.value && e.target.value !== lastSelectedCountry) {
            document.getElementById('highest-educational-attainment').removeAttribute('data-init-value'); // remove, since it is changed
            setCountryDependingData(e.target.value, toggleFormDataBlocks);
            const btn = (document.getElementById('go-to-dashboard') || document.getElementById('go-to-tab-2'));
            btn && (btn.setAttribute('disabled', true));
        } else {
            toggleFormDataBlocks();
        }
    }


    /**
     * Sets the value of the country select
     * @param {string} value the value of the last selected country
     * @param {function} callback the callback
     */
    function setCountryDependingData(value, callback) {
        lastSelectedCountry = value;
        ajax({
            url: 'startup-depending-data',
            data: 'key=highest-educational-attainment&depending=' + value,
            onsuccess: res => {
                setSelect('highest-educational-attainment', JSON.parse(res));
                let sel = document.getElementById('highest-educational-attainment');
                sel.getAttribute('data-init-value') && (sel.value = sel.getAttribute('data-init-value'));
                callback && (callback());
            }
        });
    }


    /**
     * Puts options into a select
     * @param {string} id the id of the select
     * @param {array} options the options
     */
    function setSelect(id, options) {
        var s = '<option value=""></option>';
        for (let value in options) {
            s += '<option value="' + value + '">' + options[value] + '</option>';
        }
        const sel = document.getElementById(id);
        sel.innerHTML = s;
    }


    /**
     * Saves personal details data
     */
    function savePersonalDetails() {
        if (isSURFA) {
            return ;
        }
        var data = {};
        missingMandatory = false;
        document.querySelectorAll('#tab-personal-details input').forEach(item => {
            //if (!item.disabled) {
                if (item.type === 'checkbox') {
                    data[item.id] = item.checked ? '1' : '0';
                } else {
                    data[item.id] = item.value;
                }
            //}
            if (item.required) {
                if (item.type === 'checkbox') {
                    !item.checked && (missingMandatory = true);
                } else {
                    !item.value && (missingMandatory = true);
                }
            }
        });
        document.querySelectorAll('#tab-personal-details select').forEach(item => {
            if (!item.disabled) {
                data[item.id] = item.value;
            }
            if (item.required) {
                !item.value && (missingMandatory = true);
            }
        });
        const btn = (document.getElementById('go-to-dashboard') || document.getElementById('go-to-tab-2'));
        btn && (btn.toggleAttribute('disabled', missingMandatory));
        ajax({
            url: 'update-profile-data',
            data: 'data=' + JSON.stringify(data),
            onsuccess: res => {

            }
        });
    }


    /**
     * Checks if the user has already started doing learning units.
     * The flag, defined by php backend, is the presence or not of #tabs-selector
     * element on the page: if present, user didn't start yet.
     * @returns {boolean} true if the user already started, false otherwise
     */
    function userHasAlreadyStarted() {
        return document.getElementById('tabs-selector') === null;
    }


    /**
     * Toggles form inputs visibility depending on their status
     */
    function toggleFormDataBlocks() {
        const requiredFieldsForDataBlock = {
            1: {visible: true, requires: []},
            2: {visible: true, requires: ['email', 'name', 'surname']},
            3: {visible: true, requires: ['email', 'name', 'surname', 'age', 'gender', 'country']}
        };
        document.querySelectorAll('#tab-personal-details input, #tab-personal-details select').forEach(item => {
            for (let i in requiredFieldsForDataBlock) {
                const requiredData = requiredFieldsForDataBlock[i];
                if (requiredData.requires.indexOf(item.id) > -1 && !item.value) {
                    requiredFieldsForDataBlock[i].visible = false;
                }
            }
            for (let i in requiredFieldsForDataBlock) {
                document.getElementById('data-block-' + i).classList.toggle('hidden', !requiredFieldsForDataBlock[i].visible);
            }
        });
    }


    /**
     * Inits
     */
    function init() {
        var started = userHasAlreadyStarted();
        document.addEventListener('click', click);
        if (!started) {
            document.querySelectorAll('triple-choice').forEach(choice => {
                choice.addEventListener('onchoice', onchoice);
            });
        }
        setTimeout(_ => {
            if (country.value) {
                setCountryDependingData(country.value, res => {
                    endInit(started);
                });
            } else {
                endInit(started);
            }
        }, 100);
    }


    /**
     * Prosecution of init function
     * @param {boolean} started true if already started
     */
    function endInit(started) {
        savePersonalDetails();
        checkCompleted();
        (!started && !isSURFA) && (showOrHideDashboardButton());
        listenToPersonalDetailsBlur();
        hideInputPlaceholdersIfNeeded();
        (started && !isSURFA) && (showTab(1));
        toggleFormDataBlocks();

        // show other user data
        if (isSURFA) {
            if (document.getElementById('tab-self-assessment')) {
                document.getElementById('tab-self-assessment').classList.remove('hidden');
                document.querySelector('#tab-self-assessment .page-info').classList.add('hidden');
                document.querySelectorAll('.clusters-block').forEach(block => {
                    block.classList.remove('hidden');
                    block.style.background = 'transparent';
                });

                const overlay1 = document.createElement('div');
                overlay1.classList.add('events-blocking-overlay');
                document.getElementById('tab-self-assessment').appendChild(overlay1);
            }
            if (document.getElementById('tab-learning-preferences')) {
                document.getElementById('tab-learning-preferences').classList.remove('hidden');
                document.querySelector('#tab-learning-preferences .page-info').classList.add('hidden');
                
                const overlay2 = document.createElement('div');
                overlay2.classList.add('events-blocking-overlay');
                document.getElementById('tab-learning-preferences').appendChild(overlay2);
            }
            
            document.getElementById('dashboard-button-container') && (document.getElementById('dashboard-button-container').classList.add('hidden'));



        }
    }


    var lastSelectedCountry = '';
    var missingMandatory = false; // flag to check missing mandatory personal data
    init();

})();
