(function() {

    const table = document.getElementById('registry-table');


    /**
     * What to do on table click
     * @param {event} e the event
     */
    function tableClick(e) {
        if (e.target.tagName.toLowerCase() === 'td') {
            window.location.href = e.target.parentNode.getAttribute('data-link');
        }
    }


    /**
     * Events
     */
    function events() {
        table.addEventListener('click', tableClick);
    }


    /**
     * Inits
     */
    function init() {
        $(table).DataTable({
            order: [[3, 'desc']]
        });
        events();
    }

    init();

})();