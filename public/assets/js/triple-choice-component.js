'use strict';

(function() {
  class TripleChoice extends HTMLElement {
    constructor() {
      // establish prototype chain
      super();

      // attaches shadow tree and returns shadow root reference
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow
      const shadow = this.attachShadow({ mode: 'open' });

      // creating a container for the editable-list component
      const tripleChoiceContainer = document.createElement('div');

      // get attribute values from getters
      const title = this.title;
      const test = this.getAttribute('data-test');
      this.range = [1,2,3];

      // adding a class to our container for the sake of clarity
      tripleChoiceContainer.classList.add('triple-choice');

      // creating the inner HTML of the editable list element
      tripleChoiceContainer.innerHTML = `
        <style>
          .triple-choice {
            display: block;
            height: 150px;
            position: relative;
            width: 100%;
          }
          .triple-choice-choices-container {
            background: #FEF5E8;
            border-radius: 20px;
            box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.15);
            display: block;
            font-size: 0px;
            height: 10px;
            position: relative;
            width: 100%;
          }
          .triple-choice-choice {
            cursor: pointer;
            display: inline-block;
            height: 25px;
            left: 0;
            margin-top: 0;
            position: relative;
            text-align: center;
            top: 0px;
            width: 33.33%;
          }
          .triple-choice-choice div, .triple-choice-choice p {
            pointer-events: none;
          }
          .triple-choice-dot {
            background: #FE9900;
            border: 0px solid #FE9900;
            border-radius: 25px;
            display: inline-block;
            height: 22.5px;
            margin-bottom: 8px;
            position: relative;
            width: 22.5px;
          }
          .triple-choice-choice:hover .triple-choice-dot {
            border: 3.5px solid #0906c8;
            margin-bottom: 4.5px;
            margin-top: -3.5px;
          }
          .triple-choice-choice.active .triple-choice-dot {
            border: 5px solid #0906c8;
            margin-bottom: 3px;
            margin-top: -5px;
          }
          .triple-choice-text {
            color: #000033;
            font-size: 13px;
            font-weight: 350;
            letter-spacing: 0.015em;
            line-height: 140%;
            margin: 0 0 2px 0;
            pointer-events: none;
            position: relative;
          }
          h2 {
            color: #fe9900;
            font-size: 17px;
            font-weight: 550;
            line-height: 21px;
            text-align: center;
            text-transform: uppercase;
          }
        </style>
        <h2></h2>
        <div class="triple-choice-choices-container">
          <div class="triple-choice-choice" data-value="${this.range[0]}" title="I have some basic knowledge of it but would like to know more">
            <div class="triple-choice-dot"></div>
            <p class="triple-choice-text">Beginner</p>
            
          </div>
          <div class="triple-choice-choice" data-value="${this.range[1]}" title="I have a general understading of it and I am able to use appropriate tools to deal well with it, sometimes with help">
            <div class="triple-choice-dot"></div>
            <p class="triple-choice-text">Intermediate</p>
            
          </div>
          <div class="triple-choice-choice" data-value="${this.range[2]}" title="I know a lot about it, I am familiar with this and have experience in it">
            <div class="triple-choice-dot"></div>
            <p class="triple-choice-text">Advanced</p>
            
          </div>
        </div>
      `;
      //<img src="/assets/img/info.svg" alt="Info" title="I have some basic knowledge of it but would like to know more" />
      //<img src="/assets/img/info.svg" alt="Info" title="I have a general understading of it and I am able to use appropriate tools to deal well with it, sometimes with help" />
      //<img src="/assets/img/info.svg" alt="Info" title="I know a lot about it, I am familiar with this and have experience in it" />


      // binding methods
      this.choose = this.choose.bind(this);
      this.handleRemoveItemListeners = this.handleRemoveItemListeners.bind(this);

      // appending the container to the shadow DOM
      shadow.appendChild(tripleChoiceContainer);
    }

    // choose a choice
    choose(e) {
      const value = +e.target.getAttribute('data-value');
      e.target.parentNode.parentNode.setAttribute('value', value);
      [...this.shadowRoot.querySelectorAll('.triple-choice-choice')].forEach(choice => {
        choice.classList.toggle('active', +choice.getAttribute('data-value') === value);
      });
      this.dispatchEvent(new CustomEvent('onchoice', { detail: {value:value} } ));
    }

    // fires after the element has been attached to the DOM
    connectedCallback() {

      this.shadowRoot.querySelector('h2').textContent = this.title;
      
      if (this.getAttribute('data-range')) {
        this.range = this.getAttribute('data-range').split(',').map(item => +item);
        [...this.shadowRoot.querySelectorAll('.triple-choice-choice')].forEach((choice, i) => {
          choice.setAttribute('data-value', this.range[i]);
        });
      }

      var initialValue = +this.getAttribute('value');
      [...this.shadowRoot.querySelectorAll('.triple-choice-choice')].forEach(choice => {
        choice.addEventListener('click', this.choose, false);
        if (+choice.getAttribute('data-value') === initialValue) {
          choice.classList.add('active');
        }
      });
    }

    handleRemoveItemListeners(arrayOfElements) {
      arrayOfElements.forEach(element => {
        element.addEventListener('click', this.choose, false);
      });
    }

  }

  // let the browser know about the custom element
  customElements.define('triple-choice', TripleChoice);
})();