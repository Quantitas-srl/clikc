
/**
 * Events
 */
function events() {
    document.addEventListener('change', onchange);
    document.addEventListener('click', onclick);
    document.addEventListener('drop', ondrop);
    document.querySelectorAll('.form-input').forEach(item => {
        item.addEventListener('focus', onFormInputFocus);
        item.addEventListener('blur', onFormInputBlur);
    });
}


/**
 * When an input gains the focus, hides its .placeholder
 * @param {event} e the event
 */
function onFormInputFocus(e) {
    hidePlaceholder(e.target);
}


/**
 * When an input loses the focus, shows its .placeholder
 * @param {event} e the event
 */
function onFormInputBlur(e) {
    !e.target.value && (showPlaceholder(e.target));
}


/**
 * Event on drop
 * @param {event} e the event
 */
function ondrop(e) {
    const target = e.target;
    const classList = target.classList;
    if (classList.contains('droppable')) {
        const id = target.getAttribute('data-id');
        var completed = 0;
        var expected = 0;
        document.querySelectorAll('.droppable[data-id="' + id + '"]').forEach(item => {
            completed += item.children.length > 0 ? 1 : 0;
            expected++;
        });
        if (completed === expected) {
            document.querySelector('.test-submit[data-id="' + id + '"]').removeAttribute('disabled');
        } else {
            document.querySelector('.test-submit[data-id="' + id + '"]').setAttribute('disabled', 'true');
        }
    }
    
}


/**
 * Event on click
 * @param {event} e the event
 */
function onclick(e) {
    const target = e.target;
    const id = target.id;
    const classList = target.classList;
    if (id === 'header-buttons-cta') {
        e.stopPropagation();
        document.getElementById('header-buttons').classList.toggle('shown');
        return false;
    } else if (classList.contains('test-submit')) {
        e.preventDefault();
        testSubmit(target);
    } else if (classList.contains('test-submit-confirm-submit')) {
        e.preventDefault();
        confirmSubmit(target);
    } else if (classList.contains('test-submit-cancel-submit')) {
        e.preventDefault();
        cancelSubmit(target);
    } else if (classList.contains('form-input')) {
        hidePlaceholder(target);
    }
    document.getElementById('header-buttons').classList.remove('shown');
    return false;
}


/**
 * Event on change
 * @param {event} e the event
 */
function onchange(e) {
    const target = e.target;
    const classList = target.classList;
    if (classList.contains('cloze')) {
        clozeChange(target);
    }
}


events();

