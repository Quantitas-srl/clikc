
/**
 * An ajax request
 * @param {object} obj the request, having: url string, data string, onsuccess function
 */
function ajax(obj) {
    const xhr = new XMLHttpRequest();
    xhr.open("POST", window.location.origin + '/ajax/' + obj.url, true);

    // Send the proper header information along with the request
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("X-CSRF-TOKEN", document.querySelector('meta[name="csrf-token"]').content);

    xhr.onreadystatechange = function() { // Call a function when the state changes
        this.readyState === XMLHttpRequest.DONE && this.status === 200 && obj.onsuccess && (obj.onsuccess(this.responseText));
    }
    xhr.send(obj.data);
}


/**
 * Hides the .placeholder in a form group
 * @param {dom} target the input element in the form group
 */
function hidePlaceholder(target) {
    target.parentNode.querySelector('.placeholder').classList.add('hidden');
}


/**
 * Shows the .placeholder in a form group
 * @param {dom} target the input element in the form group
 */
function showPlaceholder(target) {
    target.parentNode.querySelector('.placeholder').classList.remove('hidden');
}


/**
 * Hides .placeholder on valorized input, for all inputs
 */
function hideInputPlaceholdersIfNeeded() {
    document.querySelectorAll('.form-input').forEach(item => {
        item.value && (hidePlaceholder(item));
    });
}


/**
 * Scrolls to the end of the page
 */
function scrollTillEnd() {
    window.scrollTo(0, 10000);
}


/**
 * Scrolls to the beginning of the page
 */
function scrollToTop() {
    window.scrollTo(0, 0);
}