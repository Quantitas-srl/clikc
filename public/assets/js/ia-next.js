(function() {


    /**
     * Event on mouseover
     * @param {event} e the event
     */
    function mouseover(e) {
        const target = e.target;
        const classList = target.classList;
        if (classList.contains('ia-next-lu')) {
            target.classList.add('ia-next-lu-' + target.getAttribute('data-i') + '-shown');
            target.classList.add('shown');
        }
    }

    
    /**
     * Event on mouseout
     * @param {event} e the event
     */
    function mouseout(e) {
        const target = e.target;
        const classList = target.classList;
        if (classList.contains('ia-next-lu')) {
            target.classList.remove('ia-next-lu-' + target.getAttribute('data-i') + '-shown');
            target.classList.remove('shown');
        }
    }


    /**
     * Resizes properly
     */
    function resize() {
        document.querySelectorAll('.ia-next-lu-subtitle').forEach((item, idx) => {
            sizes[idx] = item.getBoundingClientRect().height;
        });
        rule();
    }


    /**
     * Updates css rules where needed
     */
    function rule() {
        for (let i in document.styleSheets) {
            if (document.styleSheets[i].href) {
                if (document.styleSheets[i].href.indexOf('clikc') > -1 || document.styleSheets[i].href.indexOf('localhost') > -1) {
                    for (let j in document.styleSheets[i].cssRules) {
                        for (let k = 0; k < 3; k++) {
                            if (document.styleSheets[i].cssRules[j].selectorText === '.ia-next-lu-' + k + '-shown .ia-next-lu-subtitle-container') {
                                document.styleSheets[i].cssRules[j].style.height = sizes[k] + 'px';
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * Checks if the device is a mobile or tablet
     * @returns true if it's a mobile or tablet
     */
    window.mobileAndTabletCheck = function() {
        let check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
    };


    /**
     * Asks next LUs to IA
     */
    function askNext() {
        ajax({
            url: 'ia-next',
            data: '',
            onsuccess: showLUs
        });
    }


    /**
     * Shows LUs from IA response
     * @param {*} res the LUs data
     */
    function showLUs(res) {
        res = JSON.parse(res);
        if (res.error) {
            const icon = document.getElementById('ia-next-icon');
            icon.alt = "Sad face";
            icon.title = "Sad face";
            icon.src = icon.src.replace('man-reading', 'sad-face');
            document.getElementsByTagName('h1')[0].textContent = 'Ops! Something went wrong.';
            document.querySelector('.page-info').textContent = 'Please Try Again In A Few Moments.';
            document.getElementById('waiters-container').remove();
            document.getElementById('ia-next-info').remove();
            //document.getElementById('lus').innerHTML = res.error;
            document.getElementById('lus').innerHTML = 'Ouch! It looks like we are experiencing some connection problems. Please retry later. '; // + res.error;
            document.getElementById('lus').classList.add('ops-error');
            document.getElementById('try-again-button-container').classList.remove('hidden');
        } else if (res.pendingLU) {
            window.location.href = '../learning-unit/' + res.pendingId + '/pending';
        } else if (res.recapTestTime) {
            window.location.href = '../recap-test-time';
        } else {
            const link = res.isLabourMarket ? '/learning-unit-labour-market/' : '/learning-unit/';
            if (res.isLabourMarket && res.data.length === 1) {
                window.location.href = link + res.data[0].id + '/show';
            } else {
                document.getElementsByTagName('h1')[0].textContent = res.atLeastOne ? 'What\'s next? You choose!' : 'You\'re all set! Are you ready to learn?';
                var s = '';
                for (let i in res.data) {
                    const datum = res.data[i];
                    s += '<a class="ia-next-lu" data-i="' + i + '" href="' + link + datum.id + '/show" title="Click to go to the learning unit">';
                        s += '<div>';
                            if (!res.isLabourMarket) {
                                s += '<p class="ia-next-lu-cluster">';
                                    s += res.clusters[datum.cluster_number];
                                s += '</p>';
                            }
                            s += '<p class="ia-next-lu-title">';
                                s += datum.title;
                            s += '</p>';
                            if (!res.isLabourMarket) {
                                s += '<div class="ia-next-lu-subtitle-container">';
                                    s += '<p class="ia-next-lu-subtitle">';
                                        s += datum.subtitle;
                                    s += '</p>';
                                s += '</div>';
                            }
                        s += '</div>';
                    s += '</a>';
                }
                document.getElementById('lus').innerHTML = s;
                resize();
                setTimeout(resize, 100);
            }
        }
    }


    /**
     * Inits
     */
    function init() {
        setTimeout(() => {
            document.getElementById('ia-next-container').classList.add('shown');
        }, 100);
        if (!window.mobileAndTabletCheck()) {
            resize();
            window.addEventListener('resize', resize);
            document.addEventListener('mouseover', mouseover);
            document.addEventListener('mouseout', mouseout);
        }
        askNext();
    }

    
    var sizes = [];
    init();

})();