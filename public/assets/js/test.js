/**
 * Submits a test
 * @todo needs refactoring
 * @param {dom} target the clicked element
 * @returns false
 */
function confirmSubmit(target) {
    const id = target.getAttribute('data-id');
    const type = target.getAttribute('data-type');
    document.querySelector('.test-submit-confirm-message[data-id="' + id + '"]').classList.add('hidden');
    document.querySelector('.test-submit[data-id="' + id + '"]').classList.add('hidden');
    document.querySelector('.waiter[data-id="' + id + '"]').classList.remove('hidden');

    var data;
    if (type === 'gap-fill') {
        data = Array.from(document.querySelectorAll('.cloze[data-id="' + id + '"]'))
            .map(item => {
                item.setAttribute('disabled', 'true');
                return item.selectedOptions[0].textContent;
            });
    } else if (type === 'multiple-choice') {
        data = Array.from(document.querySelectorAll('.checkbox-container[data-id="' + id + '"]'))
            .filter(item => {
                item.setAttribute('disabled', 'true');
                return item.hasAttribute('checked');
            })
            .map(item => +item.getAttribute('data-my-id'));
    } else if (type === 'matching-terms') {
        data = Array.from(document.querySelectorAll('.pair[data-id="' + id + '"]'))
            .map(item => {
                var dragged = item.querySelector('.draggable');
                if (dragged) {
                    return item.querySelector('.pair-key').getAttribute('data-my-id') + 
                            ':' + 
                            dragged.getAttribute('data-my-id')
                }
                return '';
            });
        document.querySelectorAll('.draggable[data-id="' + id + '"]').forEach(item => {
            item.removeAttribute('draggable');
        });
    } else if (type === 'jumble-text') {
        data = Array.from(document.querySelectorAll('.draggable[data-id="' + id + '"]'))
            .map(item => {
                item.removeAttribute('draggable');
                return item.getAttribute('data-my-id');
            });
    }

    
    const isRecapTest = document.getElementById('is-recap-test') === null ? 0 : 1;

    ajax({
        url: 'check-test',
        data: 'id=' + id + '&type=' + type + '&data=' + JSON.stringify(data) + '&isRecapTest=' + isRecapTest,
        onsuccess: res => {
            document.querySelector('.waiter[data-id="' + id + '"]').classList.add('hidden');
            res = JSON.parse(res);

            document.querySelector('.test[data-id="' + id + '"]').setAttribute('completed', 'true');


            if (type === 'gap-fill') {
                var solutionParts = [];
                document.querySelectorAll('.cloze[data-id="' + id + '"]').forEach((item, i) => {
                    solutionParts.push(res.correctDetails[i]);
                    if (item.selectedOptions[0].textContent === res.correctDetails[i]) {
                        item.classList.add('correct');
                    } else {
                        Array.from(item.options).forEach(option => {
                            if (option.textContent === res.correctDetails[i]) {
                                //item.value = option.value;
                            }
                        });
                        item.classList.add('wrong');
                    }
                });
                var solution = '';
                document.querySelectorAll('.test[data-id="' + id + '"] .test-content>span').forEach((item, i) => {
                    solution += item.innerHTML;
                    if (solutionParts[i]) {
                        solution += ' | ' + solutionParts[i] + ' | ';
                    }
                });
                document.querySelector('.test[data-id="' + id + '"] .test-solution').innerHTML = solution;
            } else if (type === 'multiple-choice') {
                var solutionParts = '';
                document.querySelectorAll('.checkbox-container[data-id="' + id + '"]').forEach(item => {
                    if (item.hasAttribute('checked') && res.correctDetails.indexOf(+item.getAttribute('data-my-id')) > -1) {
                        item.classList.add('correct');
                    }
                    if (item.hasAttribute('checked') && res.correctDetails.indexOf(+item.getAttribute('data-my-id')) < 0) {
                        item.classList.add('wrong');
                    }
                    if (res.correctDetails.indexOf(+item.getAttribute('data-my-id')) > -1) {
                        solutionParts += '<br />- ' + item.querySelector('.multiple-choice-label').innerHTML;
                    }
                });
                if (!solutionParts.length) {
                    solutionParts = '<br />(no correct answer)';
                }
                var solution = '';
                solution += document.querySelector('.test[data-id="' + id + '"] .test-question').innerHTML;
                document.querySelector('.test[data-id="' + id + '"] .test-solution').innerHTML = solution + solutionParts;
            } else if (type === 'matching-terms') {
                var solution = '';
                document.querySelectorAll('.pair[data-id="' + id + '"]').forEach((item, i) => {
                    var pair_one = item.querySelector('.pair-key').getAttribute('data-my-id');
                    var pair_two = item.querySelector('.draggable').getAttribute('data-my-id');
                    if (pair_one + ':' + pair_two === res.correctDetails[i].code) {
                        item.querySelector('.droppable').classList.add('correct');
                    } else {
                        item.querySelector('.droppable').classList.add('wrong');
                    }
                    solution += res.correctDetails[i].pair_one + ' | ' + res.correctDetails[i].pair_two + ' |<br />';
                });
                document.querySelector('.test[data-id="' + id + '"] .test-solution').innerHTML = solution;
            } else if (type === 'jumble-text') {
                document.querySelectorAll('.draggable[data-id="' + id + '"]').forEach((item, i) => {
                    if (item.getAttribute('data-my-id') === res.correctDetails[i].code) {
                        item.parentNode.classList.add('correct');
                    } else {
                        item.parentNode.classList.add('wrong');
                    }
                });
                var solution = '| ' + res.correctDetails.map(function(detail) {
                    return detail.text;
                }).join(' | ');
                document.querySelector('.test[data-id="' + id + '"] .test-solution').innerHTML = solution;
            }

            countCompletedTests();

        }
    });

    return false;
}


/**
 * Sets the counter of submitted tests
 */
function countCompletedTests() {
    // const expected = document.querySelectorAll('.test').length;
    var expected = 0;
    document.querySelectorAll('.test').forEach(test => {
        !test.querySelector('.corr-test') && (expected++);
    });
    const completed = document.querySelectorAll('.test[completed="true"]').length;
    document.getElementById('completed-tests-amount').textContent = completed + '/' + expected;
    document.querySelectorAll('#tests-continue .button').forEach(button => button.classList.toggle('hidden', completed < expected));
}


/**
 * Cancels the submit requests
 * @param {dom} target the clicked element
 * @returns false
 */
function cancelSubmit(target) {
    const id = target.getAttribute('data-id');
    document.querySelector('.test-submit-confirm-message[data-id="' + id + '"]').classList.add('hidden');
    document.querySelector('.test-submit[data-id="' + id + '"]').classList.remove('hidden');
    return false;
}


/**
 * Confirms the submit requests
 * @param {dom} target the clicked element
 * @returns false
 */
function testSubmit(target) {
    const id = target.getAttribute('data-id');
    target.classList.add('hidden');
    document.querySelector('.test-submit-confirm-message[data-id="' + id + '"]').classList.remove('hidden');
}


/**
 * What to do on cloze change
 * @param {dom} target the clicked element
 */
function clozeChange(target) {
    const id = target.getAttribute('data-id');
    var ok = true;
    document.querySelectorAll('.test[data-id="' + id + '"] select').forEach(select => {
        !select.value && (ok = false);
    });
    if (ok) {
        document.querySelector('button.test-submit[data-id="' + id + '"]').removeAttribute('disabled');
    } else {
        document.querySelector('button.test-submit[data-id="' + id + '"]').setAttribute('disabled', true);
    }
}


function dragging(ev) {
    if (!isIOS) {
        if (my < 200) {
            window.scrollTo(0, window.scrollY - 5);
        } else if (my > window.innerHeight - 100) {
            window.scrollTo(0, window.scrollY + 5);
        }
    }
}


function setDraggingTest(source) {
    document.querySelector('.test[data-id="' + source.getAttribute('data-id') + '"]').classList.add('dragging');
}

function unsetDraggingTest() {
    document.querySelectorAll('.test.dragging').forEach(item => {
        item.classList.remove('dragging');
    });
}

function allowDrop(ev) {
    ev.preventDefault();
    var droppable = ev.target.classList.contains('draggable') ? ev.target.parentNode : ev.target;
    droppable.parentNode.querySelectorAll('.droppable').forEach(droppable => {
        droppable.classList.remove('highlight-droppability');
    });
    droppable.classList.add('highlight-droppability');
}

function leaveDrop(ev) {
    ev.target.classList.remove('highlight-droppability');
}

function drag(ev) {
    setDraggingTest(ev.target);
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var target = ev.target;
    while (!target.classList.contains('droppable') && target.tagName.toLowerCase() !== 'body') {
        target = target.parentNode;
    }
    target.classList.remove('highlight-droppability');
    const data = ev.dataTransfer.getData('text');
    const item = document.getElementById(data);
    if (target.children.length) {
        const present = target.children[0];
        const from = document.getElementById(data).parentNode;
        if (from.classList.contains('droppable')) {
            from.appendChild(present);
        } else {
            document.querySelector('.draggables-list[data-id="' + item.getAttribute('data-id') + '"]').appendChild(present);
        }
    }
    target.appendChild(item);
}


function dragend(e) {
    unsetDraggingTest(e.target);
    e.target.parentNode.classList.remove('highlight-droppability');
}

function change(e) {
    const target = e.target;
    const classList = target.classList;
    if (classList.contains('cloze')) {
        target.classList.toggle('selected', target.value);
    }
}

function click(e) {
    const target = e.target;
    const classList = target.classList;
    if (classList.contains('checkbox-container')) {
        if (!target.hasAttribute('disabled')) {
            const checkbox = target.querySelector('input[type="checkbox"]');
            checkbox.checked = !checkbox.checked;
            if (checkbox.checked) {
                target.setAttribute('checked', 'true');
                checkbox.checked = 'true';
            } else {
                target.removeAttribute('checked');
                checkbox.checked = '';
            }

            setTimeout(() => {
                const tid = target.getAttribute('data-id');
                const expectedCorrect = +document.querySelector('.expected-correct[data-id="' + tid + '"]').value;
                const myChoices = Array.from(document.querySelectorAll('.checkbox-container[data-id="' + tid + '"]'));
                const checked = myChoices.reduce((accumulator, item) => {
                    return accumulator + (item.hasAttribute('checked') ? 1 : 0);
                }, 0);
                if (expectedCorrect && checked >= expectedCorrect) {
                    myChoices.forEach(item => {
                        if (!item.hasAttribute('checked')) {
                            item.setAttribute('disabled', 'true');
                        }
                    });
                } else {
                    myChoices.forEach(item => {
                        item.removeAttribute('disabled');
                    });
                }
            }, 50);

        }
    }
}


/**
 * Events
 */
function events() {
    document.addEventListener('change', change);
    document.addEventListener('click', click);
    document.addEventListener('dragend', dragend);
    document.addEventListener('dragover', e => {
        e = e || window.event;
        my = e.screenY;
    }, false);
}


/**
 * Inits
 */
function init() {
    countCompletedTests();
    events();
}


function iOS() {
    return [
      'iPad Simulator',
      'iPhone Simulator',
      'iPod Simulator',
      'iPad',
      'iPhone',
      'iPod'
    ].includes(navigator.platform)
    // iPad on iOS 13 detection
    || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}
var isIOS = iOS();


var my; // mouse position on dragover on document
init();