(function() {

    const table = document.getElementById('registry-table');


    /**
     * What to do on table click
     * @param {event} e the event
     */
    function tableClick(e) {
        if (e.target.tagName.toLowerCase() === 'td') {
            window.location.href = e.target.parentNode.getAttribute('data-link') + (uid ? '?ref=' + uid : '');
        }
    }


    /**
     * What to do on click
     * @param {event} e the event
     */
    function click(e) {
    }

    
    /**
     * Events
     */
    function events() {
        table.addEventListener('click', tableClick);
        document.addEventListener('click', click);
    }


    /**
     * Inits
     */
    function init() {
        $(table).DataTable({
            order: [[3, 'desc']]
        });
        events();
    }

    init();

})();