(function() {


    
    /**
     * Animates the exclamation image
     */
     function animateExclamation() {
        setTimeout(() => {
            document.querySelector('#exclamation-1').classList.add('shown');
        }, 10);
        setTimeout(() => {
            document.querySelector('#exclamation-2').classList.add('shown');
        }, 100);
    }


    /**
     * Animates the trophy image
     */
    function animateTrophy() {
        blink(1);
        setTimeout(() => blink(2), 300);
        setTimeout(() => blink(4), 600);
        setTimeout(() => blink(3), 700);
    }


    /**
     * Animates the getting-there image
     */
    function animateGettingThere() {
        setTimeout(() => {
            document.querySelector('.unstroked').classList.add('stroked');
        }, 100);
        const lines = document.querySelectorAll('.getting-there-line');
        const nlines = lines.length;
        lines.forEach((line, i) => {
            setTimeout(() => {
                line.classList.remove('hidden');
            }, 100 + (nlines-i)*60);
        });
    }


    /**
     * Blinks all the blinks of class .trophy-blink-n
     * of the trophy image, where ...-n is a number
     */
    function blink(n) {
        document.querySelectorAll('.trophy-blink-' + n).forEach(item => {
            item.style.display = 'block';
            item.classList.add('blink');
        });
    }


    /**
     * Animates the score counter
     */
    function animateScore() {
        const A_VALUE = 30;
        var current = 0;
        var intv = setInterval(() => {
            if (current >= scoreValue) {
                scoreValueItem.textContent = scoreValue;
                clearInterval(intv);
                return false;
            } else {
                scoreValueItem.textContent = current;
                scoreValueItem.classList = 'score-' + Math.floor(current/A_VALUE);
                current++;
            }
        }, 60);
    }


    /**
     * Animates the progress bars
     */
    function animateProgressBar(bar) {
        const value = +bar.getAttribute('data-value');
        const step = value/100;
        var current = 0;
        var intv = setInterval(() => {
            if (current >= value) {
                bar.value = value;
                clearInterval(intv);
                return false;
            } else {
                bar.value = current;
                current += step;
            }
        }, 10);
    }


    /**
     * Animates the progress bars after a delay
     * @param {number} delay the delay in milliseconds
     */
    function animateProgressBars(delay) {
        setTimeout(() => {
            document.querySelectorAll('progress').forEach(animateProgressBar);
        }, delay);
    }


    /**
     * Shows the trophy image
     */
    function showTrophy() {
        document.getElementById('trophy').classList.remove('hidden');
        setTimeout(() => {
            document.querySelector('#trophy').classList.add('shown');
        }, 10);
    }


    /**
     * Shows the exclamation image
     */
    function showExclamation() {
        document.getElementById('exclamation').classList.remove('hidden');
    }


    /**
     * Shows the exclamation image
     */
    function showGettingThere() {
        document.getElementById('getting-there').classList.remove('hidden');
    }


    /**
     * Shows the correct feedback image, based on two thresholds
     * @param {number} value the value to check
     * @param {number} threshold1 the first threshold
     * @param {number} threshold2 the second threshold
     */
    function showFeedbackImage(value, threshold1, threshold2) {
        if (value >= threshold1) {
            showTrophy();
            animateTrophy();
        } else if (value >= threshold2) {
            showGettingThere();
            animateGettingThere();
        } else {
            showExclamation();
            animateExclamation();
        }
    }




    /**
     * Inits
     */
    function init() {
        
        scoreValueItem = document.getElementById('score-value');
        scoreValue = +scoreValueItem.getAttribute('data-score');

        showFeedbackImage(scoreValue, LOWER_BOUND_TO_CONSIDER_TEST_AS_SUPER_PASSED, LOWER_BOUND_TO_CONSIDER_TEST_AS_PASSED);
        animateScore();
        animateProgressBars(300);
    }


    var scoreValueItem, scoreValue;
    const LOWER_BOUND_TO_CONSIDER_TEST_AS_SUPER_PASSED = 70;
    const LOWER_BOUND_TO_CONSIDER_TEST_AS_PASSED = 60;

    init();


})();