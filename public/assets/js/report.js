'use strict';

(function() {

    const ORANGE = '#FE9900';
    const GREEN = '#D8FFD8';
    const BLUE = '#0000CC';
    const RED = '#FFD8D8';
    let h3s;
    const animations = {
        1: { started: false, h3: 0 },
        2: { started: false, h3: 2 },
        3: { started: false, h3: 5 },
        4: { started: false, h3: 6 },
        5: { started: false, h3: 7 },
        6: { started: false, h3: 8 }
    };
    const DEFAULT_CHART_HEIGHT = 250; // px



    init();


    /**
     * Inits
     */
    function init() {
        h3s = document.querySelectorAll('h3, h4');
        events();
        resize();
        runAnimations();
    }


    /**
     * Binds events
     */
    function events() {
        document.addEventListener('click', click);
        window.addEventListener('scroll', scroll);
        window.addEventListener('resize', resize);
    }


    /**
     * What to do on resize
     */
    function resize() {
        window.innerWidth <= 700 && (showDetails());
    }


    /**
     * What to do on scroll
     * @param {*} e the event
     */
    function scroll(e) {
        runAnimations();
    }


    /**
     * Runs animations, if needed
     */
    function runAnimations() {
        const y = window.scrollY;
        for (let a in animations) {
            const animation = animations[a];
            if (!animation.started && h3s[animation.h3] && h3s[animation.h3].getBoundingClientRect().y < window.innerHeight/1.8) {
                animation.started = true;
                a = +a;
                if (a === 1) {
                    steppedChart('chart-access-and-usage');
                } else if (a === 2) {
                    progressesAnimation();
                } else if (a === 3) {
                    trophiesAnimation();
                } else if (a === 4) {
                    polarChart('chart-tests-overall');
                } else if (a === 5) {
                    columnsChart('chart-tests-by-cluster');
                } else if (a === 6) {
                    animateThumbs();
                }
            }
        }
    }


    /**
     * What to do on click
     * @param {*} e the event
     */
    function click(e) {
        const target = e.target;
        if (target.id === 'hide-details') {
            hideDetails();
        } else if (target.id === 'show-details') {
            showDetails();
        } else if (target.id === 'to-top') {
            window.scrollTo(0, 0);
        } else if (target.id === 'download') {
            saveReport_step1();
        }
    }


    /**
     * Hides details
     */
    function hideDetails() {
        document.querySelectorAll('.detail').forEach(detail => detail.classList.add('hidden'));
        document.getElementById('hide-details').classList.add('hidden');
        document.getElementById('show-details').classList.remove('hidden');
    }


    /**
     * Shows details
     */
    function showDetails() {
        document.querySelectorAll('.detail').forEach(detail => detail.classList.remove('hidden'));
        document.getElementById('hide-details').classList.remove('hidden');
        document.getElementById('show-details').classList.add('hidden');
    }


    /**
     * Setups a dataset for the column chart
     * @param {*} label the label
     * @param {*} backgroundColor the column's color
     * @param {*} borderColor the column border color
     * @param {*} data the data
     * @returns 
     */
    function getColumnChartDataset(label, backgroundColor, borderColor, data) {
        return {
            label: label,
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            borderWidth: 1,
            data: data,
            barPercentage: .7
        };
    }
    

    /**
     * Creates a column chart in a container element
     * @param {*} id the id of the container element
     */
    function columnsChart(id) {
        new Chart(document.getElementById(id).getContext('2d'), {
            type: 'bar',
            data: {
                labels: clusters,
                datasets: [
                    getColumnChartDataset('Failed', RED, ORANGE, dataByCluster[0]),
                    getColumnChartDataset('Passed', GREEN, BLUE, dataByCluster[1])
                ]
            },
            options: {
                maintainAspectRatio: false,
                scales: {
                    x: {
                        grid:  { display: false },
                        ticks: { color: '#fe9900' }
                    },
                    y: {
                        grid:  { display: false },
                        ticks: { display: false }
                    }
                }
            }
        });
    }
    

    /**
     * Creates a polar chart in a container element
     * @param {*} id the id of the container element
     */
    function polarChart(id) {
        new Chart(document.getElementById(id), {
            type: 'polarArea',
            data: {
                labels: ['Passed', 'Failed'],
                datasets: [{
                    backgroundColor: [GREEN, RED],
                    borderColor: [BLUE, ORANGE],
                    borderWidth: 1,
                    data: [ok, ko]
                }]
            },
            options: {
                maintainAspectRatio: false,
                plugins: {
                    legend: { display: true }
                },
                scales: {
                    r: { display: false }
                }
            }
        });
    }
    

    /**
     * Creates a stepped chart in a container element
     * @param {*} id the id of the container element
     */
    function steppedChart(id) {

        var labels = [], datasetData = [], acc = 0;
        countPerDay.forEach(datum => {
            labels.push(new Date(datum.date));
            acc += datum.n;
            datasetData.push(acc);
        });

        new Chart(document.getElementById(id), {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    backgroundColor: ORANGE,
                    borderColor: ORANGE,
                    borderWidth: 2.5,
                    data: datasetData,
                    label: 'dataset',
                    stepped: true
                }]
            },
            options: {
                maintainAspectRatio: false,
                plugins: {
                    legend: { display: false },
                    tooltip: {
                        callbacks: {
                            title: context => {
                                let date = new Date(context[0].parsed.x);
                                return date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear();
                            },
                            label: context => ' Learning Units completed: ' + context.raw
                        }
                    }
                },
                scales: {
                    x: {
                        type: 'time',
                        title: { display: false, text: 'Date' },
                        ticks: { source: 'labels' },
                        time: {
                            unit: 'day'
                        }
                    },
                    y: {
                        title: { display: false, text: 'value' }/*,
                        ticks: {
                            callback: function(val, index) {
                                return Math.round(val);
                            }
                        }*/
                    }
                },
            },
        });
    }


    /**
     * Animates trophies
     */
    function trophiesAnimation() {
        document.querySelectorAll('#trophies img').forEach((trophy, i) => {
            setTimeout(_ => {
                trophy.classList.add('shown');
            }, i*50);
        });
    }


    /**
     * Animates progresses
     */
    function progressesAnimation() {
        document.querySelectorAll('progress').forEach((progress, i) => {
            setTimeout(_ => {
                (function(progress) {
                    let current = 0;
                    const expected = +progress.getAttribute('data-value');
                    let intv = setInterval(_ => {
                        progress.value = current++;
                        if (current >= expected) {
                            progress.value = expected;
                            clearInterval(intv);
                        }
                    }, 10);
                })(progress);
            }, i*50);
        });
    }


    /**
     * Animates thumbs
     */
    function animateThumbs() {
        const tot = liked + disliked;
        if (tot) {
            animateThumb('thumb-up', 100/tot * liked);
            animateThumb('thumb-down', 100/tot * disliked);
        }
    }


    /**
     * Animates a thumb
     */
    function animateThumb(thumbId, percentageLiked) {
        const minY = 10, maxY = 160;
        const y = minY + (maxY - minY)/100 * (100-percentageLiked);
        let cy = maxY;
        const thumbItem = document.getElementById(thumbId);
        let intv = setInterval(_ => {
            if (cy > y) {
                thumbItem.setAttribute('y', cy);
                cy--;
            } else {
                thumbItem.setAttribute('y', y);
                clearInterval(intv);
            }
        }, 20);
    }








    // FROM HERE ON: functions for report download


    /**
     * Emulates a progress bar into the #custom canvas
     * @param {*} id the if of the progress bar
     * @param {*} width the width to assign to the #custom canvas
     * @param {*} height the height to assign to the #custom canvas
     * @returns the base64 data of the image
     */
    function createProgressBarImage(id, width = 1000, height = 20) {
        const custom = document.getElementById('custom');
        custom.style.width = width + 'px';
        custom.style.height = height + 'px';
        custom.width = width;
        custom.height = height;
        const border = height/5;
        const radius = 7;
        const ctx = custom.getContext('2d');
        ctx.strokeStyle = '#fe9900';
        ctx.lineWidth = border;
        ctx.fillStyle = '#f6f6f6';
        roundRect(ctx, border, border, width-2*border, height-2*border, radius, true, true); // background
        ctx.fillStyle = '#fe9900';
        const w = (width-2*border)/100 * document.getElementById(id).getAttribute('data-value');
        w > 10 && (roundRect(ctx, border, border, w, height-2*border, radius, true, false)); // progress advance

        return custom.toDataURL('image/png');
    }


    // https://stackoverflow.com/a/3368118
    /**
     * Draws a rounded rectangle using the current state of the canvas.
     * If you omit the last three params, it will draw a rectangle
     * outline with a 5 pixel border radius
     * @param {CanvasRenderingContext2D} ctx
     * @param {Number} x The top left x coordinate
     * @param {Number} y The top left y coordinate
     * @param {Number} width The width of the rectangle
     * @param {Number} height The height of the rectangle
     * @param {Number} [radius = 5] The corner radius; It can also be an object 
     *                 to specify different radii for corners
     * @param {Number} [radius.tl = 0] Top left
     * @param {Number} [radius.tr = 0] Top right
     * @param {Number} [radius.br = 0] Bottom right
     * @param {Number} [radius.bl = 0] Bottom left
     * @param {Boolean} [fill = false] Whether to fill the rectangle.
     * @param {Boolean} [stroke = true] Whether to stroke the rectangle.
     */
    function roundRect(ctx, x, y, width, height, radius = 5, fill = false, stroke = true) {
        if (typeof radius === 'number') {
            radius = {tl: radius, tr: radius, br: radius, bl: radius};
        } else {
            radius = {...{tl: 0, tr: 0, br: 0, bl: 0}, ...radius};
        }
        ctx.beginPath();
        ctx.moveTo(x + radius.tl, y);
        ctx.lineTo(x + width - radius.tr, y);
        ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
        ctx.lineTo(x + width, y + height - radius.br);
        ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
        ctx.lineTo(x + radius.bl, y + height);
        ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
        ctx.lineTo(x, y + radius.tl);
        ctx.quadraticCurveTo(x, y, x + radius.tl, y);
        ctx.closePath();
        fill && (ctx.fill());
        stroke && (ctx.stroke());
    }


    /**
     * Creates the images representing the progress bars
     * @returns the base64 data of the images
     */
    function createProgressBarsImages() {
        let progress_dataURLs = {};
        progress_dataURLs['overall'] = createProgressBarImage('overall-progress', 1800, 32);
        for (let i = 0; i <= 11; i++) {
            progress_dataURLs['skill-' + i] = createProgressBarImage('progress-skill-' + i, 522, 20);
        }
        return progress_dataURLs;
    }


    /**
     * Saves the images of the progress bars
     * @param {*} progress_dataURLs the base64 data of the progress bars
     * @param {*} callback the callback function
     */
    function saveProgressBarsImages(progress_dataURLs, callback) {
        var imageCodes = {};
        let n = 0;
        for (let i in progress_dataURLs) {
            n++;
        }
        for (let i in progress_dataURLs) {
            const params = 'name=' + i + '&data=' + progress_dataURLs[i].replace(/\+/g, '#');
            ajax('/ajax/save-image', params, res => {
                imageCodes[res.name] = res.name + '-' + res.code;
                n--;
                !n && (callback(imageCodes));
            }, true);
        }
    }


    /**
     * Saves the images of the charts
     * @param {*} ids the id of the charts (canvas)
     * @param {*} callback the callback function
     */
    function saveChartJSImages(ids, callback) {
        var imageCodes = {};
        let n = ids.length;
        for (let i in ids) {
            const data = document.getElementById('chart-' + ids[i]).toDataURL('image/png');
            const params = 'name=' + ids[i] + '&data=' + data.replace(/\+/g, '#');
            ajax('/ajax/save-image', params, res => {
                imageCodes[res.name] = res.name + '-' + res.code;
                n--;
                !n && (callback(imageCodes));
            }, true);
        }
    }


    /**
     * Sizes a canvas
     * @param {*} id the id of the canvas
     * @param {*} newWidth the new width
     * @param {*} previousWidth the old width
     * @param {*} previousHeight the old height 
     */
    function sizeCanvas(id, newWidth, previousWidth, previousHeight) {
        const canvas = document.getElementById(id);
        canvas.parentNode.style.width = newWidth + 'px';
        canvas.parentNode.style.height = (newWidth/previousWidth*previousHeight) + 'px';
    }


    /**
     * Resets the size of a canvas
     * @param {*} id the id of the canvas
     * @param {*} height the new height
     */
    function resetChartSize(id, height) {
        const item = document.getElementById(id).parentNode;
        item.style.width = 'auto';
        item.style.height = height + 'px';
    }


    /**
     * Hides report producing message
     */
    function hideProducingReportMessage() {
        document.getElementById('download').classList.remove('hidden');
        document.getElementById('download-message').classList.add('hidden');
    }


    /**
     * Shows report producing message
     */
    function showProducingReportMessage() {
        document.getElementById('download').classList.add('hidden');
        document.getElementById('download-message').classList.remove('hidden');
    }


    /**
     * Saves the report - step 1
     */
    function saveReport_step1() {
        showDetails();
        showProducingReportMessage();
        saveProgressBarsImages(createProgressBarsImages(), saveReport_step2);
    }


    /**
     * Saves the report - step 2
     */
    function saveReport_step2(imageCodes) {
        
        sizeCanvas('chart-access-and-usage', 1500, 900, DEFAULT_CHART_HEIGHT);
        sizeCanvas('chart-tests-overall', 1000, 440, DEFAULT_CHART_HEIGHT);
        sizeCanvas('chart-tests-by-cluster', 1500, 900, DEFAULT_CHART_HEIGHT);

        setTimeout(_ => { // wait a bit to let chartJs to resize charts
            saveReport_step3(imageCodes);
        }, 100);
    }


    /**
     * Saves the report - step 3
     */
    function saveReport_step3(imageCodes) {
        const ids = ['tests-overall', 'access-and-usage', 'tests-by-cluster'];
        saveChartJSImages(ids, chartJSImageCodes => {
            for (let i in chartJSImageCodes) {
                imageCodes[i] = chartJSImageCodes[i];
            }
            saveReport_step4(imageCodes);
        });
    }


    /**
     * Retrieves data about the indicators
     * @returns the indicators data
     */
    function getIndicatorsData() {
        const indicators = {
            'access-and-usage': [],
            'learning-path': [],
            'trophies': [],
            'tests': [],
            'reception': []
        };
        for (let i in indicators) {
            document.querySelectorAll('#' + i + '-indicators .chart-indicator').forEach(indicator => {
                indicators[i].push({
                    title: indicator.querySelector('.chart-indicator-title').textContent,
                    value: indicator.querySelector('.chart-indicator-value').innerHTML
                });
            });
        }
        return indicators;
    }


    /**
     * Retrieves data about the trophies
     * @returns the trophies data
     */
    function getTrophiesData() {
        return Array.from(document.querySelectorAll('#trophies .detail img')).map(img => {
            return 'assets/img/' + img.getAttribute('data-src') + '.png';
        });
    }


    /**
     * Retrieves data about the skill percentages
     * @returns the skills data
     */
    function getSkillPercentages() {
        return Array.from(document.querySelectorAll('progress.short'))
            .map(progress => progress.getAttribute('data-value'));
    }


    /**
     * Saves the report - step 4
     */
    function saveReport_step4(imageCodes) {
        const user = isSURFA ? document.querySelector('h1.title').textContent.trim() : document.querySelector('#welcome-subtitle div:nth-child(1)').textContent.trim();
        const hash = document.getElementById('report-container').getAttribute('data-hash');
        const params = [
                        'hash=' + hash,
                        'user=' + user,
                        'imageCodes=' + JSON.stringify(imageCodes),
                        'skillPercentages=' + JSON.stringify(getSkillPercentages()),
                        'indicators=' + JSON.stringify(getIndicatorsData()),
                        'trophies=' + JSON.stringify(getTrophiesData()),
                        'car=' + document.querySelector('#tests-overall-left div:nth-child(1) p:nth-child(2)').textContent,
                        'rr=' + document.querySelector('#tests-overall-left div:nth-child(2) p:nth-child(2)').textContent
                    ].join('&');
        
        ajax('/ajax/save-report', params, res => {
            resetChartSize('chart-access-and-usage', DEFAULT_CHART_HEIGHT);
            resetChartSize('chart-tests-overall', DEFAULT_CHART_HEIGHT);
            resetChartSize('chart-tests-by-cluster', DEFAULT_CHART_HEIGHT);
            window.open('/reports/report-' + hash + '.pdf');
            hideProducingReportMessage();
        }, false);

    }


    /**
     * An async request
     * @param {*} url the url to call
     * @param {*} params the parameters to pass
     * @param {*} callback the callback function
     * @param {*} passResult true to pass json-parsed results to the callback function
     */
    function ajax(url, params, callback, passResult) {
        const http = new XMLHttpRequest();
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.setRequestHeader("X-CSRF-TOKEN", document.querySelector('meta[name="csrf-token"]').content);
        http.onreadystatechange = function() {
            if (+http.readyState === 4 && +http.status === 200) {
                callback && (callback(passResult ? JSON.parse(http.responseText) : http.responseText));
            }
        }
        http.send(params);
    }



})();
