(function() {


    /**
     * Shows the button to continue
     */
    function enableContinueButton() {
        document.getElementById('lu-go-to-test').removeAttribute('disabled');
    }


    /**
     * Likes or dislikes
     * @param {dom} target the clicked item
     * @param {number} luid the id of the learning unit
     * @param {number} like 1 to like, -1 to dislike
     * @param {function} callback a callback
     */
    function _setLike(target, luid, like, callback) {
        ajax({
            url: like > 0 ? 'like-lu' : 'unlike-lu',
            data: 'luid=' + luid,
            onsuccess: res => {
                _setLikeSuccess(target);
                callback && (callback());
            }
        });
    }


    /**
     * Called back after like or dislike
     * @param {dom} target the clicked item
     */
    function _setLikeSuccess(target) {
        if (target) {
            document.querySelectorAll('#lu-liked-box button').forEach(item => {
                item.classList.remove('active');
            });
            target.classList.add('active');
        }
        setTimeout(_ => {
            enableContinueButton();
            scrollTillEnd();
        }, 400);
    }
    

    /**
     * Likes a learning unit
     * @param {dom} target the clicked item
     * @param {*} luid the id of the learning unit
     */
    function likeLU(target, luid) {
        _setLike(target, luid, +1);
    }
    

    /**
     * Dislikes a learning unit
     * @param {dom} target the clicked item
     * @param {*} luid the id of the learning unit
     */
    function unlikeLU(target, luid) {
        _setLike(target, luid, -1);
    }


    /**
     * Event on click
     * @param {event} e the event
     */
    function click(e) {
        const target = e.target;
        const id = target.id;
        if (id === 'language-point-opener') {
            document.getElementById('language-point').classList.remove('hidden');
            setTimeout(() => {
                document.getElementById('language-point-container').classList.add('shown');
            }, 10);
        } else if (id === 'language-point-closer') {
            document.getElementById('language-point').classList.add('hidden');
        } else if (id === 'language-point') {
            document.getElementById('language-point').classList.add('hidden');
        } else if (id === 'lu-liked') {
            likeLU(target, target.getAttribute('data-luid'));
        } else if (id === 'lu-not-liked') {
            unlikeLU(target, target.getAttribute('data-luid'));
        } else if (id === 'lu-go-to-test') {
            goToTests();
        }
    }


    /**
     * Sends to tests
     */
    function goToTests() {
        window.location.href = document.getElementById('lu-go-to-test').getAttribute('href');
    }


    /**
     * Events
     */
    function events() {
        document.addEventListener('click', click);
    }


    /**
     * Sets blank target for all the link in .lu-content
     */
    function setLinkTargets() {
        document.querySelectorAll('.lu-content a').forEach(a => a.setAttribute('target', '_blank'));
    }


    /**
     * Inits
     */
    function init() {
        events();
        setLinkTargets();
    }

    
    init();


})();