(function() {
    const rc = document.getElementById('redirection-counter');
    let secs = 3;
    rc.textContent = secs;
    const intv = setInterval(_ => {
        rc.textContent = --secs;
        if (!secs) {
            clearInterval(intv);
            window.location.href = rc.parentNode.parentNode.querySelector('a').href;
        }
    }, 1000);
})();