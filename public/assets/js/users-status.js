(function() {

    const table = document.getElementById('registry-table');


    /**
     * What to do on table click
     * @param {event} e the event
     */
    function tableClick(e) {
        if (e.target.tagName.toLowerCase() === 'td') {
            if (!e.target.hasAttribute('colspan')) {
                const id = e.target.parentNode.getAttribute('data-id');
                window.location.href = '/superuser/dashboard?uid=' + id;
            }
        }
    }


    /**
     * Events
     */
    function events() {
        table.addEventListener('click', tableClick);
    }


    /**
     * Inits
     */
    function init() {
        events();
        $('#registry-table').DataTable({
            order: [[1, 'desc']]
        });
    }

    init();

})();